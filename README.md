# NMSU MESH

To run the MESH project, use the following configuration:
  - Program Arguments: -gui -agents house1:agents.HouseAgent
  - Main: jade.Boot

Here's a screenshot of the Intellij configuration:

![alt text](https://i.imgur.com/BVqivq3.gif)

***Important note:** In order to run this application, you must first locally run the MESH_API application locally. Eventually this will be deployed on a server.*

For more information on running/understanding this project, see the [Developer Guide](BackingResearch/MESH_2018_DEVELOPERS_GUIDE.pdf).