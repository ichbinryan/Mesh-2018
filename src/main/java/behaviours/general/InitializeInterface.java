package behaviours.general;

import agents.HouseAgent;
import behaviours.device.DatabaseAveUpdate;
import devices.PowerHandler;
import jade.core.behaviours.OneShotBehaviour;
import javafx.application.Application;
import meshinterface.model.MeshModel;

/**
 * Created by jeremiah on 5/24/17.
 */
public class InitializeInterface extends OneShotBehaviour {

    private HouseAgent myAgent;

    public InitializeInterface(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {
        MeshModel.getInstance().setMyAgent(myAgent);
        //PowerHandler.setDeviceList(myAgent.getDeviceList());
        PowerHandler.setMyAgent(myAgent);
        DatabaseAveUpdate.setMyAgent(myAgent);
        new Thread(() -> Application.launch(meshfx.Inprogress.Main.class)).start();
    }
}
