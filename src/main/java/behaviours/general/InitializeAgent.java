package behaviours.general;

import agents.HouseAgent;
import factories.DeviceListFactory;
import jade.core.behaviours.OneShotBehaviour;
import utilities.ConstructData;

/**
 * Created by Jeremiah Smith on 1/29/17.
 */
public class InitializeAgent extends OneShotBehaviour {

    private HouseAgent myAgent;

    public InitializeAgent(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {
        myAgent.setNeighbors(ConstructData.getInstance().constructNeighbors());
        myAgent.setDeviceList(ConstructData.getInstance().constructDeviceList());

    }
}
