package behaviours.general;

import actions.Action;
import agents.HouseAgent;
import behaviours.device.ActivateAction;
import behaviours.device.DatabaseAveUpdate;
import behaviours.schedule.MGM;
import behaviours.schedule.ProcessMGMInfoMessage;
import behaviours.testing.GenerateSchedule;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import devices.Actuator;
import devices.Device;
import utilities.LoadProjectProperties;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utilities.Database;

/**
 * Created by jeremiah on 7/21/16.
 */
public class GeneralMessageChecker extends CyclicBehaviour {

    private HouseAgent myAgent;



    public GeneralMessageChecker(HouseAgent myAgent) {
        this.myAgent = myAgent;

    }

    @Override
    public void action() {

        MessageTemplate generalMT;
        MessageTemplate conIDGenSchedule;
        MessageTemplate conIDMGM;
        MessageTemplate conIDActivateAction;
        MessageTemplate orMT;
        MessageTemplate conIDMGMCheck;
        MessageTemplate conIDStartExp;
        MessageTemplate conWriteDatabase;
        MessageTemplate conIDMGMInfo;

        //setup conversation id message templates
        conIDGenSchedule = MessageTemplate.MatchConversationId("Generate-Schedule");
        conIDMGM = MessageTemplate.MatchConversationId("MGM");
        conIDMGMCheck = MessageTemplate.MatchConversationId("MGMCheck");
        conIDStartExp = MessageTemplate.MatchConversationId("StartExp");
        conIDActivateAction = MessageTemplate.MatchConversationId("ActivateAction");
        conIDMGMInfo =  MessageTemplate.MatchConversationId("MGMInfoMessage");

        conWriteDatabase = MessageTemplate.MatchConversationId("Write-Database");

        //setup or message template
        orMT = MessageTemplate.or(conIDGenSchedule, conIDMGM);
        orMT = MessageTemplate.or(orMT, conIDMGMCheck);
        orMT = MessageTemplate.or(orMT, conIDStartExp);
        orMT = MessageTemplate.or(orMT, conIDActivateAction);
        orMT = MessageTemplate.or(orMT, conIDMGMInfo);
        orMT = MessageTemplate.or(orMT, conWriteDatabase);

        //add all MT for general message checking
        generalMT = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST), orMT);


        ACLMessage generalMessage = myAgent.receive(generalMT);

        if(generalMessage != null) {

            if(generalMessage.getPerformative() == ACLMessage.REQUEST) {
                if (generalMessage.getConversationId().equals("Generate-Schedule")) {
                    //todo add handler for Generate Schedule
                    System.out.println("Will generate schedule");
                    myAgent.addBehaviour(new GenerateSchedule(myAgent));
                }

                if (generalMessage.getConversationId().equals("MGM")) {
                    //todo add handler for MGM
                    System.out.println("Will start MGM");
                    myAgent.addBehaviour(new MGM(myAgent, LoadProjectProperties.getInstance().getMgmCycles()));
                }

                if(generalMessage.getConversationId().equals("ActivateAction")) {
                    //todo change this to json content message
                    System.out.println("Will active action");
                    Gson gson = new Gson();

                    JsonElement element = gson.fromJson(generalMessage.getContent(), JsonElement.class);
                    JsonObject actionInfo = element.getAsJsonObject();



                    int deviceID = Integer.parseInt(actionInfo.get("Device_ID").getAsString());
                    int actionID = Integer.parseInt(actionInfo.get("Action_ID").getAsString());


                    Device device = myAgent.getDeviceList().getDevice(deviceID);
                    if( device instanceof Actuator) {
                        Actuator actuator = (Actuator) device;
                        actuator.activate(actuator.getActionList().getAction(actionID));
                    }

                    Actuator actuator = (myAgent.getDeviceList().getDevice(deviceID) instanceof Actuator)? (Actuator) myAgent.getDeviceList().getDevice(deviceID): null;
                    Action action = actuator.getActionList().getAction(actionID);

                    if((!(actuator == null)) && (!(action == null))) {
                        myAgent.addBehaviour(new ActivateAction(myAgent, actuator, action));
                    }

                }

                if(generalMessage.getConversationId().equals("conWriteDatabase")){
                    System.out.println("Will write to database");
                    DatabaseAveUpdate.getOurInstance().updateHourly();
                }


                if(generalMessage.getConversationId().equalsIgnoreCase("MGMInfoMessage")) {
                    myAgent.addBehaviour(new ProcessMGMInfoMessage(myAgent,generalMessage.getContent(), generalMessage.getSender()));
                }

            }
        }
        else {
            block();
        }


    }

}
