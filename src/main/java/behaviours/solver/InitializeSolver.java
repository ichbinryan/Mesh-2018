package behaviours.solver;

import agents.HouseAgent;
import devices.Actuator;
import devices.Device;
import devices.Sensor;
import factories.PredictiveModelFactory;
import jade.core.behaviours.OneShotBehaviour;
import models.PredictiveModel;
import rules.SchedulingRule;
import solver.solver;
import utilities.ConstructData;
import utilities.LoadProjectProperties;

import java.util.ArrayList;

/**
 * Created by jeremiah on 1/29/17.
 */
public class InitializeSolver extends OneShotBehaviour {


    private HouseAgent myAgent;
    private double[] bgLoads;


    public InitializeSolver(HouseAgent myAgent) {
        this.myAgent = myAgent;

        bgLoads = new double[LoadProjectProperties.getInstance().getTimeSteps()];

        for(int i = 0; i < bgLoads.length; i++) {
            bgLoads[i]= 0.0;
        }
    }

    public InitializeSolver(HouseAgent myAgent, double[] bgLoads) {
        this.myAgent = myAgent;
        this.bgLoads = bgLoads;
    }

    @Override
    public void action() {
        if(myAgent.getAgentDebugLevel() > 0) {
            printSeparator();
            System.out.println("Starting initialize solver behavior");
        }

        ArrayList<Actuator> actuators = new ArrayList<>();
        ArrayList<Sensor> sensors = new ArrayList<>();

        if(myAgent.getAgentDebugLevel() > 0) {
            System.out.println("Spiting Device List into actuators and sensors");
        }

        for(Device device : myAgent.getDeviceList().getDeviceHashMap().values()) {
            if (device instanceof Sensor) {
                sensors.add((Sensor)device);
            }
            else {
                actuators.add((Actuator) device);
            }
        }

        for( Sensor sensor : sensors) {
            PredictiveModel m = PredictiveModelFactory.constructPredictiveModel(sensor, actuators);
            System.out.println(m.toString());
        }

        if(myAgent.getAgentDebugLevel() > 0) {
            System.out.println("Getting rules from database");
        }

        ArrayList<SchedulingRule> rules = ConstructData.getInstance().constructSchedulingRules();



        if(myAgent.getAgentDebugLevel() > 0) {
            for (SchedulingRule sr : rules) {
                System.out.println(sr.toString());
            }
        }
        //myAgent.setSolver(new solver(rules, bgLoads));
    }

    private void printSeparator() {
        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }
        System.out.println();
    }
}
