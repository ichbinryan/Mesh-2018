package behaviours.device;

import agents.HouseAgent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**
 * Created by jeremiah on 6/6/17.
 */
public class StoreHourlyPowerConsumption extends OneShotBehaviour {

    private HouseAgent myAgent;

    public StoreHourlyPowerConsumption(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {

    }
}
