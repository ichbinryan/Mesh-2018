package behaviours.device;

import actions.Action;
import agents.HouseAgent;

import devices.Actuator;
import jade.core.behaviours.*;

public class ActivateAction extends OneShotBehaviour {

    private HouseAgent myAgent;
    private Action actionToActivate;
    private Actuator device;

    public ActivateAction(HouseAgent myAgent, Actuator device, Action actionToActivate) {
        this.myAgent = myAgent;
        this.actionToActivate = actionToActivate;
        this.device = device;
    }
    
    public void action() {

        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }

        device.activate(actionToActivate);



        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }
        System.out.println();

    }
}