package behaviours.device;

import agents.HouseAgent;
import devices.Actuator;
import devices.Device;
import devices.DeviceList;
import devices.TimeStep;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import utilities.Database;
import utilities.LoadProjectProperties;
import java.util.HashMap;


/**
 * Created by ryanread on 4/24/17.
 */

//todo:  Do I need to pass agent device list?
public class DatabaseAveUpdate extends OneShotBehaviour{

    private static DatabaseAveUpdate ourInstance = new DatabaseAveUpdate();
    private static HouseAgent myAgent;
    private static DeviceList deviceList;
    private Database dbase;
    private TimeStep timeStep = TimeStep.getOurInstance();

    //public void insertData(String tableName, HashMap<String, String> data)
    private DatabaseAveUpdate(){
        dbase = new Database();
        deviceList = new DeviceList();
        myAgent = new HouseAgent();
        //dbase = new Database();
    }

    /*private DatabaseAveUpdate(DeviceList d){

    }*/

    public static void setMyAgent(HouseAgent agent){
        ourInstance.myAgent = agent;
        ourInstance.deviceList = myAgent.getDeviceList();
        //This should be handled in the agent initialization
        //timeStep.setAgent(myAgent);
    }

    public static DatabaseAveUpdate getOurInstance(){
        return ourInstance;
    }

    //todo:differentiate between sim and real time modes.

    /**
     * Should get all devices in device list and update their hourly time step in the
     * database
     */
    public void updateHourly(){
        System.out.println("******************************\n" +
                           "***UPDATING DATABASE HOURLY***\n" +
                           "******************************");
        String tableName = "Hourly_Power_Consumption";
        HashMap <String, String> data = new HashMap();


            //if(LoadProjectProperties.getInstance().isSimulation()) {
                for(Device d: ourInstance.deviceList.getDeviceHashMap().values()) {
                    if (d instanceof Actuator) {
                        //int time = timeStep.getTimeStep();

                        DateTime temp = new DateTime();

                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                        int simTime = timeStep.getTimeStep() * 2;
                        //TODO: fix string to get other things outside fo time step.
                        String dateString = temp.getYear() + "-" + temp.getMonthOfYear() + "-" + temp.getDayOfMonth() + " "
                                + simTime + ":00:00";
                        DateTime dt = fmt.parseDateTime(dateString);

                        dbase.updateHourly(fmt.print(dt), ((Actuator) d).getUsage(), d.getID(), d.getName()); //TODO test this

                    }
                }
            //}

            /*else if(!LoadProjectProperties.getInstance().isSimulation()){
                //write to hourly here
                for(Device d: ourInstance.deviceList.getDevices()) {
                    if (d instanceof Actuator) {
                        DateTime timestamp = DateTime.now();
                        //System.out.println(timestamp.toString());
                        //YYYY-MM-DD HH:MI:SS
                        //Date time formatter will change date time to match the pattern used for sql
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                        //System.out.println(fmt.print(timestamp));
                        String dt = fmt.print(timestamp);
                        dbase.updateHourly(dt, (float) ((Actuator) d).getDeviceAverage().getTracker().get(timestamp.getHourOfDay()), d.getID());
                    }
                }
            }*/
    }

    @Override
    public void action() {
        updateHourly();
    }
}
