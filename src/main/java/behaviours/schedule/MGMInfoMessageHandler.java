package behaviours.schedule;

import agents.HouseAgent;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jade.core.AID;
import mgm.MGMInfo;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 7/27/16.
 */
public class MGMInfoMessageHandler extends CyclicBehaviour {

    private HouseAgent myAgent;
    private int debugLevel;
    private Gson gson;

    public MGMInfoMessageHandler(HouseAgent myAgent) {

        this.myAgent = myAgent;
        this.debugLevel = myAgent.getAgentDebugLevel();
        gson = new Gson();
    }

    @Override
    public void action() {

        //setting up message templates
        MessageTemplate mgmInfoTemplate;
        mgmInfoTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
        mgmInfoTemplate = MessageTemplate.and(MessageTemplate.MatchConversationId("MGM-Info"), mgmInfoTemplate);

        //check message queue for matching message
        ACLMessage mgmInfoMessage = myAgent.receive(mgmInfoTemplate);

        //handle message
        if (mgmInfoMessage != null) {
            if(debugLevel > 0) {
                this.printSeparator(50, "+");
                System.out.println("MGM Info Message Received");
            }

            JsonObject mgmInfo = gson.fromJson(mgmInfoMessage.getContent(), JsonObject.class);

            if(debugLevel >= 3) {
                System.out.println("After Conversion of message content to JSON object.");
                System.out.println(mgmInfo.toString());
            }

            int cycleNumber = mgmInfo.get("Cycle").getAsInt();
            double gain = mgmInfo.get("Gain").getAsDouble();
            JsonArray energyProfile = mgmInfo.get("EP").getAsJsonArray();
            double[] ep = new double[LoadProjectProperties.getInstance().getTimeSteps()];

            if(ep.length == energyProfile.size()) {
                for(int i = 0; i < ep.length; i++){
                    ep[i] = energyProfile.get(i).getAsDouble();
                }
            }

            HashMap<AID, MGMInfo> values = new HashMap<>();
            values.put(mgmInfoMessage.getSender(), new MGMInfo(cycleNumber, gain, ep));

            if(myAgent.getMGMInformation().get(cycleNumber) != null) {
                myAgent.getMGMInformation().get(cycleNumber).add(values);
            }
            else {
                ArrayList<HashMap<AID, MGMInfo>> mgmInfoALHM = new ArrayList<>();
                mgmInfoALHM.add(values);
                myAgent.getMGMInformation().put(cycleNumber, mgmInfoALHM);
            }

            if(debugLevel > 0) {
                if(debugLevel >= 3){
                    System.out.println("After Conversion of JSON content to HashMap Object.");
                    System.out.println(myAgent.getMGMInformation().get(cycleNumber).toString());
                }
                System.out.println("MGM Info processed");
                this.printSeparator(50, "+");
            }
        }
        else {
            this.block();
        }



    }

    private void printSeparator(int numberOfStars, String separator) {
        System.out.println();
        for (int i = 0; i < numberOfStars; i++) {
            System.out.print(separator);
        }
        System.out.println();
    }
}
