package behaviours.schedule;

import agents.HouseAgent;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import mgm.MGMInfo;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 6/9/17.
 */
public class ProcessMGMInfoMessage extends OneShotBehaviour {

    private HouseAgent myAgent;
    private String gainInfo;
    private AID neighbor;
    private Gson gson = new Gson();


    public ProcessMGMInfoMessage(HouseAgent myAgent, String gainInfo, AID neighbor) {
        this.myAgent = myAgent;
        this.gainInfo = gainInfo;
        this.neighbor = neighbor;
    }

    @Override
    public void action() {

        JsonObject mgmInformation = gson.fromJson(gainInfo, JsonObject.class);

        int cycleNumber = mgmInformation.get("Cycle").getAsInt();
        double gain = mgmInformation.get("Gain").getAsDouble();
        double [] ep = new double[LoadProjectProperties.getInstance().getTimeSteps()];

        JsonArray energyProfile = mgmInformation.get("ep").getAsJsonArray();

        for(int i = 0; i < energyProfile.size(); i++) {
            ep[i] = energyProfile.get(i).getAsDouble();
        }

        MGMInfo mgmInfo = new MGMInfo(cycleNumber, gain, ep);

        HashMap<AID, MGMInfo> mgmAID_MGMInfoHM = new HashMap<>();
        mgmAID_MGMInfoHM.put(neighbor, mgmInfo);

        if(!myAgent.getMGMInformation().get(cycleNumber).isEmpty()) {
            if(!myAgent.getMGMInformation().get(cycleNumber).contains(mgmAID_MGMInfoHM))
                myAgent.getMGMInformation().get(cycleNumber).add(mgmAID_MGMInfoHM);
        }
        else {
            myAgent.getMGMInformation().put(cycleNumber, new ArrayList<>());
            myAgent.getMGMInformation().get(cycleNumber).add(mgmAID_MGMInfoHM);
        }



    }
}
