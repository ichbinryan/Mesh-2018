package behaviours.schedule;

import agents.HouseAgent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import utilities.ConstructData;
import utilities.Database;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 7/21/16.
 */
public class PopulateNeighborList extends OneShotBehaviour {

    private HouseAgent myAgent;


    public PopulateNeighborList (HouseAgent myAgent) {
        this.myAgent = myAgent;

    }

    @Override
    public void action() {

        myAgent.setNeighbors(ConstructData.getInstance().constructNeighbors());

    }
}
