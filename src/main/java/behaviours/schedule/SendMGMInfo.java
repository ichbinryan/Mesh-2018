package behaviours.schedule;

import agents.HouseAgent;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jade.core.AID;
import mgm.MGMInfo;
import neighbor.Neighbor;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.io.IOException;

/**
 * Created by Jeremiah Smith on 7/27/16.
 */
public class SendMGMInfo extends OneShotBehaviour {

    private HouseAgent myAgent;
    private MGMInfo mgmInfo;
    private int cycleNumber;
    private double gain;
    private double ep[];


    public SendMGMInfo(HouseAgent myAgent, int cycleNumber, double gain, double[] ep) {
        this.myAgent = myAgent;
        this.cycleNumber = cycleNumber;
        this.gain = gain;
        this.ep = ep;
    }

    @Override
    public void action() {

        if(!myAgent.getNeighbors().isEmpty()) {
            JsonObject mgmJSON = new JsonObject();
            JsonArray epArray = new JsonArray();

            for (double d : ep) {
                epArray.add(d);
            }

            mgmJSON.addProperty("Cycle", cycleNumber);
            mgmJSON.addProperty("Gain", gain);
            mgmJSON.add("EP", epArray);
            //create message
            ACLMessage mgmInfoMessage = new ACLMessage(ACLMessage.INFORM);

            //set conversation id
            mgmInfoMessage.setConversationId("MGM-Info");

            //add receivers to message
            for (AID aid : myAgent.getNeighbors().values()) {
                mgmInfoMessage.addReceiver(aid);
            }

            mgmInfoMessage.setContent(mgmJSON.toString());

            //send message
            myAgent.send(mgmInfoMessage);
        }


    }
}
