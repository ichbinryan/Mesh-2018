package behaviours.schedule;

import agents.HouseAgent;
import agents.states.MGMState;
import jade.core.AID;
import mgm.MGMInfo;
import schedule.Schedule;
import utilities.LoadProjectProperties;
import jade.core.behaviours.Behaviour;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

/**
 * Created by Jeremiah Smith on 7/21/16.
 */
public class MGM extends Behaviour {

    private HouseAgent myAgent;
    private int cycleNumber;
    private int maxCycles;
    private int debugLevel;
    private Schedule currentSchedule;
    private Schedule newSchedule;
    private final LoadProjectProperties LPP;
    private double gain;
    ArrayList<Double> gainsReceived = new ArrayList<>();
    private Random random = new Random();
    private Instant mgmStart;
    private Instant mgmEnd;
    private Instant cycleStart;
    private Instant cycleEnd;
    private Instant solverStart;
    private Instant solverEnd;


    public MGM(HouseAgent myAgent, int maxCycles) {

        mgmStart = Instant.now();
        this.LPP = LoadProjectProperties.getInstance();
        this.myAgent = myAgent;
        this.maxCycles = maxCycles;
        this.gain = 0.0;
        this.debugLevel = myAgent.getAgentDebugLevel();

    }

    @Override
    public void action() {

        try {

            MGMState currentState = myAgent.getMgmState();

            if(debugLevel > 0) {
                System.out.println("Current MGM State: " + currentState);
            }

            switch(currentState) {
                case INITIALIZE: {
                    this.initialize();
                    break;
                }

                case GENERATING_SCHEDULE:{
                    //needed?

                    break;
                }

                case WAITING_FOR_GAIN: {
                    if (gainMessageCheck()) myAgent.setMgmState(MGMState.GAIN_MESSAGE_RECEIVED);
                    else this.block(5000);
                    break;
                }

                case GAIN_MESSAGE_RECEIVED: {
                    this.performCycle();
                    break;
                }

                case GAIN_MESSAGE_NOT_NEEDED: {
                    this.performCycle();
                    break;
                }

                default:
                    this.block(5000);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean done() {

        if(cycleNumber == maxCycles) {
            mgmEnd = Instant.now();
            Duration mgmTotalDuration  = Duration.between(mgmStart, mgmEnd);
            System.out.println("Total MGM Duration: " + mgmTotalDuration.getSeconds());
            return true;
        }
        return false;
    }

    private void initialize() {
        cycleStart = Instant.now();

        myAgent.setMgmState(MGMState.GENERATING_SCHEDULE);
        solverStart = Instant.now();
        currentSchedule = myAgent.getSolver().getFirstSchedule();
        solverEnd = Instant.now();

        myAgent.addBehaviour(new SendMGMInfo(myAgent, cycleNumber, gain, currentSchedule.getEnergyProfile()));

        this.setNextState();

        cycleEnd = Instant.now();

        if(debugLevel > 0 ) {
            this.printSeparator();
            System.out.println("Starting MGM Behavior Cycle: " + cycleNumber);
            System.out.println(currentSchedule.toString());
            System.out.println("Total Cycle Time: " + Duration.between(cycleStart, cycleEnd).getNano() * 1000 + " ms");
            System.out.println("Ending Cycle: " + cycleNumber);
            this.printSeparator();
        }

        cycleNumber++;
    }

    private void performCycle() {

        cycleStart = Instant.now();
        double aggregatedPowerConsumption[] = new double[LoadProjectProperties.getInstance().getTimeSteps()];

        Comparator<Double> comparator = (o1, o2) -> {
            if (o1 < o2) return 1;
            if (o1 > o2) return -1;
            return 0;
        };

        PriorityQueue<Double> gains = new PriorityQueue<>(comparator);

        for(int i = 0; i < aggregatedPowerConsumption.length; i++) {
            aggregatedPowerConsumption[i] = 0.0;
        }

        if(debugLevel > 0 ) {
            this.printSeparator();
            System.out.println("Starting MGM Behavior Cycle: " + cycleNumber);
            System.out.println("Generating New Schedule");
        }

        if(!myAgent.getNeighbors().isEmpty()) {
            ArrayList<HashMap<AID, MGMInfo>> previousCycleInfo = myAgent.getMGMInfoCycle(cycleNumber - 1);

            //sum all power consumption
            for (HashMap<AID, MGMInfo> aid_mgm_InfoHashMap : previousCycleInfo) {
                for (MGMInfo mgmInfo : aid_mgm_InfoHashMap.values()) {
                    gains.add(mgmInfo.getGain());
                    double neighborEnergyProfile[] = mgmInfo.getEnergyProfile();
                    for (int i = 0; i < neighborEnergyProfile.length; i++) {
                        aggregatedPowerConsumption[i] += neighborEnergyProfile[i];
                    }
                }
            }
        }


        myAgent.setMgmState(MGMState.GENERATING_SCHEDULE);
        solverStart = Instant.now();
        newSchedule = myAgent.getSolver().getSchedule(aggregatedPowerConsumption);
        solverEnd = Instant.now();

        if(debugLevel > 0 ) System.out.println("New Schedules Utility: " + newSchedule.getUtility());
        if(debugLevel > 0 ) System.out.println("Calculating gain of new schedule vs current schedule");

        gain = this.calculateGain(newSchedule.getUtility(), currentSchedule.getUtility());

        if(debugLevel > 0 ) System.out.println("Gain form calculation: " + gain);

        if(!gains.isEmpty()) {
            if (debugLevel > 0) System.out.println("Gains Not Empty comparing");
            if (gain > gains.peek()) {
                currentSchedule = newSchedule;
                if (debugLevel > 0) System.out.println("I have best gain taking new schedule");
            }
        }
        else {
            if(gain > 0) {
                currentSchedule = newSchedule;
                if (debugLevel > 0) System.out.println("I have best gain taking new schedule");
            }
        }

        cycleEnd = Instant.now();
        myAgent.setCurrentSchedule(currentSchedule);
        myAgent.addBehaviour(new SendMGMInfo(myAgent, cycleNumber, gain, currentSchedule.getEnergyProfile()));
        cycleNumber++;

        this.setNextState();
    }

    private boolean gainMessageCheck() {

        //if no neighbors return true not waiting for gain messages
        if(myAgent.getNeighbors().isEmpty()) return true;

        int neighborCount = myAgent.getNeighbors().size();
        int gainMessageCount = myAgent.getMGMInformation().get(cycleNumber).size();

        if(neighborCount == gainMessageCount) return true;

        return false;
    }


    private void printSeparator() {
        System.out.println();
        for (int i = 0; i < 50; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    private void setNextState() {
        if(myAgent.getNeighbors().isEmpty()) {
            myAgent.setMgmState(MGMState.GAIN_MESSAGE_NOT_NEEDED);
        }
        else {
            myAgent.setMgmState(MGMState.WAITING_FOR_GAIN);
        }
    }

    private double calculateGain(double newUtility, double currentUtility) {
        return newUtility - currentUtility;
    }
}
