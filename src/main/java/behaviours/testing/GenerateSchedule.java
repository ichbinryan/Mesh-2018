package behaviours.testing;

import agents.HouseAgent;
import devices.DeviceList;
import schedule.Schedule;
import utilities.LoadProjectProperties;
import jade.core.behaviours.OneShotBehaviour;
import java.io.*;
import java.time.Instant;
import java.util.Random;

/**
 * Created by Jeremiah Smith on 6/30/16.
 */
public class GenerateSchedule extends OneShotBehaviour {

    private HouseAgent myAgent;
    private Schedule currentSchedule;
    private int debugLevel;

    public GenerateSchedule(HouseAgent myAgent) {

        this.myAgent = myAgent;
        this.debugLevel = myAgent.getAgentDebugLevel();
    }

    @Override
    public void action() {

        try {

            if(debugLevel > 0) System.out.println("Attempting to get schedule!");
            currentSchedule = myAgent.getSolver().getFirstSchedule();
            myAgent.setCurrentSchedule(currentSchedule);
            if(debugLevel > 0) System.out.println("Schedule complete!");

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}
