package behaviours.testing;

import agents.HouseAgent;
import behaviours.device.DatabaseAveUpdate;
import devices.Actuator;
import devices.Device;
import devices.TimeStep;
import jade.core.behaviours.TickerBehaviour;
import schedule.ScheduleEntry;
import utilities.LoadProjectProperties;

/**
 * Created by Jeremiah Smith on 1/31/17.
 */
public class DaySimulation extends TickerBehaviour {

    int timeSteps = LoadProjectProperties.getInstance().getTimeSteps();
    int currentTimeStep;

    private HouseAgent myAgent;

    public DaySimulation(HouseAgent myAgent, long period) {
        super(myAgent, period);
        this.myAgent = myAgent;
        currentTimeStep = 0;
        myAgent.setSimCycle(0);
    }

    @Override
    protected void onTick() {
        if(currentTimeStep >= timeSteps) {
            currentTimeStep = 0;
        }

        System.out.println("Current TS: " + currentTimeStep);

        if(myAgent.getCurrentSchedule() != null) {
            for(int deviceID : myAgent.getCurrentSchedule().getScheduleEntryMap().keySet()) {
                Device device = myAgent.getDeviceList().getDevice(deviceID);
                if( device instanceof Actuator) {
                    Actuator actuator = (Actuator) device;
                    if(myAgent.getAgentDebugLevel() > 0) {
                        System.out.print("Activating Device: " + actuator.getName());
                        //System.out.println("Power Consumption: " + actuator.getUsage());
                    }
                    actuator.activate(myAgent.getCurrentSchedule().getScheduleEntryMap().get(deviceID).getScheduledAction(currentTimeStep));
                    if(myAgent.getAgentDebugLevel() > 0) {
                        //System.out.println("Activating Device: " + actuator.getName());
                        System.out.println(" with power Consumption: " + actuator.getUsage());
                    }
                }
            }
        }

        //update database with power used
        DatabaseAveUpdate.getOurInstance().updateHourly();
        //myAgent.addBehaviour(new DatabaseAveUpdate());
        TimeStep.getOurInstance().setTimeStep(currentTimeStep);
        currentTimeStep++;

    }
}
