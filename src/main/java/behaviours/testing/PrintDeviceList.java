package behaviours.testing;

import agents.HouseAgent;

import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by Jeremiah Smith on 5/23/16.
 */
public class PrintDeviceList extends OneShotBehaviour {

    private HouseAgent myAgent;

    public PrintDeviceList(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {
        myAgent.getDeviceList().printDevices();
    }
}
