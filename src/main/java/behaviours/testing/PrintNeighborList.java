package behaviours.testing;

import agents.HouseAgent;
import neighbor.Neighbor;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Testing Behavior to print neighbor list.
 * Created by Jeremiah Smith on 5/23/16.
 */
public class PrintNeighborList extends OneShotBehaviour {

    private HouseAgent myAgent;

    public PrintNeighborList(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {

        if(myAgent.getNeighbors().size() > 0) {
            System.out.println(myAgent.getNeighbors().toString());
        }
        else {
            System.out.println("No Neighbors to Print");
        }

    }
}
