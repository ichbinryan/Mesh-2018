package behaviours.testing;

import agents.HouseAgent;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by Jeremiah Smith on 2/9/17.
 */
public class PrintDebugMessage extends OneShotBehaviour {

    private Agent myAgent;
    private String message;
    private int separatorLength;
    private String separatorCharacter;

    public PrintDebugMessage(Agent myAgent, String message, String separatorCharacter) {
        this.separatorCharacter = separatorCharacter;
        this.myAgent = myAgent;
        this.message = message;
        this.separatorLength = 100;



    }

    @Override
    public void action() {
        this.printSeparator();
        System.out.println();
        this.printMessageLine();
        this.printSeparator();
        System.out.println();
        System.out.println();

    }

    private void printSeparator() {
        for(int i = 0; i < separatorLength; i++) {
            System.out.print(separatorCharacter);
        }

    }

    private void printMessageLine() {

        System.out.printf("%s%s %-95s%s%s%n", separatorCharacter, separatorCharacter, message, separatorCharacter, separatorCharacter);
    }
}
