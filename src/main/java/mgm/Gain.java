package mgm;

import java.io.Serializable;

/**
 * Created by Jeremiah Smith on 7/11/16.
 */
public class Gain implements Serializable {

    private int cycleNumber;
    private double gain;



    public Gain(int cycleNumber, double gain) {
        this.cycleNumber = cycleNumber;
        this.gain = gain;
    }

    public int getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(int cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }

    @Override
    public boolean equals(Object obj) {
        Gain temp = null;
        if(obj instanceof Gain) {
            temp = (Gain) obj;
        }
        return (this.cycleNumber == temp.cycleNumber) && (this.gain == temp.gain);

    }

    @Override
    public String toString() {
        return "Gain{" +
                "cycleNumber=" + cycleNumber +
                ", gain=" + gain +
                '}';
    }
}
