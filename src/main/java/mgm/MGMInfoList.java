package mgm;


import utilities.LoadProjectProperties;

import java.util.ArrayList;

/**
 * Created by Jerem on 7/26/2016.
 */
public class MGMInfoList {

    private ArrayList<MGMInfo> mgmInfoList;

    private final LoadProjectProperties LPP;

    public MGMInfoList() {
        mgmInfoList = new ArrayList();
        LPP = LoadProjectProperties.getInstance();
    }

    public MGMInfoList(ArrayList mgmInfo) {

        this.mgmInfoList = mgmInfo;
        LPP = LoadProjectProperties.getInstance();
    }

    public void addMGMInfo(MGMInfo mgmInfo) {
        System.out.println("Adding MGMInfo to List");
        this.mgmInfoList.add(mgmInfo);

        if(this.mgmInfoList.contains(mgmInfo)) {
            System.out.println("MGM Info Added");
        }
    }

    public boolean hasReceivedMGMInfo(int cycleNumber) {

        for(MGMInfo mgm : mgmInfoList) {
            if (mgm.getCycleNumber() == cycleNumber) {
                return true;
            }
        }
        return false;
    }
    public double[] getSpecificEP(int cycleNumber) {
        for(MGMInfo mgm : mgmInfoList) {
            if (mgm.getCycleNumber() == cycleNumber) {
                return mgm.getEnergyProfile();
            }
        }

        return null;
    }

}
