package mgm;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Jeremiah Smith on 7/26/2016.
 */
public class MGMInfo implements Serializable {

    private final int cycleNumber;
    private final double gain;
    private final double [] energyProfile;

    public MGMInfo(int cycleNumber, double gain, double[] energyProfile) {
        this.cycleNumber = cycleNumber;
        this.gain = gain;
        this.energyProfile = energyProfile;
    }

    public int getCycleNumber() {
        return cycleNumber;
    }

    public double getGain() {
        return gain;
    }

    public double[] getEnergyProfile() {
        return energyProfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MGMInfo mgmInfo = (MGMInfo) o;

        if (cycleNumber != mgmInfo.cycleNumber) return false;
        if (Double.compare(mgmInfo.gain, gain) != 0) return false;
        return Arrays.equals(energyProfile, mgmInfo.energyProfile);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = cycleNumber;
        temp = Double.doubleToLongBits(gain);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + Arrays.hashCode(energyProfile);
        return result;
    }

    @Override
    public String toString() {
        return "MGMInfo{" +
                "cycleNumber=" + cycleNumber +
                ", gain=" + gain +
                ", energyProfile=" + Arrays.toString(energyProfile) +
                '}';
    }
}
