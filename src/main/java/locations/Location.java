package locations;

/**
 * Created by Jeremiah Smith on 11/11/16.
 */
public class Location {

    private int locationID;
    private String locationName;
    private LocationType locationType;


    public Location(int locationID, String locationName, LocationType locationType) {
        this.locationID = locationID;
        this.locationName = locationName;
        this.locationType = locationType;
    }

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (locationID != location.locationID) return false;
        if (locationName != null ? !locationName.equals(location.locationName) : location.locationName != null)
            return false;
        return locationType == location.locationType;
    }

    @Override
    public int hashCode() {
        int result = locationID;
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        result = 31 * result + (locationType != null ? locationType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Location{" +
                "locationID=" + locationID +
                ", locationName='" + locationName + '\'' +
                ", locationType=" + locationType +
                '}';
    }
}
