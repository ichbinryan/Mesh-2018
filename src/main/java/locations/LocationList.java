package locations;


import java.util.HashMap;

/**
 * Created by jeremiah on 7/10/17.
 */
public class LocationList {

    private HashMap<Integer, Location> locationHashMap;

    public LocationList(HashMap<Integer, Location> locationHashMap) {
        this.locationHashMap = locationHashMap;
    }

    public HashMap<Integer, Location> getLocationHashMap() {
        return locationHashMap;
    }

    public void setLocationHashMap(HashMap<Integer, Location> locationHashMap) {
        this.locationHashMap = locationHashMap;
    }

    public Location getLocation(int locationID) {
        return locationHashMap.get(locationID);
    }
}
