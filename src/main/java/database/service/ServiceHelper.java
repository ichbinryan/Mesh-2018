package database.service;

import com.google.gson.*;
import database.model.*;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.lang.reflect.Type;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Service Helper is a singleton which builds the Retrofit instance and adapter for API calls
 * <p>
 * @author Stepheny
 * @since 1.20.18
 */
public class ServiceHelper {
    private static ServiceHelper instance       = new ServiceHelper();
    private static final String ENDPOINT        = "http://localhost:8080/";
    private static final int CONNECT_TIMEOUT    = 30;
    private static final int WRITE_TIMEOUT      = 30;
    private static final int READ_TIMEOUT       = 30;
    private static OkHttpClient httpClient;
    private IService service;

    public static ServiceHelper getInstance() {
        return instance;
    }

    private ServiceHelper() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();
        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(IService.class);
    }

    private Retrofit.Builder createAdapter() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        return new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    /*
     * CommunicationProtocols Calls
     */

    public Call<List<CommunicationProtocols>> getAllCommunicationProtocols() {
        return service.getAllCommunicationProtocol();
    }

    public Call<CommunicationProtocols> getCommunicationProtocols(int communicationProtocolId) {
        return service.getCommunicationProtocol(communicationProtocolId);
    }

    public Call<Void> addCommunicationProtocols(CommunicationProtocols communicationProtocol) {
        return service.addCommunicationProtocol(communicationProtocol);
    }

    public Call<Void> modifyCommunicationProtocols(CommunicationProtocols communicationProtocol) {
        return service.modifyCommunicationProtocol(communicationProtocol);
    }

    public Call<Void> deleteCommunicationProtocols(Integer communicationProtocolId) {
        return service.deleteCommunicationProtocol(communicationProtocolId);
    }
    
    /*
     * DeviceTable Calls
     */

    public Call<List<DeviceTable>> getAllDevices() {
        return service.getAllDevices();
    }

    public Call<DeviceTable> getDevice(int deviceId) {
        return service.getDevice(deviceId);
    }

    public Call<Void> addDevice(DeviceTable device) {
        return service.addDevice(device);
    }

    public Call<Void> modifyDevice(DeviceTable device) {
        return service.modifyDevice(device);
    }

    public Call<Void> deleteDevice(Integer deviceId) {
        return service.deleteDevice(deviceId);
    }

    /*
     * DailyPowerConsumption Calls
     */

    public Call<List<DailyPowerConsumption>> getAllDailyPowerConsumption() {
        return service.getAllDailyPowerConsumption();
    }

    public Call<DailyPowerConsumption> getDailyPowerConsumption(int deviceId, Date timestamp) {
        return service.getDailyPowerConsumption(deviceId, timestamp);
    }

    public Call<Void> addDailyPowerConsumption(DailyPowerConsumption dpc) {
        return service.addDailyPowerConsumption(dpc);
    }

    public Call<Void> modifyDailyPowerConsumption(DailyPowerConsumption dpc) {
        return service.modifyDailyPowerConsumption(dpc);
    }

    public Call<Void> deleteDailyPowerConsumption(Integer deviceId, Date timestamp) {
        return service.deleteDailyPowerConsumption(deviceId, timestamp);
    }
    
    /*
     * DeviceActionRelation Calls
     */

    public Call<List<DeviceActionRelation>> getAllDeviceActionRelation() {
        return service.getAllDeviceActionRelation();
    }

    public Call<DeviceActionRelation> getDeviceActionRelation(int deviceId, int actionId) {
        return service.getDeviceActionRelation(deviceId, actionId);
    }

    public Call<Void> addDeviceActionRelation(DeviceActionRelation dar) {
        return service.addDeviceActionRelation(dar);
    }

    public Call<Void> modifyDeviceActionRelation(DeviceActionRelation dar) {
        return service.modifyDeviceActionRelation(dar);
    }

    public Call<Void> deleteDeviceActionRelation(Integer deviceId, int actionId) {
        return service.deleteDeviceActionRelation(deviceId, actionId);
    }
    
    /*
     * DeviceActionSpDelta Calls
     */

    public Call<List<DeviceActionSpDelta>> getAllDeviceActionSpDelta() {
        return service.getAllDeviceActionSpDelta();
    }

    public Call<DeviceActionSpDelta> getDeviceActionSpDelta(int deviceId, int actionId, int sensorPropId) {
        return service.getDeviceActionSpDelta(deviceId, actionId, sensorPropId);
    }

    public Call<Void> addDeviceActionSpDelta(DeviceActionSpDelta dasd) {
        return service.addDeviceActionSpDelta(dasd);
    }

    public Call<Void> modifyDeviceActionSpDelta(DeviceActionSpDelta dasd) {
        return service.modifyDeviceActionSpDelta(dasd);
    }

    public Call<Void> deleteDeviceActionSpDelta(Integer deviceId, int actionId, int sensorPropId) {
        return service.deleteDeviceActionSpDelta(deviceId, actionId, sensorPropId);
    }
    
    /*
     * DeviceCommunicationRelation Calls
     */

    public Call<List<DeviceCommunicationRelation>> getAllDeviceCommunicationRelation() {
        return service.getAllDeviceCommunicationRelation();
    }

    public Call<DeviceCommunicationRelation> getDeviceCommunicationRelation(int deviceId, int protocolId) {
        return service.getDeviceCommunicationRelation(deviceId, protocolId);
    }

    public Call<Void> addDeviceCommunicationRelation(DeviceCommunicationRelation dcr) {
        return service.addDeviceCommunicationRelation(dcr);
    }

    public Call<Void> modifyDeviceCommunicationRelation(DeviceCommunicationRelation dcr) {
        return service.modifyDeviceCommunicationRelation(dcr);
    }

    public Call<Void> deleteDeviceCommunicationRelation(Integer deviceId, int protocolId) {
        return service.deleteDeviceCommunicationRelation(deviceId, protocolId);
    }
    
    /*
     * HourlypowerConsumption Calls
     */

    public Call<List<HourlyPowerConsumption>> getAllHourlyPowerConsumption() {
        return service.getAllHourlyPowerConsumption();
    }

    public Call<List<HourlyPowerConsumption>> getAllHourlyPowerConsumption(String timestamp) {
        return service.getAllHourlyPowerConsumption(timestamp);
    }

    public Call<HourlyPowerConsumption> getHourlyPowerConsumption(int deviceId, Date timestamp) {
        return service.getHourlyPowerConsumption(deviceId, timestamp);
    }

    public Call<Void> addHourlyPowerConsumption(HourlyPowerConsumption hpc) {
        return service.addHourlyPowerConsumption(hpc);
    }

    public Call<Void> modifyHourlyPowerConsumption(HourlyPowerConsumption hpc) {
        return service.modifyHourlyPowerConsumption(hpc);
    }

    public Call<Void> deleteHourlyPowerConsumption(Integer deviceId, Date timestamp) {
        return service.deleteHourlyPowerConsumption(deviceId, timestamp);
    }
    
    /*
     * Icons Calls
     */

    public Call<List<Icons>> getAllIcons() {
        return service.getAllIcons();
    }

    public Call<Icons> getIcons(int iconId) {
        return service.getIcons(iconId);
    }

    public Call<Void> addIcons(Icons icon) {
        return service.addIcons(icon);
    }

    public Call<Void> modifyIcons(Icons icon) {
        return service.modifyIcons(icon);
    }

    public Call<Void> deleteIcons(Integer iconId) {
        return service.deleteIcons(iconId);
    }
    
    /*
     * Locations Calls
     */

    public Call<List<Locations>> getAllLocations() {
        return service.getAllLocations();
    }

    public Call<Locations> getLocations(int locationId) {
        return service.getLocations(locationId);
    }

    public Call<Void> addLocations(Locations location) {
        return service.addLocations(location);
    }

    public Call<Void> modifyLocations(Locations location) {
        return service.modifyLocations(location);
    }

    public Call<Void> deleteLocations(Integer locationId) {
        return service.deleteLocations(locationId);
    }
    
    /*
     * Models Calls
     */

    public Call<List<Models>> getAllModels() {
        return service.getAllModels();
    }

    public Call<Models> getModels(int modelId) {
        return service.getModels(modelId);
    }

    public Call<Void> addModels(Models model) {
        return service.addModels(model);
    }

    public Call<Void> modifyModels(Models model) {
        return service.modifyModels(model);
    }

    public Call<Void> deleteModels(Integer modelId) {
        return service.deleteModels(modelId);
    }
    
    /*
     * Neighbors Calls
     */

    public Call<List<Neighbors>> getAllNeighbors() {
        return service.getAllNeighbors();
    }

    public Call<Neighbors> getNeighbors(int neighborId) {
        return service.getNeighbors(neighborId);
    }

    public Call<Void> addNeighbors(Neighbors neighbor) {
        return service.addNeighbors(neighbor);
    }

    public Call<Void> modifyNeighbors(Neighbors neighbor) {
        return service.modifyNeighbors(neighbor);
    }

    public Call<Void> deleteNeighbors(Integer neighborId) {
        return service.deleteNeighbors(neighborId);
    }
    
    /*
     * RuleTable Calls
     */

    public Call<List<RuleTable>> getAllRuleTable() {
        return service.getAllRuleTable();
    }

    public Call<RuleTable> getRuleTable(int deviceId, Date timestamp) {
        return service.getRuleTable(deviceId, timestamp);
    }

    public Call<Void> addRuleTable(RuleTable rt) {
        return service.addRuleTable(rt);
    }

    public Call<Void> modifyRuleTable(RuleTable rt) {
        return service.modifyRuleTable(rt);
    }

    public Call<Void> deleteRuleTable(Integer deviceId, Date timestamp) {
        return service.deleteRuleTable(deviceId, timestamp);
    }
    
    /*
     * SensorProperties Calls
     */

    public Call<List<SensorProperties>> getAllSensorProperties() {
        return service.getAllSensorProperties();
    }

    public Call<SensorProperties> getSensorProperties(int sensorPropId) {
        return service.getSensorProperties(sensorPropId);
    }

    public Call<Void> addSensorProperties(SensorProperties sensorProperties) {
        return service.addSensorProperties(sensorProperties);
    }

    public Call<Void> modifySensorProperties(SensorProperties sensorProperties) {
        return service.modifySensorProperties(sensorProperties);
    }

    public Call<Void> deleteSensorProperties(Integer sensorPropId) {
        return service.deleteSensorProperties(sensorPropId);
    }
    
    /*
     * Users Calls
     */

    public Call<List<Users>> getAllUsers() {
        return service.getAllUsers();
    }

    public Call<Users> getUsers(int userId) {
        return service.getUsers(userId);
    }

    public Call<Void> addUsers(Users user) {
        return service.addUsers(user);
    }

    public Call<Void> modifyUsers(Users user) {
        return service.modifyUsers(user);
    }

    public Call<Void> deleteUsers(Integer userId) {
        return service.deleteUsers(userId);
    }
}
