package database.service;

import database.model.*;
import retrofit2.Call;
import retrofit2.http.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * IService holds all Service calls to the API
 * <p>
 * @author Stepheny
 * @since 1.20.18
 */
public interface IService {

    /*
     * CommunicationProtocol
     */

    @GET("/api/protocol")
    Call<List<CommunicationProtocols>> getAllCommunicationProtocol();

    @GET("/api/protocol/{protocolId}")
    Call<CommunicationProtocols> getCommunicationProtocol(@Path("protocolId") Integer protocolId);

    @POST("/api/protocol")
    Call<Void> addCommunicationProtocol(@Body CommunicationProtocols dto);

    @PUT("/api/protocol")
    Call<Void> modifyCommunicationProtocol(@Body CommunicationProtocols dto);

    @DELETE("/api/protocol/{protocolId}}")
    Call<Void> deleteCommunicationProtocol(@Path("protocolId") Integer protocolId);

    /*
     *   DeviceTable
     */
    @GET("/api/devices")
    Call<List<DeviceTable>> getAllDevices();

    @GET("/api/devices/{deviceId}")
    Call<DeviceTable> getDevice(@Path("deviceId") Integer deviceId);

    @POST("/api/devices")
    Call<Void> addDevice(@Body DeviceTable dto);

    @PUT("/api/devices")
    Call<Void> modifyDevice(@Body DeviceTable dto);

    @DELETE("/api/devices/{deviceId}")
    Call<Void> deleteDevice(@Path("deviceId") Integer deviceId);

    /*
     *  DailyPowerConsumption
     */

    @GET("/api/dailypowerconsumption")
    Call<List<DailyPowerConsumption>> getAllDailyPowerConsumption();

    @GET("/api/dailypowerconsumption/{deviceId}/{timestamp}")
    Call<DailyPowerConsumption> getDailyPowerConsumption(@Path("deviceId") Integer deviceId, @Path("timestamp") Date timestamp);

    @POST("/api/dailypowerconsumption")
    Call<Void> addDailyPowerConsumption(@Body DailyPowerConsumption dto);

    @PUT("/api/dailypowerconsumption")
    Call<Void> modifyDailyPowerConsumption(@Body DailyPowerConsumption dto);

    @DELETE("/api/dailypowerconsumption/{deviceId}/{timestamp}")
    Call<Void> deleteDailyPowerConsumption(@Path("deviceId") Integer deviceId, @Path("timestamp") Date timestamp);


    /*
     * DeviceActionRelation
     */

    @GET("/api/deviceactionrelation")
    Call<List<DeviceActionRelation>> getAllDeviceActionRelation();

    @GET("/api/deviceactionrelation/{deviceId}/{actionId}")
    Call<DeviceActionRelation> getDeviceActionRelation(@Path("deviceId") Integer deviceId, @Path("actionId") Integer actionId);

    @POST("/api/deviceactionrelation")
    Call<Void> addDeviceActionRelation(@Body DeviceActionRelation dto);

    @PUT("/api/deviceactionrelation")
    Call<Void> modifyDeviceActionRelation(@Body DeviceActionRelation dto);

    @DELETE("/api/deviceactionrelation/{deviceId}/{actionId}")
    Call<Void> deleteDeviceActionRelation(@Path("deviceId") Integer deviceId, @Path("actionId") Integer actionId);

    
    /*
     * DeviceActionSpDelta
     */

    @GET("/api/deviceactionspdelta")
    Call<List<DeviceActionSpDelta>> getAllDeviceActionSpDelta();

    @GET("/api/deviceactionspdelta/{deviceId}/{actionId}/{sensorPropId}")
    Call<DeviceActionSpDelta> getDeviceActionSpDelta(@Path("deviceId") Integer deviceId, @Path("actionId") Integer actionId, @Path("sensorPropId") Integer sensorPropId);

    @POST("/api/deviceactionspdelta")
    Call<Void> addDeviceActionSpDelta(@Body DeviceActionSpDelta dto);

    @PUT("/api/deviceactionspdelta")
    Call<Void> modifyDeviceActionSpDelta(@Body DeviceActionSpDelta dto);

    @DELETE("/api/deviceactionspdelta/{deviceId}/{actionId}/{sensorPropId}")
    Call<Void> deleteDeviceActionSpDelta(@Path("deviceId") Integer deviceId, @Path("actionId") Integer actionId, @Path("sensorPropId") Integer sensorPropId);
    
    /*
     * DeviceCommunicationRelation
     */
    
    @GET("/api/devicecommunicationrelation")
    Call<List<DeviceCommunicationRelation>> getAllDeviceCommunicationRelation();

    @GET("/api/devicecommunicationrelation/{deviceId}/{protocolId}")
    Call<DeviceCommunicationRelation> getDeviceCommunicationRelation(@Path("deviceId") Integer deviceId,  @Path("protocolId") Integer protocolId);

    @POST("/api/devicecommunicationrelation")
    Call<Void> addDeviceCommunicationRelation(@Body DeviceCommunicationRelation dto);

    @PUT("/api/devicecommunicationrelation")
    Call<Void> modifyDeviceCommunicationRelation(@Body DeviceCommunicationRelation dto);

    @DELETE("/api/devicecommunicationrelation/{deviceId}/{protocolId}")
    Call<Void> deleteDeviceCommunicationRelation(@Path("deviceId") Integer deviceId, @Path("protocolId") Integer protocolId);


    /*
     * HourlyPowerConsumption
     */

    @GET("/api/hourlypowerconsumption")
    Call<List<HourlyPowerConsumption>> getAllHourlyPowerConsumption();

    @GET("/api/hourlypowerconsumption/{timestamp}")
    Call<List<HourlyPowerConsumption>> getAllHourlyPowerConsumption(@Path("timestamp") String timestamp);

    @GET("/api/hourlypowerconsumption/{deviceId}/{timestamp}")
    Call<HourlyPowerConsumption> getHourlyPowerConsumption(@Path("deviceId") Integer deviceId,  @Path("timestamp") Date timestamp);

    @POST("/api/hourlypowerconsumption")
    Call<Void> addHourlyPowerConsumption(@Body HourlyPowerConsumption dto);

    @PUT("/api/hourlypowerconsumption")
    Call<Void> modifyHourlyPowerConsumption(@Body HourlyPowerConsumption dto);

    @DELETE("/api/hourlypowerconsumption/{deviceId}/{timestamp}")
    Call<Void> deleteHourlyPowerConsumption(@Path("deviceId") Integer deviceId, @Path("timestamp") Date timestamp);
    
    /*
     * Icons
     */

    @GET("/api/icons")
    Call<List<Icons>> getAllIcons();

    @GET("/api/icons/{iconId}")
    Call<Icons> getIcons(@Path("iconId") Integer iconId);

    @POST("/api/icons")
    Call<Void> addIcons(@Body Icons dto);

    @PUT("/api/icons")
    Call<Void> modifyIcons(@Body Icons dto);

    @DELETE("/api/icons/{iconId}}")
    Call<Void> deleteIcons(@Path("iconId") Integer iconId);
    
    /*
     * Locations
     */

    @GET("/api/locations")
    Call<List<Locations>> getAllLocations();

    @GET("/api/locations/{locationId}")
    Call<Locations> getLocations(@Path("locationId") Integer locationId);

    @POST("/api/locations")
    Call<Void> addLocations(@Body Locations dto);

    @PUT("/api/locations")
    Call<Void> modifyLocations(@Body Locations dto);

    @DELETE("/api/locations/{locationId}}")
    Call<Void> deleteLocations(@Path("locationId") Integer locationId);
    
    /*
     * Models
     */

    @GET("/api/models")
    Call<List<Models>> getAllModels();

    @GET("/api/models/{modelId}")
    Call<Models> getModels(@Path("modelId") Integer modelId);

    @POST("/api/models")
    Call<Void> addModels(@Body Models dto);

    @PUT("/api/models")
    Call<Void> modifyModels(@Body Models dto);

    @DELETE("/api/models/{modelId}}")
    Call<Void> deleteModels(@Path("modelId") Integer modelId);
    
    /*
     * Neighbors
     */

    @GET("/api/neighbors")
    Call<List<Neighbors>> getAllNeighbors();

    @GET("/api/neighbors/{neighborId}")
    Call<Neighbors> getNeighbors(@Path("neighborId") Integer neighborId);

    @POST("/api/neighbors")
    Call<Void> addNeighbors(@Body Neighbors dto);

    @PUT("/api/neighbors")
    Call<Void> modifyNeighbors(@Body Neighbors dto);

    @DELETE("/api/neighbors/{neighborId}}")
    Call<Void> deleteNeighbors(@Path("neighborId") Integer neighborId);
    
    /*
     * RuleTable
     */

    @GET("/api/ruletable")
    Call<List<RuleTable>> getAllRuleTable();

    @GET("/api/ruletable/{deviceId}/{timestamp}")
    Call<RuleTable> getRuleTable(@Path("deviceId") Integer deviceId,  @Path("timestamp") Date timestamp);

    @POST("/api/ruletable")
    Call<Void> addRuleTable(@Body RuleTable dto);

    @PUT("/api/ruletable")
    Call<Void> modifyRuleTable(@Body RuleTable dto);

    @DELETE("/api/ruletable/{deviceId}/{timestamp}")
    Call<Void> deleteRuleTable(@Path("deviceId") Integer deviceId, @Path("timestamp") Date timestamp);
    
    /*
     * SensorProperties
     */

    @GET("/api/sensorproperties")
    Call<List<SensorProperties>> getAllSensorProperties();

    @GET("/api/sensorproperties/{sensorPropId}")
    Call<SensorProperties> getSensorProperties(@Path("sensorPropId") Integer sensorPropId);

    @POST("/api/sensorproperties")
    Call<Void> addSensorProperties(@Body SensorProperties dto);

    @PUT("/api/sensorproperties")
    Call<Void> modifySensorProperties(@Body SensorProperties dto);

    @DELETE("/api/sensorproperties/{sensorPropId}}")
    Call<Void> deleteSensorProperties(@Path("sensorPropId") Integer sensorPropId);


    /*
     * Users
     */

    @GET("/api/users")
    Call<List<Users>> getAllUsers();

    @GET("/api/users/{userId}")
    Call<Users> getUsers(@Path("userId") Integer userId);

    @POST("/api/users")
    Call<Void> addUsers(@Body Users dto);

    @PUT("/api/users")
    Call<Void> modifyUsers(@Body Users dto);

    @DELETE("/api/users/{userId}}")
    Call<Void> deleteUsers(@Path("userId") Integer userId);
}
