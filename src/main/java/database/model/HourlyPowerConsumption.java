package database.model;

import java.sql.Timestamp;

/**
 * A DTO Representing HourlyPowerConsumption
 * @since 9.17.17
 * @author Stepheny Perez
 */
public class HourlyPowerConsumption
{

    private Integer deviceId;
    private long timeStamp;
    private Double whUsage;
    private Double whUsageDouble;
    private String deviceName;

    public HourlyPowerConsumption(){
    }

    public HourlyPowerConsumption(Integer deviceId, long timeStamp, Double whUsage, Double whUsageDouble,
                                  String deviceName) {
        this.deviceId = deviceId;
        this.timeStamp = timeStamp;
        this.whUsage = whUsage;
        this.whUsageDouble = whUsageDouble;
        this.deviceName = deviceName;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Double getWhUsage() {
        return whUsage;
    }

    public void setWhUsage(Double whUsage) {
        this.whUsage = whUsage;
    }

    public Double getWhUsageDouble() {
        return whUsageDouble;
    }

    public void setWhUsageDouble(Double whUsageDouble) {
        this.whUsageDouble = whUsageDouble;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HourlyPowerConsumption that = (HourlyPowerConsumption) o;

        if (timeStamp != that.timeStamp) return false;
        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (whUsage != null ? !whUsage.equals(that.whUsage) : that.whUsage != null) return false;
        if (whUsageDouble != null ? !whUsageDouble.equals(that.whUsageDouble) : that.whUsageDouble != null)
            return false;
        return deviceName != null ? deviceName.equals(that.deviceName) : that.deviceName == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (int) (timeStamp ^ (timeStamp >>> 32));
        result = 31 * result + (whUsage != null ? whUsage.hashCode() : 0);
        result = 31 * result + (whUsageDouble != null ? whUsageDouble.hashCode() : 0);
        result = 31 * result + (deviceName != null ? deviceName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HourlyPowerConsumption{" +
                "deviceId=" + deviceId +
                ", timeStamp=" + timeStamp +
                ", whUsage=" + whUsage +
                ", whUsageDouble=" + whUsageDouble +
                ", deviceName='" + deviceName + '\'' +
                '}';
    }
}
