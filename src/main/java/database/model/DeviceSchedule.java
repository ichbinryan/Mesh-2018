package edu.nmsu.mesh.model;

/**
 * A DTO Representing the schedule for one device
 * @since 11.21.17
 * @author Stepheny Perez
 */
public class DeviceSchedule
{

    private Integer deviceId;
    private Integer[] schedule = new Integer[12]; //todo remove hardcode

    public DeviceSchedule(Integer deviceId, Integer[] schedule) {
        this.deviceId = deviceId;
        this.schedule = schedule;
    }

    public DeviceSchedule(){}

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer[] getSchedule() {
        return schedule;
    }

    public void setSchedule(Integer[] schedule) {
        this.schedule = schedule;
    }
}
