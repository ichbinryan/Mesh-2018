package database.model;

/**
 * Spring JPA Entity representing the Locations table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class Locations {

    private Integer locationId;
    private String locationName;
    private Integer locationType;
    private String locationImagePath;

    public Locations(Integer locationId, String locationName, Integer locationType, String locationImagePath) {
        this.locationId = locationId;
        this.locationName = locationName;
        this.locationType = locationType;
        this.locationImagePath = locationImagePath;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Integer getLocationType() {
        return locationType;
    }

    public void setLocationType(Integer locationType) {
        this.locationType = locationType;
    }

    public String getLocationImagePath() {
        return locationImagePath;
    }

    public void setLocationImagePath(String locationImagePath) {
        this.locationImagePath = locationImagePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Locations locations = (Locations) o;

        if (locationId != null
                ? !locationId.equals(locations.locationId)
                : locations.locationId != null) return false;
        if (locationName != null
                ? !locationName.equals(locations.locationName)
                : locations.locationName != null) return false;
        if (locationType != null
                ? !locationType.equals(locations.locationType)
                : locations.locationType != null) return false;
        if (locationImagePath != null
                ? !locationImagePath.equals(locations.locationImagePath)
                : locations.locationImagePath != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = locationId != null ? locationId.hashCode() : 0;
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        result = 31 * result + (locationType != null ? locationType.hashCode() : 0);
        result = 31 * result + (locationImagePath != null ? locationImagePath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Locations{" +
                "locationId=" + locationId +
                ", locationName='" + locationName + '\'' +
                ", locationType=" + locationType +
                ", locationImagePath='" + locationImagePath + '\'' +
                '}';
    }
}
