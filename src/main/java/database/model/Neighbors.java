package database.model;

/**
 * Spring JPA Entity representing the Neighbors table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class Neighbors {

    private Integer neighborId;
    private String neighborAddress;
    private String neighborAgentName;
    private String neighborPlatformName;

    public Neighbors(Integer neighborId, String neighborAddress, String neighborAgentName, String neighborPlatformName) {
        this.neighborId = neighborId;
        this.neighborAddress = neighborAddress;
        this.neighborAgentName = neighborAgentName;
        this.neighborPlatformName = neighborPlatformName;
    }

    public Integer getNeighborId() {
        return neighborId;
    }

    public void setNeighborId(Integer neighborId) {
        this.neighborId = neighborId;
    }

    public String getNeighborAddress() {
        return neighborAddress;
    }

    public void setNeighborAddress(String neighborAddress) {
        this.neighborAddress = neighborAddress;
    }

    public String getNeighborAgentName() {
        return neighborAgentName;
    }

    public void setNeighborAgentName(String neighborAgentName) {
        this.neighborAgentName = neighborAgentName;
    }

    public String getNeighborPlatformName() {
        return neighborPlatformName;
    }

    public void setNeighborPlatformName(String neighborPlatformName) {
        this.neighborPlatformName = neighborPlatformName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Neighbors neighbors = (Neighbors) o;

        if (neighborId != null
                ? !neighborId.equals(neighbors.neighborId)
                : neighbors.neighborId != null) return false;
        if (neighborAddress != null
                ? !neighborAddress.equals(neighbors.neighborAddress)
                : neighbors.neighborAddress != null) return false;
        if (neighborAgentName != null
                ? !neighborAgentName.equals(neighbors.neighborAgentName)
                : neighbors.neighborAgentName != null) return false;
        if (neighborPlatformName != null
                ? !neighborPlatformName.equals(neighbors.neighborPlatformName)
                : neighbors.neighborPlatformName != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = neighborId != null ? neighborId.hashCode() : 0;
        result = 31 * result + (neighborAddress != null ? neighborAddress.hashCode() : 0);
        result = 31 * result + (neighborAgentName != null ? neighborAgentName.hashCode() : 0);
        result = 31 * result + (neighborPlatformName != null ? neighborPlatformName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Neighbors{" +
                "neighborId=" + neighborId +
                ", neighborAddress='" + neighborAddress + '\'' +
                ", neighborAgentName='" + neighborAgentName + '\'' +
                ", neighborPlatformName='" + neighborPlatformName + '\'' +
                '}';
    }
}
