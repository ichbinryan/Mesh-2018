package database.model;

/**
 * Spring JPA Entity representing the Icons table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class Icons {

    private Integer iconId;
    private String iconName;
    private String iconUrl;
    private Integer modelType;
    private Integer modelId;

    public Icons(Integer iconId, String iconName, String iconUrl, Integer modelType, Integer modelId) {
        this.iconId = iconId;
        this.iconName = iconName;
        this.iconUrl = iconUrl;
        this.modelType = modelType;
        this.modelId = modelId;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getModelType() {
        return modelType;
    }

    public void setModelType(Integer modelType) {
        this.modelType = modelType;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Icons icons = (Icons) o;

        if (iconId != null ? !iconId.equals(icons.iconId) : icons.iconId != null) return false;
        if (iconName != null ? !iconName.equals(icons.iconName) : icons.iconName != null) return false;
        if (iconUrl != null ? !iconUrl.equals(icons.iconUrl) : icons.iconUrl != null) return false;
        if (modelType != null ? !modelType.equals(icons.modelType) : icons.modelType != null) return false;
        return modelId != null ? modelId.equals(icons.modelId) : icons.modelId == null;
    }

    @Override
    public int hashCode() {
        int result = iconId != null ? iconId.hashCode() : 0;
        result = 31 * result + (iconName != null ? iconName.hashCode() : 0);
        result = 31 * result + (iconUrl != null ? iconUrl.hashCode() : 0);
        result = 31 * result + (modelType != null ? modelType.hashCode() : 0);
        result = 31 * result + (modelId != null ? modelId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Icons{" +
                "iconId=" + iconId +
                ", iconName='" + iconName + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", modelType=" + modelType +
                ", modelId=" + modelId +
                '}';
    }
}
