package database.model;

import java.util.List;

/**
 * A DTO Representing the input to the Solver
 * @since 11.21.17
 * @author Stepheny Perez
 */
public class SolverInput
{

    private Integer deviceId;
    private List<RuleTable> rules;
    //todo probably other things

    public SolverInput(int deviceId, List<RuleTable> rules) {
        this.deviceId = deviceId;
        this.rules = rules;
    }

    public SolverInput() {}

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public List<RuleTable> getRules() {
        return rules;
    }

    public void setRules(List<RuleTable> rules) {
        this.rules = rules;
    }
}
