package database.model;

/**
 * Spring JPA Entity representing the Sensor_Properties table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class SensorProperties {

    private Integer sensorPropId;
    private String sensorPropName;
    private String sensorPropImagePath;

    public SensorProperties(Integer sensorPropId, String sensorPropName, String sensorPropImagePath) {
        this.sensorPropId = sensorPropId;
        this.sensorPropName = sensorPropName;
        this.sensorPropImagePath = sensorPropImagePath;
    }

    public Integer getSensorPropId() {
        return sensorPropId;
    }

    public void setSensorPropId(Integer sensorPropId) {
        this.sensorPropId = sensorPropId;
    }

    public String getSensorPropName() {
        return sensorPropName;
    }

    public void setSensorPropName(String sensorPropName) {
        this.sensorPropName = sensorPropName;
    }

    public String getSensorPropImagePath() {
        return sensorPropImagePath;
    }

    public void setSensorPropImagePath(String sensorPropImagePath) {
        this.sensorPropImagePath = sensorPropImagePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorProperties that = (SensorProperties) o;

        if (sensorPropId != null
                ? !sensorPropId.equals(that.sensorPropId)
                : that.sensorPropId != null) return false;
        if (sensorPropName != null
                ? !sensorPropName.equals(that.sensorPropName)
                : that.sensorPropName != null) return false;
        if (sensorPropImagePath != null
                ? !sensorPropImagePath.equals(that.sensorPropImagePath)
                : that.sensorPropImagePath != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = sensorPropId != null ? sensorPropId.hashCode() : 0;
        result = 31 * result + (sensorPropName != null ? sensorPropName.hashCode() : 0);
        result = 31 * result + (sensorPropImagePath != null ? sensorPropImagePath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SensorProperties{" +
                "sensorPropId=" + sensorPropId +
                ", sensorPropName='" + sensorPropName + '\'' +
                ", sensorPropImagePath='" + sensorPropImagePath + '\'' +
                '}';
    }
}
