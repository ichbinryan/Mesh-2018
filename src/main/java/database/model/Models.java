package database.model;

/**
 * Spring JPA Entity representing the Models table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class Models {

    private Integer modelId;
    private Integer modelType;
    private String modelThumbNail;

    public Models(Integer modelId, Integer modelType, String modelThumbNail) {
        this.modelId = modelId;
        this.modelType = modelType;
        this.modelThumbNail = modelThumbNail;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getModelType() {
        return modelType;
    }

    public void setModelType(Integer modelType) {
        this.modelType = modelType;
    }

    public String getModelThumbNail() {
        return modelThumbNail;
    }

    public void setModelThumbNail(String modelThumbNail) {
        this.modelThumbNail = modelThumbNail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Models models = (Models) o;

        if (modelId != null
                ? !modelId.equals(models.modelId)
                : models.modelId != null) return false;
        if (modelType != null
                ? !modelType.equals(models.modelType)
                : models.modelType != null) return false;
        if (modelThumbNail != null
                ? !modelThumbNail.equals(models.modelThumbNail)
                : models.modelThumbNail != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = modelId != null ? modelId.hashCode() : 0;
        result = 31 * result + (modelType != null ? modelType.hashCode() : 0);
        result = 31 * result + (modelThumbNail != null ? modelThumbNail.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Models{" +
                "modelId=" + modelId +
                ", modelType=" + modelType +
                ", modelThumbNail='" + modelThumbNail + '\'' +
                '}';
    }
}
