package database.model;

/**
 * A DTO Representing DeviceTable
 * @since 9.17.17
 * @author Stepheny Perez
 */
public class DeviceTable
{

    private Integer deviceId;
    private String deviceName;
    private Integer deviceArchetype;
    private Integer locationId;
    private Integer iconId;

    public DeviceTable() {
    }

    public DeviceTable(Integer deviceId, String deviceName, Integer deviceArchetype, Integer locationId,
                       Integer iconId) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.deviceArchetype = deviceArchetype;
        this.locationId = locationId;
        this.iconId = iconId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Integer getDeviceArchetype() {
        return deviceArchetype;
    }

    public void setDeviceArchetype(Integer deviceArchetype) {
        this.deviceArchetype = deviceArchetype;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceTable that = (DeviceTable) o;

        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (deviceName != null ? !deviceName.equals(that.deviceName) : that.deviceName != null) return false;
        if (deviceArchetype != null ? !deviceArchetype.equals(that.deviceArchetype) : that.deviceArchetype != null)
            return false;
        if (locationId != null ? !locationId.equals(that.locationId) : that.locationId != null) return false;
        return iconId != null ? iconId.equals(that.iconId) : that.iconId == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (deviceName != null ? deviceName.hashCode() : 0);
        result = 31 * result + (deviceArchetype != null ? deviceArchetype.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (iconId != null ? iconId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeviceTable{" +
                "deviceId=" + deviceId +
                ", deviceName='" + deviceName + '\'' +
                ", deviceArchetype=" + deviceArchetype +
                ", locationId=" + locationId +
                ", iconId=" + iconId +
                '}';
    }
}
