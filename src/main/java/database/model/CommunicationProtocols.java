package database.model;

/**
 * Spring JPA Entity representing the Communication_Protocols table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class CommunicationProtocols {

    private Integer protocolId;
    private String protocolName;

    public CommunicationProtocols(Integer protocolId, String protocolName) {
        this.protocolId = protocolId;
        this.protocolName = protocolName;
    }

    public Integer getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(Integer protocolId) {
        this.protocolId = protocolId;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommunicationProtocols that = (CommunicationProtocols) o;

        if (protocolId != null
                ? !protocolId.equals(that.protocolId)
                : that.protocolId != null) return false;
        if (protocolName != null
                ? !protocolName.equals(that.protocolName)
                : that.protocolName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = protocolId != null ? protocolId.hashCode() : 0;
        result = 31 * result + (protocolName != null ? protocolName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommunicationProtocols{" +
                "protocolId=" + protocolId +
                ", protocolName='" + protocolName + '\'' +
                '}';
    }
}
