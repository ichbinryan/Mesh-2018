package database.model;

import java.sql.Date;

/**
 * A DTO Representing DailyPowerConsumption
 * @since 9.8.17
 * @author Stepheny Perez
 */
public class DailyPowerConsumption
{

    private Integer deviceId;
    private Date timeStamp;
    private Double whUsage;

    public DailyPowerConsumption() {
    }

    public DailyPowerConsumption(Integer deviceId, Date timeStamp, Double whUsage) {
        this.deviceId = deviceId;
        this.timeStamp = timeStamp;
        this.whUsage = whUsage;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Double getWhUsage() {
        return whUsage;
    }

    public void setWhUsage(Double whUsage) {
        this.whUsage = whUsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyPowerConsumption that = (DailyPowerConsumption) o;

        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (timeStamp != null ? !timeStamp.equals(that.timeStamp) : that.timeStamp != null) return false;
        return whUsage != null ? whUsage.equals(that.whUsage) : that.whUsage == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (timeStamp != null ? timeStamp.hashCode() : 0);
        result = 31 * result + (whUsage != null ? whUsage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DailyPowerConsumption{" +
                "deviceId=" + deviceId +
                ", timeStamp=" + timeStamp +
                ", whUsage=" + whUsage +
                '}';
    }
}
