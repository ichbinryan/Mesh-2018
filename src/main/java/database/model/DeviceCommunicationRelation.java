package database.model;

/**
 * A DTO Representing DeviceCommunicationRelation
 * @since 9.15.17
 * @author Stepheny Perez
 */
public class DeviceCommunicationRelation
{

    private Integer deviceId;
    private Integer protocolId;

    public DeviceCommunicationRelation() {
    }

    public DeviceCommunicationRelation(Integer deviceId, Integer protocolId) {
        this.deviceId = deviceId;
        this.protocolId = protocolId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(Integer protocolId) {
        this.protocolId = protocolId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceCommunicationRelation that = (DeviceCommunicationRelation) o;

        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        return protocolId != null ? protocolId.equals(that.protocolId) : that.protocolId == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (protocolId != null ? protocolId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeviceCommunicationRelation{" +
                "deviceId=" + deviceId +
                ", protocolId=" + protocolId +
                '}';
    }
}
