package database.model;

/**
 * A DTO Representing DeviceActionRelation
 * @since 9.15.17
 * @author Stepheny Perez
 */
public class DeviceActionRelation
{

    private Integer deviceId;
    private Integer actionId;
    private Double kwh;

    public DeviceActionRelation() {
    }

    public DeviceActionRelation(Integer deviceId, Integer actionId, Double kwh) {
        this.deviceId = deviceId;
        this.actionId = actionId;
        this.kwh = kwh;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public Double getKwh() {
        return kwh;
    }

    public void setKwh(Double kwh) {
        this.kwh = kwh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceActionRelation that = (DeviceActionRelation) o;

        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (actionId != null ? !actionId.equals(that.actionId) : that.actionId != null) return false;
        return kwh != null ? kwh.equals(that.kwh) : that.kwh == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (actionId != null ? actionId.hashCode() : 0);
        result = 31 * result + (kwh != null ? kwh.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeviceActionRelation{" +
                "deviceId=" + deviceId +
                ", actionId=" + actionId +
                ", kwh=" + kwh +
                '}';
    }
}
