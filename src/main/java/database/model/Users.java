package database.model;

/**
 * Spring JPA Entity representing the Users table
 * @since 8.15.17
 * @author Stepheny Perez
 */
public class Users {

    private Integer userId;
    private String userName;
    private String password;
    private Integer accessLevel;

    public Users(Integer userId, String userName, String password, Integer accessLevel) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.accessLevel = accessLevel;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(Integer accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (userId != null
                ? !userId.equals(users.userId)
                : users.userId != null) return false;
        if (userName != null
                ? !userName.equals(users.userName)
                : users.userName != null) return false;
        if (password != null
                ? !password.equals(users.password)
                : users.password != null) return false;
        if (accessLevel != null
                ? !accessLevel.equals(users.accessLevel)
                : users.accessLevel != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (accessLevel != null ? accessLevel.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", accessLevel=" + accessLevel +
                '}';
    }
}
