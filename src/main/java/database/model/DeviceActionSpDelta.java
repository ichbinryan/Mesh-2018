package database.model;

/**
 * A DTO Representing DeviceActionSpDelta
 * @since 9.15.17
 * @author Stepheny Perez
 */
public class DeviceActionSpDelta
{

    private Integer deviceId;
    private Integer actionId;
    private Integer sensorPropId;
    private Double delta;

    public DeviceActionSpDelta() {
    }

    public DeviceActionSpDelta(Integer deviceId, Integer actionId, Integer sensorPropId, Double delta) {
        this.deviceId = deviceId;
        this.actionId = actionId;
        this.sensorPropId = sensorPropId;
        this.delta = delta;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public Integer getSensorPropId() {
        return sensorPropId;
    }

    public void setSensorPropId(Integer sensorPropId) {
        this.sensorPropId = sensorPropId;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceActionSpDelta that = (DeviceActionSpDelta) o;

        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (actionId != null ? !actionId.equals(that.actionId) : that.actionId != null) return false;
        if (sensorPropId != null ? !sensorPropId.equals(that.sensorPropId) : that.sensorPropId != null) return false;
        return delta != null ? delta.equals(that.delta) : that.delta == null;
    }

    @Override
    public int hashCode() {
        int result = deviceId != null ? deviceId.hashCode() : 0;
        result = 31 * result + (actionId != null ? actionId.hashCode() : 0);
        result = 31 * result + (sensorPropId != null ? sensorPropId.hashCode() : 0);
        result = 31 * result + (delta != null ? delta.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeviceActionSpDelta{" +
                "deviceId=" + deviceId +
                ", actionId=" + actionId +
                ", sensorPropId=" + sensorPropId +
                ", delta=" + delta +
                '}';
    }
}
