package database.model;

/**
 * A DTO Representing RuleTable
 * @since 9.17.17
 * @author Stepheny Perez
 */
public class RuleTable
{

    private Integer ruleId;
    private String ruleType;
    private String ruleRelation;
    private String timePrefix;
    private Integer goalState;
    private Integer startTime;
    private Integer endTime;
    private Integer locationId;
    private Integer sensorPropId;

    public RuleTable() {
    }

    public RuleTable(Integer ruleId, String ruleType, String ruleRelation, String timePrefix, Integer goalState,
                     Integer startTime, Integer endTime, Integer locationId, Integer sensorPropId) {
        this.ruleId = ruleId;
        this.ruleType = ruleType;
        this.ruleRelation = ruleRelation;
        this.timePrefix = timePrefix;
        this.goalState = goalState;
        this.startTime = startTime;
        this.endTime = endTime;
        this.locationId = locationId;
        this.sensorPropId = sensorPropId;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getRuleRelation() {
        return ruleRelation;
    }

    public void setRuleRelation(String ruleRelation) {
        this.ruleRelation = ruleRelation;
    }

    public String getTimePrefix() {
        return timePrefix;
    }

    public void setTimePrefix(String timePrefix) {
        this.timePrefix = timePrefix;
    }

    public Integer getGoalState() {
        return goalState;
    }

    public void setGoalState(Integer goalState) {
        this.goalState = goalState;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getSensorPropId() {
        return sensorPropId;
    }

    public void setSensorPropId(Integer sensorPropId) {
        this.sensorPropId = sensorPropId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RuleTable that = (RuleTable) o;

        if (ruleId != null ? !ruleId.equals(that.ruleId) : that.ruleId != null) return false;
        if (ruleType != null ? !ruleType.equals(that.ruleType) : that.ruleType != null) return false;
        if (ruleRelation != null ? !ruleRelation.equals(that.ruleRelation) : that.ruleRelation != null) return false;
        if (timePrefix != null ? !timePrefix.equals(that.timePrefix) : that.timePrefix != null) return false;
        if (goalState != null ? !goalState.equals(that.goalState) : that.goalState != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (locationId != null ? !locationId.equals(that.locationId) : that.locationId != null) return false;
        return sensorPropId != null ? sensorPropId.equals(that.sensorPropId) : that.sensorPropId == null;
    }

    @Override
    public int hashCode() {
        int result = ruleId != null ? ruleId.hashCode() : 0;
        result = 31 * result + (ruleType != null ? ruleType.hashCode() : 0);
        result = 31 * result + (ruleRelation != null ? ruleRelation.hashCode() : 0);
        result = 31 * result + (timePrefix != null ? timePrefix.hashCode() : 0);
        result = 31 * result + (goalState != null ? goalState.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (sensorPropId != null ? sensorPropId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RuleTable{" +
                "ruleId=" + ruleId +
                ", ruleType=" + ruleType +
                ", ruleRelation=" + ruleRelation +
                ", timePrefix=" + timePrefix +
                ", goalState=" + goalState +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", locationId=" + locationId +
                ", sensorPropId=" + sensorPropId +
                '}';
    }
}
