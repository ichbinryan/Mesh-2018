package factories;

import actions.Action;
import actions.ActionList;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Jeremiah on 12/14/2016.
 */
public class ActionListFactory {


    public static ActionList constructActionList(ArrayList<HashMap<String, String>> actions, ArrayList<HashMap<String, String>> affectedProps) {

        HashMap<Integer, Action> actionHashMap = new HashMap<>();

        for(HashMap<String, String> hm : actions) {
            int actionID = Integer.parseInt(hm.get("Action_ID"));
            Action action = ActionFactory.constructAction(hm, affectedProps);
            actionHashMap.put(action.getActionID(), action);
        }

        return new ActionList(actionHashMap);
    }
}
