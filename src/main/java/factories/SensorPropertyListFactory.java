package factories;

import sensorproperties.SensorPropList;
import sensorproperties.SensorProperty;
import utilities.Database;
import utilities.LoadProjectProperties;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 12/14/2016.
 */
public class SensorPropertyListFactory {

    /**
     * Method that takes an ArrayList of HashMap<String, String> and builds a sensor property list
     * @param sensorPropertiesHM
     *      HashMap<Integer, HashMap<String, String>>
     *          Integer, SensorPropID
     *          HashMap<String, String>
     *              SensorPropInfo, Value
     * @return SensorPropList
     */
    public static SensorPropList constructSensorPropList(HashMap<Integer, HashMap<String, String>> sensorPropertiesHM) {
        int debugLevel = LoadProjectProperties.getInstance().getFactoryDebugLevel();

        if(debugLevel > 0) System.out.println("Starting Sensor Property List Factory");

        HashMap<Integer, SensorProperty> sensorPropList = new HashMap<>();

        for(Integer sensorPropID: sensorPropertiesHM.keySet()) {

            sensorPropList.put(sensorPropID, SensorPropFactory.constructSensorProperty(sensorPropertiesHM.get(sensorPropID)));

        }

        if(debugLevel > 0) System.out.println("Ending Sensor Property List Factory");

        return new SensorPropList(sensorPropList);
    }

    /**
     * Method that constructs a SensorPropList of all sensor properties
     * @return SensorPropList
     */
    public static SensorPropList constructSensorPropListAll() {
        int debugLevel = LoadProjectProperties.getInstance().getFactoryDebugLevel();
        Database database = new Database();
        if(debugLevel > 0) System.out.println("Starting Sensor Property List Factory");

        HashMap<Integer, HashMap<String, String>> sensorPropertiesIndexedHM = database.selectAllSensorProperties();
        HashMap<Integer, SensorProperty> sensorPropList = new HashMap<>();

        for(Integer sensorPropID: sensorPropertiesIndexedHM.keySet()) {

            sensorPropList.put(sensorPropID, SensorPropFactory.constructSensorProperty(sensorPropertiesIndexedHM.get(sensorPropID)));

        }

        if(debugLevel > 0) System.out.println("Ending Sensor Property List Factory");

        return new SensorPropList(sensorPropList);
    }
}
