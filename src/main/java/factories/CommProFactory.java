package factories;

import communication.CommunicationProtocol;
import utilities.LoadProjectProperties;

import java.util.HashMap;

/**
 * Created by Jeremmiah on 12/15/2016.
 * @author Jeremiah Smith Smith
 * @creationDate 12/15/2016
 * @lastModifiedDate 12/16/2016
 * @inputs HashMap<String, String> with three values
 * Protocol_ID, ID
 * Protocol_Name, Name
 * Comm_ID, Communication Id - which is the protocol specific id used for communication
 * @outputs CommunicationProtocol Object
 *
 */
public class CommProFactory {

    /**
     * Construction a CommunicationProtocol Object with the given hashmap values
     * @param commPro HashMap<String, String> with three values
     * Protocol_ID, ID
     * Protocol_Name, Name
     * Comm_ID, Communication Id - which is the protocol specific id used for communication
     * @return CommunicationProtocol Object
     */
    public static CommunicationProtocol constructCommunicationProtocol(HashMap<String, String> commPro){

        int debugLevel = LoadProjectProperties.getInstance().getFactoryDebugLevel();

        if(debugLevel > 0) System.out.println("Starting Communication Protocol Factory");

        CommunicationProtocol communicationProtocol = new CommunicationProtocol(Integer.parseInt(commPro.get("Protocol_ID")), commPro.get("Protocol_Name"), commPro.get("Comm_ID"));

        if(debugLevel > 0) System.out.println("Ending Communication Protocol Factory");

        return communicationProtocol;
    }
}
