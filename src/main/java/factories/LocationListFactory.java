package factories;

import locations.Location;
import locations.LocationList;
import utilities.Database;
import utilities.LoadProjectProperties;

import java.util.HashMap;


/**
 * Created by jeremiah on 7/10/17.
 */
public class LocationListFactory {

    public static LocationList constructLocationListAll() {

        Database database = new Database();
        int debugLevel = LoadProjectProperties.getInstance().getFactoryDebugLevel();
        if(debugLevel > 0) System.out.println("Starting Location List Factory");

        HashMap<Integer, HashMap<String, String>> locationsIndexedHM = database.selectAllLocations();
        HashMap<Integer, Location> locationList = new HashMap<>();

        for(Integer locationID: locationsIndexedHM.keySet()) {

            locationList.put(locationID, LocationFactory.constructLocation(locationsIndexedHM.get(locationID)));

        }

        if(debugLevel > 0) System.out.println("Ending Location List Factory");

        return new LocationList(locationList);

    }

}
