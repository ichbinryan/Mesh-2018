package factories;

import locations.Location;
import models.PredictiveModel;
import rules.*;
import sensorproperties.SensorProperty;
import utilities.Database;
import utilities.LoadProjectProperties;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 12/16/16.
 */
//todo finish schedule rule factory
    //todo change to allow build from file
    ///todo:  insert assert statements to assure that rule input follows the 'rules'
public class RuleFactory {
    
    public static SchedulingRule constructScheduleRule(HashMap<String, String> rule, HashMap<String, String> sensorPropertyHM, HashMap<String, String> locationHM) {

        int goalState = Integer.parseInt(rule.get("Goal_State"));
        Location location = LocationFactory.constructLocation(locationHM);
        SensorProperty affectedSensorProp = SensorPropFactory.constructSensorProperty(sensorPropertyHM);
        int ruleID = Integer.parseInt(rule.get("Rule_ID"));
        RuleRelation ruleRelation = RuleRelation.assignRuleRelation(Integer.parseInt(rule.get("Rule_Relation")));
        RuleType ruleType = RuleType.assignRuleType(Integer.parseInt(rule.get("Rule_Type")));
        int startTime = Integer.parseInt(rule.get("Start_Time"));
        int endTime = Integer.parseInt(rule.get("End_Time"));
        RuleTimePrefix timePrefix = RuleTimePrefix.assignRuleTimePrefix(Integer.parseInt(rule.get("Time_Prefix")));
        PredictiveModel predictiveModel = PredictiveModel.valueOf(location, affectedSensorProp);
        RuleTimePredicate ruleTimePredicate;

        if ( ruleType == RuleType.PASSIVE) {
            // START Bill 6_5_2017
            startTime = 0;
            endTime = LoadProjectProperties.getInstance().getTimeSteps() - 1;
            // END Bill
            ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
        }
        else {
            switch(timePrefix) {
                case BEFORE: {
                    // START Bill 6_5_2017
                    startTime = 0;
                    endTime = Integer.parseInt(rule.get("End_Time"));
                    // END Bill
                    ruleTimePredicate = RuleTimePredicate.DISJUNCTION;
                    break;
                }
                case AFTER: {
                    // START Bill 6_5_2017
                    startTime = Integer.parseInt(rule.get("Start_Time"));
                    endTime = LoadProjectProperties.getInstance().getTimeSteps() - 1;
                    // END Bill
                    ruleTimePredicate = RuleTimePredicate.DISJUNCTION;
                    break;
                }
                case WITHIN: {
                    // START Bill 6_5_2017
                    startTime = Integer.parseInt(rule.get("Start_Time"));
                    endTime = Integer.parseInt(rule.get("End_Time"));
                    // END Bill 6_5_2017
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                    break;
                }
                case AT: {
                    // START Bill 6_5_2017
                    startTime = Integer.parseInt(rule.get("Start_Time"));
                    endTime = Integer.parseInt(rule.get("End_Time"));
                    // END Bill
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                    break;
                }
                default: {
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                    System.out.println("BROKEN RULE: " + rule);
                }
            }
        }

        return new SchedulingRule(ruleID, affectedSensorProp, location, ruleRelation, timePrefix, ruleType, goalState, startTime, endTime, ruleTimePredicate, predictiveModel);

    }
}
