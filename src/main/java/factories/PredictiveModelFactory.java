package factories;


import actions.Action;
import devices.Actuator;
import devices.Sensor;
import models.PredictiveModel;
import utilities.LoadProjectProperties;

import java.util.ArrayList;

/**
 * Created by Jeremiah Smith on 1/18/2017.
 */
public class PredictiveModelFactory {

    public static PredictiveModel constructPredictiveModel(Sensor sensor, ArrayList<Actuator> actuators) {
        int debugLevel = LoadProjectProperties.getInstance().getFactoryDebugLevel();
        ArrayList<Actuator> modelActuators = new ArrayList<>();
        if(debugLevel > 0) System.out.println("Starting Predictive Model Factory");
        for (Actuator a : actuators) {
            if (a.getLocation().equals(sensor.getLocation())) {
                for (Action da : a.getActionList().getActions().values()) {
                    if(da.getAffectedSensorProperties().containsSensorProperty(sensor.getAffectedProperty())) {
                        modelActuators.add(a); break; // break added by Bill 6_5_2017 --- this way it only adds the actuator to the model once
                    }
                }
            }
        }

        if(debugLevel > 0) System.out.println("Ending Predictive Model Factory");
        if(modelActuators.size() > 0) {
            return new PredictiveModel(sensor, modelActuators);
        }
        return null;
    }
}
