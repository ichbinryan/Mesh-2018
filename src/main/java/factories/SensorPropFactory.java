package factories;


import sensorproperties.SensorProperty;

import java.util.HashMap;

/**
 *
 * @author Jeremiah Smith
 * @creationDate 12/15/2016
 * @lastModifiedDate 04/24/17
 *
 * @inputs HashMap<String, String>
 *     Sensor_Property_ID, ID
 *     Sensor_Property_Name, Name
 *
 * @outputs SensorProperty Object
 *
 */
public class SensorPropFactory {

    public static SensorProperty constructSensorProperty(HashMap<String, String> sensorProp ) {

        return new SensorProperty(Integer.parseInt(sensorProp.get("Sensor_Prop_ID")), sensorProp.get("Sensor_Prop_Name"));
    }
}
