package factories;

import locations.Location;
import locations.LocationType;
import utilities.Database;

import java.util.HashMap;

/**
 *
 * @author Jeremiah Smith
 * @creationDate 12/14/2016
 * @lastModifiedDate 12/16/2016
 * @inputs HashMap<String, String> with three values
 * User_ID, ID
 * Location_Name, Name
 * Location_Type_ID - This is the KW used for one hour of continued use.
 * @outputs Location Object
 *
 */
public class LocationFactory {



    public static Location constructLocation(HashMap<String, String> locationMap) {

        int locationID = Integer.parseInt(locationMap.get("Location_ID"));
        String locationName = locationMap.get("Location_Name");
        int locationType = Integer.parseInt(locationMap.get("Location_Type"));

        return new Location(locationID, locationName, LocationType.assignLocationType(locationType));
    }
}
