package factories;

import actions.Action;
import devices.Sensor;
import sensorproperties.SensorProperty;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;


/**
 * Class used to construct an action object
 * @author Jeremiah Smith
 * creationDate: 12/15/2016
 * lastModifiedDate: 1/19/2017
 *
 */
public class ActionFactory {


    public static Action constructAction(HashMap<String, String> actionInfo, ArrayList<HashMap<String, String>> affectedSensorProperties) {
        Database database = new Database();
        int actionID = Integer.parseInt(actionInfo.get("Action_ID"));
        String actionName = actionInfo.get("Action_Name");
        double actionKWH =  Double.parseDouble(actionInfo.get("KWH"));

        HashMap<Integer, SensorProperty> propertyHashMap = new HashMap<>();
        HashMap<SensorProperty, Double> actionDeltas = new HashMap<>();
        for(HashMap<String, String> spHM : affectedSensorProperties) {
            int sensorPropID = Integer.parseInt(spHM.get("Sensor_Prop_ID"));
            propertyHashMap.put(sensorPropID, SensorPropFactory.constructSensorProperty(database.selectSensorProperty(sensorPropID)));
            actionDeltas.put(propertyHashMap.get(Integer.parseInt(spHM.get("Sensor_Prop_ID"))), Double.parseDouble((spHM.get("Delta"))));
        }


        return new Action(actionID, actionName, actionKWH,propertyHashMap, actionDeltas);
    }

}
