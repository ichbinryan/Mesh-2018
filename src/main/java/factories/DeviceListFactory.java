package factories;

import devices.Device;
import devices.DeviceList;
import utilities.Database;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 12/14/16.
 */
public class DeviceListFactory {


    public static DeviceList constructDeviceList() {

        Database database = new Database();
        HashMap<Integer, HashMap<String, String>> devices = database.selectAllDevices();
        HashMap<Integer, Device> deviceHashMap = new HashMap<>();

        for(HashMap<String, String> deviceInfoHM : devices.values()) {

            Device device = DeviceFactory.constructDevice(deviceInfoHM);

            deviceHashMap.put(device.getID(), device);
        }


        return new DeviceList(deviceHashMap);
    }

}
