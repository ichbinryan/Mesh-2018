package factories;

import actions.ActionList;
import actions.Action;
import communication.CommunicationProtocol;
import devices.*;
import locations.Location;
import sensorproperties.SensorProperty;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

//todo modify comment header to include class information
/**
 * Created by jeremiah on 12/13/16.
 */
public class DeviceFactory {


    public static Device constructDevice(HashMap<String, String> deviceInfoHM) {

        Database database = new Database();

        int deviceID = Integer.parseInt(deviceInfoHM.get("Device_ID"));
        String deviceName = deviceInfoHM.get("Device_Name");
        Location myLocation = LocationFactory.constructLocation(database.selectLocation(Integer.parseInt(deviceInfoHM.get("Location_ID"))));

        //todo fix communication protocol factory
        CommunicationProtocol communicationProtocol = new CommunicationProtocol(0, "Not Real", "Not Really Real");
        int deviceArchetype = Integer.parseInt(deviceInfoHM.get("Device_Archetype"));

        ClassType classType = ClassType.assignClassType(deviceArchetype);

        switch(classType) {
            case BINARY_SWITCH: {
                ActionList actionList = ActionListFactory.constructActionList(database.selectDeviceActions(deviceID), database.selectActionSPRelations(deviceID));
                BinarySwitch binarySwitch = new BinarySwitch(deviceName, deviceID, communicationProtocol, myLocation, actionList);
                return binarySwitch;
            }

            case THERMOSTAT: {

                break;
            }

            case LIGHT_ACTUATOR: {

                break;
            }

            case LUMINOSITY_SENSOR: {

                return new LuminositySensor(deviceID, 0, deviceName, myLocation, new SensorProperty(1, "Luminosity"), communicationProtocol);
            }

            case TEMP_SENSOR: {
                return new TempSensor(deviceID, 0, deviceName, myLocation, new SensorProperty(2, "Temperature"), communicationProtocol);
            }

            case ROOMBA: {
                ActionList actionList = ActionListFactory.constructActionList(database.selectDeviceActions(deviceID), database.selectActionSPRelations(deviceID));
                return new RoombaActuator(deviceID, myLocation, actionList);
            }

            default: {
                System.err.println("Unknown device class type");
            }
        }


        //todo finish new construct device

        return null;

    }
}
