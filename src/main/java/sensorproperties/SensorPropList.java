package sensorproperties;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 11/11/16.
 */
public class SensorPropList {

    private HashMap<Integer, SensorProperty> sensorProperties;

    public SensorPropList(HashMap<Integer, SensorProperty> sensorProperties) {
        this.sensorProperties = sensorProperties;
    }

    public HashMap<Integer, SensorProperty> getSensorProperties() {
        return sensorProperties;
    }

    public void setSensorProperties(HashMap<Integer, SensorProperty> sensorProperties) {
        this.sensorProperties = sensorProperties;
    }

    public boolean containsSensorProperty(SensorProperty sensorProperty) {

        return sensorProperties.containsValue(sensorProperty);

    }


}
