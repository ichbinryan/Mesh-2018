package sensorproperties;

/**
 * Created by Jeremiah Smith on 11/4/16.
 */
public class SensorProperty {

    private int sensorPropertyID;
    private String sensorPropertyName;

    public SensorProperty(int sensorPropertyID, String sensorPropertyName) {
        this.sensorPropertyID = sensorPropertyID;
        this.sensorPropertyName = sensorPropertyName;
    }

    public int getSensorPropertyID() {
        return sensorPropertyID;
    }

    public void setSensorPropertyID(int sensorPropertyID) {
        this.sensorPropertyID = sensorPropertyID;
    }

    public String getSensorPropertyName() {
        return sensorPropertyName;
    }

    public void setSensorPropertyName(String sensorPropertyName) {
        this.sensorPropertyName = sensorPropertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SensorProperty)) return false;

        SensorProperty that = (SensorProperty) o;

        if (sensorPropertyID != that.sensorPropertyID) return false;
        return sensorPropertyName != null ? sensorPropertyName.equals(that.sensorPropertyName) : that.sensorPropertyName == null;
    }

    @Override
    public int hashCode() {
        int result = sensorPropertyID;
        result = 31 * result + (sensorPropertyName != null ? sensorPropertyName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SensorProperty{" +
                "sensorPropertyID=" + sensorPropertyID +
                ", sensorPropertyName='" + sensorPropertyName + '\'' +
                '}';
    }
}
