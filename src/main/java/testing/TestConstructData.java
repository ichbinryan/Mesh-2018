package testing;

import devices.DeviceList;
import jade.core.AID;
import rules.SchedulingRule;
import utilities.ConstructData;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 5/15/17.
 */
public class TestConstructData {

    public static void main(String args[]) {
        DeviceList testDeviceList = ConstructData.getInstance().constructDeviceList();
        HashMap<Integer, AID> testNeighbors =  ConstructData.getInstance().constructNeighbors();
        ArrayList<SchedulingRule> testRules = ConstructData.getInstance().constructSchedulingRules();

        testDeviceList.printDevices();
        System.out.println(testNeighbors);
        System.out.println(testRules);
    }
}
