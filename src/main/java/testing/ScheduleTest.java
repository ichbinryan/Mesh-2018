package testing;

import actions.ActionList;
import communication.CommunicationProtocol;
import devices.*;
import factories.PredictiveModelFactory;
import locations.Location;
import locations.LocationType;
import models.PredictiveModel;
import rules.SchedulingRule;
import schedule.Schedule;
import schedule.ScheduleEntry;
import solver.solver;
import utilities.ConstructData;
import utilities.LoadProjectProperties;

import java.util.ArrayList;

/**
 * Created by Jeremiah Smith on 1/13/17.
 *
 * edited rread 1/29/2018
 */
public class ScheduleTest {

    public static void main(String[] Args) {

        int DEBUG = 2;

        System.out.println("Initialize solver");

        double bgLoads[] = new double[LoadProjectProperties.getInstance().getTimeSteps()];


        for(int i = 0; i < bgLoads.length; i++) {
            //background loads
            bgLoads[i]= 0.0;
        }

        ArrayList<Actuator> actuators = new ArrayList<>();
        ArrayList<Sensor> sensors = new ArrayList<>();
        DeviceList testDeviceList = ConstructData.getInstance().constructDeviceList();

        System.out.println("Splitting Device List into actuators and sensors");


        for(Device device : testDeviceList.getDeviceHashMap().values()) {
            if (device instanceof Sensor) {
                sensors.add((Sensor)device);
            }
            else {
                actuators.add((Actuator) device);
            }
        }

        //actuators.add(new BinarySwitch("light1", 1, new CommunicationProtocol(),
        //        new Location(1,"Kitchen", new LocationType(1))), new ActionList());

        for( Sensor sensor : sensors) {
            //goes into solver code
            PredictiveModel m = PredictiveModelFactory.constructPredictiveModel(sensor, actuators);
            System.out.println(m.toString());
            if(DEBUG >= 1){
                System.out.println(sensor);
            }
        }

        System.out.println("Constructing Rules");


        ArrayList<SchedulingRule> rules = ConstructData.getInstance().constructSchedulingRules();
        if(DEBUG == 2) {
            System.out.println("**************\n\nFollowing rules were constructed\n\n*********************");
            for (SchedulingRule sr : rules) {
                System.out.println(sr.toString());
            }
        }

        System.out.println("*********************\n\n We are entering solver \n\n*************************");
        solver solver = new solver(rules, bgLoads);

        //Schedule testSchedule = testSlover.getFirstSchedule();

        Schedule testSchedule = solver.getSchedule(bgLoads);

        for(ScheduleEntry se : testSchedule.getScheduleEntryMap().values()) {

            System.out.println("Schedule Entries for Device: " + testDeviceList.getDevice(se.getDeviceID()).getName() + ": ");
            System.out.println("\tScheduled Actions:");
            for(int i = 0; i < se.getScheduledActions().length; i++) {
                if(se.getScheduledActions()[i] != null) {
                    System.out.println("\t\tTime Step " + i + ": " + se.getScheduledActions()[i].getActionName());
                }
                else {
                    System.out.println("\t\tTime Step " + i + ": No Action");
                }
            }

        }

        System.out.println(testSchedule.getJSONSchedule());

    }


}
