package testing;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import devices.BinarySwitch;
import actions.Action;
import devices.Device;
import devices.DeviceList;
import devices.PowerHandler;
import factories.DeviceListFactory;
import utilities.ConstructData;

import java.util.ArrayList;

/**
 * Created by ryanread on 1/30/17.
 */
public class BinSwitchTest {

    public static void main(String[] args){

        ArrayList<BinarySwitch> switches = new ArrayList<BinarySwitch>();

        DeviceList testDeviceList = ConstructData.getInstance().constructDeviceList();
        for (Device device : testDeviceList.getDeviceHashMap().values()) {
            if (device instanceof BinarySwitch) {
                BinarySwitch bs = (BinarySwitch) device;
                System.out.println(bs);
                switches.add(bs);

            }
        }
        while(true) {

            //for (int i = 0; i < switches.size(); i++) {
            for(int i = 1; i <= 3; i++){
                try {
                    System.out.println("on: " + i);
                    HttpResponse<JsonNode> response = Unirest.get("https://graph-na02-useast1.api.smartthings.com/api/smartapps/installations/c891a564-ea53-4aeb-9e07-ba5d352df014"
                            + "/switches/" + "on" + i)
                            .header("Content-Type", "application/json")
                            .header("accept", "application/json")
                            .header("Authorization", "Bearer " + "ef88fbdf-ed48-405c-a407-952aeed66157")
                            .asJson();
                }catch(UnirestException e){

                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    System.out.println("off: " + i);
                    HttpResponse<JsonNode> response = Unirest.get("https://graph-na02-useast1.api.smartthings.com/api/smartapps/installations/c891a564-ea53-4aeb-9e07-ba5d352df014"
                            + "/switches/" + "off" + i)
                            .header("Content-Type", "application/json")
                            .header("accept", "application/json")
                            .header("Authorization", "Bearer " + "ef88fbdf-ed48-405c-a407-952aeed66157")
                            .asJson();
                }catch(UnirestException e){

                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                /*for (Action action : switches.get(i).getActionList().getActions().values()) {
                    System.out.println(action);
                    switches.get(i).activate(action);
                    PowerHandler.getInstance().getKWHSum(switches);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }*/
            }
        }





    }
}
