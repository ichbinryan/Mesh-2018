package testing;

import utilities.Database;
import utilities.JsonData;

import java.util.HashMap;

/**
 * Created by jeremiah on 5/25/17.
 */
public class DatabaseTest {


    public static void main(String args[]) {
        JsonData jsonData = JsonData.getInstance();
        Database database = new Database();

        HashMap<Integer, HashMap<String, String>> testNeighbors = jsonData.getNeighborsMap();


        for(Integer i : testNeighbors.keySet()) {

            database.insertData("Neighbors", testNeighbors.get(i));
        }
    }




}
