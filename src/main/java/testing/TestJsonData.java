package testing;

import devices.DeviceList;
import factories.DeviceListFactory;
import utilities.ConstructData;
import utilities.JsonData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 4/18/2017.
 */
public class TestJsonData {

    public static void main(String[] args) {
        JsonData jsonData = JsonData.getInstance();

        //HashMap of device information paired with device id
        HashMap<Integer, HashMap<String, String>> deviceInfo = jsonData.getActuatorsMap();
        System.out.println(deviceInfo);

        //HashMap of actions paired with device id
        HashMap<Integer, ArrayList<HashMap<String, String>>> actions = jsonData.getDeviceAction();
        System.out.println(actions);

        //HashMap of location information paired with location id
        HashMap<Integer, HashMap<String, String>> locations = jsonData.getLocationsMap();
        System.out.println(locations);

        //HashMap of delta information paired with action id
        HashMap<Integer, ArrayList<HashMap<String, String>>> affectedSensorProps = jsonData.getAffectedSensorProperties();
        System.out.println(affectedSensorProps);

        //HashMap of sensor property information paired with sensor prop id
        HashMap<Integer, HashMap<String,String>> sensorProps = jsonData.getSensorPropertiesMap();
        System.out.println(sensorProps);

        DeviceList deviceList = DeviceListFactory.constructDeviceList();

        deviceList.printDevices();
    }
}
