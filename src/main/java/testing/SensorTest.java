package testing;

import devices.LuminositySensor;

import java.util.PriorityQueue;

/**
 * Created by ryanread on 3/15/17.
 */
public class SensorTest {

    public static void main(String [] args){

        PriorityQueue<Integer> pq = new PriorityQueue<>((x, y) -> y-x);
        pq.add(10);
        pq.add(5);
        System.out.println(pq.peek());

    }

}
