package schedule;

import actions.Action;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import devices.Device;
import utilities.LoadProjectProperties;

import java.util.Arrays;

/**
 * Created by Jeremiah Smith on 1/16/2017.
 */
public class ScheduleEntry {

    private int deviceID;
    private String deviceName;
    private Action[] scheduledActions;
    private Gson gson;

    public ScheduleEntry(Device device) {
        this.gson = new Gson();
        this.deviceID = device.getID();
        this.deviceName = device.getName();
        scheduledActions = new Action[LoadProjectProperties.getInstance().getTimeSteps()];

        for(int i = 0; i < scheduledActions.length; i++) {
            scheduledActions[i] = new Action();
        }

    }

    public int getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(int deviceID) {
        this.deviceID = deviceID;
    }

    public Action[] getScheduledActions() {
        return scheduledActions;
    }

    public void setScheduledActions(Action[] scheduledActions) {
        this.scheduledActions = scheduledActions;
    }

    public void setScheduledAction(int scheduledTime, Action action) {
        assert(scheduledTime > 0 && scheduledTime < LoadProjectProperties.getInstance().getTimeSteps());

        scheduledActions[scheduledTime] = action;
    }

    public Action getScheduledAction(int timestep) {
        return scheduledActions[timestep];
    }

    //todo add json support

    public String getJSONScheduleEntry() {
        String scheduleEntry =  gson.toJson(this);

        return scheduleEntry;
    }

    public JsonObject getJSONScheduleEntryObject() {
        JsonObject scheduleEntryJSONObject = new JsonObject();
        JsonArray scheduleEntryJSONArray = new JsonArray();

        scheduleEntryJSONObject.addProperty("Device_ID", this.deviceID);
        scheduleEntryJSONObject.addProperty("Device_Name", this.deviceName);

        for(int i = 0; i < scheduledActions.length; i++) {
            if(scheduledActions[i] == null) {
                continue;
            }
            JsonObject entry = new JsonObject();

            if(scheduledActions[i] != null) {
                entry.addProperty("Action_ID", scheduledActions[i].getActionID());
                entry.addProperty("Timestep", i);
                scheduleEntryJSONArray.add(entry);
            }

        }
        scheduleEntryJSONObject.add("Actions", scheduleEntryJSONArray);

        return scheduleEntryJSONObject;
    }
    @Override
    public String toString() {
        return "ScheduleEntry{" +
                "\ndeviceID=" + deviceID +
                "\n\t\tscheduledActions=\n\t\t\t" + Arrays.toString(scheduledActions) +
                '}';
    }
}
