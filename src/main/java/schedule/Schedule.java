package schedule;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import utilities.LoadProjectProperties;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jeremiah Smith on 1/16/2017.
 */
public class Schedule {

    private final int TIMESTEPS;
    private Map<Integer, ScheduleEntry> scheduleEntryMap;
    private double utility;
    private double[] energyProfile;
    private double[] pricePerTimeStep;
    private Gson gson;

    //constructors

    public Schedule() {
        this.gson = new Gson();
        this.TIMESTEPS = LoadProjectProperties.getInstance().getTimeSteps();
        this.energyProfile = new double[TIMESTEPS];
        this.pricePerTimeStep = new double[TIMESTEPS];
        scheduleEntryMap = new HashMap<>();
    }

    //getter and setters

    public double getUtility() {
        return utility;
    }

    public void setUtility(double utility) {
        this.utility = utility;
    }

    public double[] getEnergyProfile() {
        return energyProfile;
    }

    public void setEnergyProfile(double[] energyProfile) {
        this.energyProfile = energyProfile;
    }

    public double[] getPricePerTimeStep() {
        return pricePerTimeStep;
    }

    public void setPricePerTimeStep(double[] pricePerTimeStep) {
        this.pricePerTimeStep = pricePerTimeStep;
    }

    //public methods
    public double getTotalScheduleCost() {
        double sum = 0.0;
        for(int i = 0; i < pricePerTimeStep.length; i++) {
            sum += pricePerTimeStep[i];
        }

        return sum;
    }

    public void addScheduleEntry(ScheduleEntry scheduleEntry) {
        scheduleEntryMap.put(scheduleEntry.getDeviceID(), scheduleEntry);
    }

    public ScheduleEntry getScheduleEntry(int deviceID) {
        return scheduleEntryMap.get(deviceID);
    }

    public void setPowerConsumptionKw(int timeStep, double powerConsumption) {
        energyProfile[timeStep] = powerConsumption;
        pricePerTimeStep[timeStep] = powerConsumption * LoadProjectProperties.getInstance().getPriceSchema()[timeStep];
    }

    public Map<Integer, ScheduleEntry> getScheduleEntryMap() {
        return scheduleEntryMap;
    }

    public String getJSONSchedule() {
        String scheduleJSON = gson.toJson(this.getJSONScheduleObject());

        return scheduleJSON;
    }

    public JsonObject getJSONScheduleObject() {
        JsonObject scheduleJSONObject = new JsonObject();
        JsonObject scheduledDevice = new JsonObject();
        JsonArray scheduleEntries = new JsonArray();
        JsonArray scheduleDates = new JsonArray();


        for( ScheduleEntry se : scheduleEntryMap.values()) {
            scheduleEntries.add(se.getJSONScheduleEntryObject());
        }


        scheduleJSONObject.add("Schedule", scheduleEntries);

        return  scheduleJSONObject;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "TIMESTEPS=" + TIMESTEPS +
                ", scheduleEntryMap=" + scheduleEntryMap +
                ", utility=" + utility +
                ", energyProfile=" + Arrays.toString(energyProfile) +
                ", pricePerTimeStep=" + Arrays.toString(pricePerTimeStep) +
                '}';
    }


}
