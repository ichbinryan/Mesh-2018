package meshinterface.controllers.main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import meshinterface.model.MeshModel;



import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;


/**
 * Created by Jeremiah Smith on 7/31/2017.
 */
public class MainController implements Initializable {



    @FXML
    private Button devicesButton;

    @FXML
    private Button agentButton;

    @FXML
    private Button rulesButton;

    @FXML
    private Button scheduleButton;

    @FXML
    private AreaChart<Date, Double> powerCostAreaChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        XYChart.Series<Date, Double> series = new XYChart.Series<>();
        XYChart.Data<Date, Double> seriesData = new XYChart.Data<>();

        HashMap<Integer, HashMap<String, String>> hourlyData = MeshModel.getInstance().getHourlyPCData();

        XYChart.Series<Date, Double> powerConsumption = new XYChart.Series<>();

        for(Integer deviceID: hourlyData.keySet()) {

            Double pc = Double.parseDouble(hourlyData.get(deviceID).get("WH_Usage_Double"));
            Timestamp timestamp = Timestamp.valueOf(hourlyData.get(deviceID).get("Time_Stamp"));
            Date date = timestamp;

            series.getData().add(new XYChart.Data<>(date, pc));
        }




    }
}
