package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import meshinterface.model.MeshModel;
import meshfx.states.RuleCreationState;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/21/17.
 */
public class AddRuleAnchorPane extends AnchorPane implements Initializable {

    @FXML
    private Label iconPaneLabel;
    @FXML
    private ImageView iconPaneImageView;

    private HashMap<String, String> addRuleAnchorPaneInfo;

    public AddRuleAnchorPane(HashMap<String, String> addRuleAnchorPaneInfo) {
        this.addRuleAnchorPaneInfo = addRuleAnchorPaneInfo;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/iconAnchorPane.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        RuleCreationState currentRuleCreationState =  MeshModel.getInstance().getRuleCreationState();
        switch (currentRuleCreationState) {
            case SENSOR_PROPERTY_SELECTION:
                this.loadSensorProperty();
                break;
            case LOCATION_SELECTION:
                this.loadLocation();
                break;
            case RULE_RELATION_SELECTION:
                break;
            case RULE_TYPE_SELECTION:
                break;
            case TIME_PREFIX_SELECTION:
                break;
            case TIME_SELECTION:
                break;
            case GOAL_STATE_SELECTION:
                break;
        }

    }

    public HashMap<String, String> getAddRuleAnchorPaneInfo() {
        return addRuleAnchorPaneInfo;
    }

    public void setAddRuleAnchorPaneInfo(HashMap<String, String> addRuleAnchorPaneInfo) {
        this.addRuleAnchorPaneInfo = addRuleAnchorPaneInfo;
    }

    private void loadSensorProperty() {

        int sensorPropID = Integer.parseInt(addRuleAnchorPaneInfo.get("Sensor_Prop_ID"));
        String sensorPropName = addRuleAnchorPaneInfo.get("Sensor_Prop_Name");


        iconPaneImageView.setImage(MeshModel.getInstance().getSensorPropImage(sensorPropID));
        iconPaneLabel.setText(sensorPropName);
    }

    private void loadLocation() {

        int locationID = Integer.parseInt(addRuleAnchorPaneInfo.get("Location_ID"));
        String locationName = addRuleAnchorPaneInfo.get("Location_Name");

        iconPaneImageView.setImage(MeshModel.getInstance().getLocationImage(locationID));
        iconPaneLabel.setText(locationName);
    }
}
