package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import meshinterface.model.MeshModel;
import sensorproperties.SensorProperty;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jeremiah Smith on 6/26/2017.
 */
public class SensorPropIconAnchorPane extends AnchorPane implements Initializable {


    private SensorProperty mySensorProperty;

    @FXML
    private ImageView iconPaneImageView;

    @FXML
    private Label iconPaneLabel;

    public SensorPropIconAnchorPane(SensorProperty mySensorProperty) {

        this.mySensorProperty = mySensorProperty;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/iconAnchorPane.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public SensorProperty getMySensorProperty() {
        return mySensorProperty;
    }




    @Override
    public void initialize(URL location, ResourceBundle resources) {


        iconPaneLabel.setText(mySensorProperty.getSensorPropertyName());
        iconPaneImageView.setImage(MeshModel.getInstance().getSensorPropImage(mySensorProperty.getSensorPropertyID()));

    }
}
