package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/23/17.
 */
public class TimeValueIconAnchorPane extends AnchorPane implements Initializable {


    private int timeValueHour;
    private int timeValueMin;
    private boolean am;



    @FXML
    private Label iconPaneLabel;

    @FXML
    private Label timeValueLabel;

    @FXML
    private Label amPMLabel;

    public TimeValueIconAnchorPane(int timeValueHour, int timeValueMin, boolean am) {
        this.timeValueHour = timeValueHour;
        this.timeValueMin = timeValueMin;
        this.am = am;


        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/addRuleTimeValueAnchorPane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getTimeValueHour() {
        return timeValueHour;
    }

    public int getTimeValueMin() {
        return timeValueMin;
    }

    public boolean isAm() {
        return am;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        iconPaneLabel.setText("");
        timeValueLabel.setText(timeValueHour + " : " + timeValueMin);
        if(am) {
            amPMLabel.setText("AM");
        }
        else {
            amPMLabel.setText("PM");
        }
    }
}
