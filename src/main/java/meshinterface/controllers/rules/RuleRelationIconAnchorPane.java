package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import meshinterface.model.MeshModel;
import rules.RuleRelation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/22/17.
 */
public class RuleRelationIconAnchorPane extends AnchorPane implements Initializable {

    private RuleRelation myRelation;



    @FXML
    private Label iconPaneLabel;
    @FXML
    private ImageView iconPaneImageView;



    public RuleRelationIconAnchorPane(RuleRelation relation) {
        this.myRelation = relation;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/iconAnchorPane.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getRuleRelationID() {
        return this.myRelation.getValue();
    }

    public RuleRelation getMyRelation() {
        return this.myRelation;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        switch (myRelation) {
            case EQUAL:
                iconPaneLabel.setText("Equal");
                break;
            case NOT_EQUAL:
                iconPaneLabel.setText("Not Equal");
                break;
            case LESS_THAN:
                iconPaneLabel.setText("Less Than");
                break;
            case GREATER_THAN:
                iconPaneLabel.setText("Greater Than");
                break;
            case LESS_THAN_EQUAL:
                iconPaneLabel.setText("Less Than Equal");
                break;
            case GREATER_THAN_EQUAL:
                iconPaneLabel.setText("Greater Than Equal");
                break;
        }

        iconPaneImageView.setImage(MeshModel.getInstance().getRuleRelationImage(myRelation));


    }
}
