package meshinterface.controllers.rules;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import meshinterface.model.MeshModel;
import meshfx.states.RuleCreationState;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/13/17.
 */
public class RuleManagerController implements Initializable {

    @FXML
    private TilePane currentRules;
    @FXML
    private Button newRule;


    @FXML
    private void handleNewRule(ActionEvent event) {

        if(event.getSource() == newRule) {
            try {

                Stage stage;
                Parent root;
                Scene scene;

                stage = (Stage) newRule.getScene().getWindow();
                root = FXMLLoader.load(getClass().getResource("/fxml/rules/addRule.fxml"));

                scene = new Scene(root);

                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //non fxml injected
    private HashMap<Integer, HashMap<String, String>> allRulesIndexed;
    private HashMap<Integer, HashMap<String, String>> allSensorProps;
    private HashMap<Integer, HashMap<String, String>> allLocations;

    private void handleRuleButton(ActionEvent event) {
        allRulesIndexed = MeshModel.getInstance().getRules();
        allSensorProps = MeshModel.getInstance().getSensorProps();
        allLocations = MeshModel.getInstance().getLocations();

        Button ruleBTN = (Button)event.getSource();
        HashMap<String, String> ruleHM = allRulesIndexed.get(Integer.parseInt(ruleBTN.getText()));
        HashMap<String, String> sensorPropHM = allSensorProps.get(Integer.parseInt(ruleHM.get("Sensor_Prop_ID")));
        HashMap<String, String> locationHM = allSensorProps.get(Integer.parseInt(ruleHM.get("Location_ID")));



    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allRulesIndexed = MeshModel.getInstance().getRules();
        MeshModel.getInstance().setRuleCreationState(RuleCreationState.LOCATION_SELECTION);
        for(HashMap<String, String> ruleHM : allRulesIndexed.values()) {
            MeshModel.getInstance().setRule(ruleHM);
            //AnchorPane anchorPane = FXMLLoader.load(getClass().getResource("/interfaceFXML/viewRuleAnchorPane.fxml"));
            ViewRuleAnchorPane ruleAnchorPane = new ViewRuleAnchorPane(ruleHM);
            currentRules.getChildren().add(ruleAnchorPane);
        }
    }
}
