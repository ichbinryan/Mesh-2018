package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/22/17.
 */
public class GoalStateIconAnchorPane extends AnchorPane implements Initializable {

    private int myGoalState;

    @FXML
    private Label iconPaneLabel;

    @FXML
    private Label goalStateLabel;

    public GoalStateIconAnchorPane(int goalStateValue) {
        this.myGoalState = goalStateValue;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/goalStateAnchorPane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int getMyGoalState() {
        return myGoalState;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        iconPaneLabel.setText("Goal State");
        goalStateLabel.setText(Integer.toString(myGoalState));
    }
}
