package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import locations.Location;
import meshinterface.model.MeshModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jeremiah Smith on 6/26/2017.
 */
public class LocationIconAnchorPane extends AnchorPane implements Initializable {

    private Location myLocation;

    @FXML
    private ImageView iconPaneImageView;

    @FXML
    private Label iconPaneLabel;

    public LocationIconAnchorPane(Location myLocation) {

        this.myLocation = myLocation;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/iconAnchorPane.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public Location getMyLocation() {
        return this.myLocation;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        iconPaneLabel.setText(myLocation.getLocationName());
        iconPaneImageView.setImage(MeshModel.getInstance().getLocationImage(myLocation.getLocationID()));

    }
}
