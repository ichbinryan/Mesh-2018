package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import meshinterface.model.MeshModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/23/17.
 */
public class ChooseTimeDialog extends AnchorPane implements Initializable {

    final ToggleGroup amPmToggleGroup = new ToggleGroup();

    @FXML
    private ChoiceBox<Integer> hourChoiceBox;

    @FXML
    private ChoiceBox<Integer> minChoiceBox;

    @FXML
    private Button enterButton;

    @FXML
    private Button cancelButton;

    @FXML
    private RadioButton amRadio;

    @FXML
    private RadioButton pmRadio;

    public ChooseTimeDialog() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/addRuleChooseTimeDialog.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        amRadio.setToggleGroup(amPmToggleGroup);
        pmRadio.setToggleGroup(amPmToggleGroup);

        amRadio.setSelected(true);

        for(int i = 1; i <= 12; i++) {
            hourChoiceBox.getItems().add(i);
        }

        for(int i = 0; i <= 60; i++) {
            if(i%15 == 0)
                minChoiceBox.getItems().add(i);
        }

        hourChoiceBox.getSelectionModel().selectFirst();
        minChoiceBox.getSelectionModel().selectFirst();
        enterButton.setOnMouseClicked(event -> handleEnterButtonOnMouseClick());

    }

    private void handleEnterButtonOnMouseClick() {

        MeshModel.getInstance().setHour(hourChoiceBox.getSelectionModel().getSelectedItem());
        MeshModel.getInstance().setMin(minChoiceBox.getSelectionModel().getSelectedItem());
        MeshModel.getInstance().setAm(amRadio.isSelected());

        Stage stage = (Stage) enterButton.getScene().getWindow();
        stage.close();

    }
}
