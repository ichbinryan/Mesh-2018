package meshinterface.controllers.rules;

import devices.Sensor;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import locations.Location;
import meshinterface.model.MeshModel;
import meshfx.states.RuleCreationState;
import rules.RuleRelation;
import rules.RuleTimePrefix;
import rules.RuleType;
import rules.SchedulingRule;
import utilities.LoadProjectProperties;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/16/17.
 */
public class AddRuleController implements Initializable {

    @FXML
    private Button next;
    @FXML
    private Button back;

    @FXML
    private Label pickLabel;

    @FXML
    private TilePane pickTilePane;

    @FXML
    private TilePane selectedTilePane;



    @FXML
    private ProgressBar ruleProgress;



    private SchedulingRule newRule = new SchedulingRule();


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        pickLabel.setText("Select Location");
        this.populateTilePaneLocationIconAP(pickTilePane);
    }

    private void handleSelectedAnchorPaneOnMouseClick(AnchorPane currentPane) {
        switch (MeshModel.getInstance().getRuleCreationState()) {
            case SENSOR_PROPERTY_SELECTION:
                ruleProgress.setProgress(0.0);
                selectedTilePane.getChildren().remove(currentPane);
                pickTilePane.getChildren().removeAll(pickTilePane.getChildren());
                pickLabel.setText("Select Location");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.LOCATION_SELECTION);
                this.populateTilePaneLocationIconAP(pickTilePane);
                break;
            case LOCATION_SELECTION:
                break;
            case RULE_RELATION_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() - 0.15);
                selectedTilePane.getChildren().remove(currentPane);
                pickTilePane.getChildren().removeAll(pickTilePane.getChildren());
                pickLabel.setText("Select Sensor Property");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.SENSOR_PROPERTY_SELECTION);
                LocationIconAnchorPane locationIconAnchorPane = null;
                for(Node node: selectedTilePane.getChildren()) {
                    if(node instanceof LocationIconAnchorPane) {
                        locationIconAnchorPane = (LocationIconAnchorPane) node;
                    }
                }

                if(locationIconAnchorPane != null) {
                    this.populateTilePaneSensorPropAP(pickTilePane, locationIconAnchorPane.getMyLocation());
                }

                break;
            case RULE_TYPE_SELECTION:
                break;
            case TIME_PREFIX_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() - 0.15);
                selectedTilePane.getChildren().remove(currentPane);
                pickTilePane.getChildren().removeAll(pickTilePane.getChildren());
                pickLabel.setText("Select Rule Relation");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.GOAL_STATE_SELECTION);
                this.constructPickTilePaneGoalStateAP();
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.TIME_PREFIX_SELECTION);
                this.populateTilePaneTimePrefixAP(pickTilePane);
                break;
            case TIME_SELECTION:
                break;
            case GOAL_STATE_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() - 0.15);
                selectedTilePane.getChildren().remove(currentPane);
                pickTilePane.getChildren().removeAll(pickTilePane.getChildren());
                pickLabel.setText("Select Rule Relation");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.RULE_RELATION_SELECTION);
                this.populateTilePaneRelationAP(pickTilePane);
                break;
        }


    }

    private void handlePickAnchorPaneOnMouseClick(AnchorPane currentPane) {
        switch (MeshModel.getInstance().getRuleCreationState()) {
            case SENSOR_PROPERTY_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() + 0.15);
                newRule.setRuleType(RuleType.ACTIVE);
                newRule.setAffectedProperty(((SensorPropIconAnchorPane) currentPane).getMySensorProperty());
                currentPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(currentPane));
                selectedTilePane.getChildren().add(currentPane);
                pickLabel.setText("Select Rule Relation");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.RULE_RELATION_SELECTION);
                this.populateTilePaneRelationAP(pickTilePane);
                break;
            case LOCATION_SELECTION: {
                ruleProgress.setProgress(0.15);
                newRule.setLocation(((LocationIconAnchorPane) currentPane).getMyLocation());
                currentPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(currentPane));
                selectedTilePane.getChildren().add(currentPane);
                pickLabel.setText("Select Sensor Properties");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.SENSOR_PROPERTY_SELECTION);
                this.populateTilePaneSensorPropAP(pickTilePane, ((LocationIconAnchorPane)currentPane).getMyLocation());
                break;
            }
            case RULE_RELATION_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() + 0.15);
                newRule.setRuleRelation(((RuleRelationIconAnchorPane) currentPane).getMyRelation());
                currentPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(currentPane));
                selectedTilePane.getChildren().add(currentPane);
                pickLabel.setText("");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.GOAL_STATE_SELECTION);
                this.constructPickTilePaneGoalStateAP();
                ruleProgress.setProgress(ruleProgress.getProgress() + 0.15);
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.TIME_PREFIX_SELECTION);
                this.populateTilePaneTimePrefixAP(pickTilePane);
                break;
            case RULE_TYPE_SELECTION:
                break;
            case TIME_PREFIX_SELECTION:
                ruleProgress.setProgress(ruleProgress.getProgress() + 0.15);
                newRule.setRuleTimePrefix(((TimePrefixIconAnchorPane)currentPane).getMyTimePrefix());
                currentPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(currentPane));
                selectedTilePane.getChildren().add(currentPane);
                pickLabel.setText("Select Goal State");
                MeshModel.getInstance().setRuleCreationState(RuleCreationState.TIME_SELECTION);
                this.constructPickTilePaneTimeValueAP(((TimePrefixIconAnchorPane) currentPane).getMyTimePrefix());
                this.constructConfirmRuleAP();
                this.reloadRuleManager();
                break;
            case TIME_SELECTION:
                break;
            case GOAL_STATE_SELECTION:
                break;

        }




    }

    private void populateTilePaneSensorPropAP(TilePane tilePane, Location location) {
        tilePane.getChildren().removeAll(tilePane.getChildren());

        ArrayList<Sensor> sensors = MeshModel.getInstance().getMyAgent().getDeviceList().getSensorsForLocation(location);

        for(Sensor sensor : sensors) {

            SensorPropIconAnchorPane sensorPropIconAnchorPane = new SensorPropIconAnchorPane(sensor.getAffectedProperty());
            sensorPropIconAnchorPane.setOnMouseClicked(event -> handlePickAnchorPaneOnMouseClick(sensorPropIconAnchorPane));
            tilePane.getChildren().add(sensorPropIconAnchorPane);

        }
    }

    private void populateTilePaneLocationIconAP(TilePane tilePane) {
        tilePane.getChildren().removeAll(tilePane.getChildren());

        for(Location location : MeshModel.getInstance().getMyAgent().getAllLocations().getLocationHashMap().values()) {

            LocationIconAnchorPane locationIconAnchorPane = new LocationIconAnchorPane(location);
            locationIconAnchorPane.setOnMouseClicked(event -> handlePickAnchorPaneOnMouseClick(locationIconAnchorPane));
            tilePane.getChildren().add(locationIconAnchorPane);

        }

    }

    private void populateTilePaneRelationAP(TilePane tilePane) {

        tilePane.getChildren().removeAll(tilePane.getChildren());

        for(RuleRelation relation : RuleRelation.values()) {
            RuleRelationIconAnchorPane addRuleRelationAnchorPane = new RuleRelationIconAnchorPane(relation);
            addRuleRelationAnchorPane.setOnMouseClicked(event -> handlePickAnchorPaneOnMouseClick(addRuleRelationAnchorPane));
            tilePane.getChildren().add(addRuleRelationAnchorPane);
        }

    }

    private void populateTilePaneTimePrefixAP(TilePane tilePane) {

        tilePane.getChildren().removeAll(tilePane.getChildren());

        for(RuleTimePrefix timePrefix : RuleTimePrefix.values()) {
            TimePrefixIconAnchorPane addRuleTimePrefixAnchorPane = new TimePrefixIconAnchorPane(timePrefix);
            addRuleTimePrefixAnchorPane.setOnMouseClicked(event -> handlePickAnchorPaneOnMouseClick(addRuleTimePrefixAnchorPane));
            tilePane.getChildren().add(addRuleTimePrefixAnchorPane);
        }

    }

    private void constructPickTilePaneGoalStateAP() {

        Stage goalStateDialogStage = new Stage();
        goalStateDialogStage.initModality(Modality.APPLICATION_MODAL);

        goalStateDialogStage.setScene(new Scene(new GoalStateDialog()));

        goalStateDialogStage.showAndWait();

        GoalStateIconAnchorPane goalStateIconAnchorPane = new GoalStateIconAnchorPane(MeshModel.getInstance().getGoalState());
        goalStateIconAnchorPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(goalStateIconAnchorPane));
        newRule.setGoalState(goalStateIconAnchorPane.getMyGoalState());
        selectedTilePane.getChildren().add(goalStateIconAnchorPane);

    }

    private void constructPickTilePaneTimeValueAP(RuleTimePrefix timePrefix) {
        Stage timeSelectionStage = new Stage();

        timeSelectionStage.initModality(Modality.APPLICATION_MODAL);

        timeSelectionStage.setScene(new Scene(new ChooseTimeDialog()));
        timeSelectionStage.showAndWait();

        TimeValueIconAnchorPane timeValueIconAnchorPane = new TimeValueIconAnchorPane(MeshModel.getInstance().getHour(), MeshModel.getInstance().getMin(), MeshModel.getInstance().isAm());
        timeValueIconAnchorPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(timeValueIconAnchorPane));
        selectedTilePane.getChildren().add(timeValueIconAnchorPane);

        int timeStep;

        if(timeValueIconAnchorPane.isAm()) {
            timeStep = timeValueIconAnchorPane.getTimeValueHour()/2;
        }
        else {
            timeStep = (timeValueIconAnchorPane.getTimeValueHour() + 12)/2;
        }

        switch (timePrefix) {
            case BEFORE:
                newRule.setStartTime(0);
                newRule.setEndTime(timeStep);
                break;
            case AFTER:
                newRule.setStartTime(timeStep);
                newRule.setEndTime(LoadProjectProperties.getInstance().getTimeSteps() - 1);
                break;
            case AT:
                newRule.setStartTime(timeStep);
                newRule.setEndTime(timeStep);
                break;
            case WITHIN:
                newRule.setStartTime(timeStep);
                timeSelectionStage.showAndWait();
                timeValueIconAnchorPane.setOnMouseClicked(event -> handleSelectedAnchorPaneOnMouseClick(timeValueIconAnchorPane));
                selectedTilePane.getChildren().add(timeValueIconAnchorPane);
                if(timeValueIconAnchorPane.isAm()) {
                    timeStep = timeValueIconAnchorPane.getTimeValueHour()/2;
                }
                else {
                    timeStep = (timeValueIconAnchorPane.getTimeValueHour() + 12)/2;
                }
                newRule.setEndTime(timeStep);
                break;
        }

    }

    private void constructConfirmRuleAP() {
        Stage confirmNewRule = new Stage();

        confirmNewRule.initModality(Modality.APPLICATION_MODAL);
        confirmNewRule.setTitle("Create This Rule");
        confirmNewRule.setScene(new Scene(new ConfirmNewRuleDialog(newRule.createRuleHM())));
        confirmNewRule.showAndWait();
    }

    private void reloadRuleManager() {

        MeshModel.getInstance().refreshRules();

        try {

            Stage stage;
            Parent root;
            Scene scene;

            stage = (Stage) pickTilePane.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("/fxml/rules/ruleManager.fxml"));

            scene = new Scene(root);

            stage.setScene(scene);
            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean completeRule() {


        return false;
    }

}
