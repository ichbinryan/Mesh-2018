package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import meshinterface.model.MeshModel;
import rules.RuleRelation;
import rules.RuleTimePrefix;
import rules.RuleType;
import utilities.LoadProjectProperties;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by Jeremiah Smith on 6/26/2017.
 */
public class ConfirmNewRuleDialog extends AnchorPane implements Initializable {


    @FXML
    private ImageView imageView;
    @FXML
    private ImageView ruleTypeImageView;
    @FXML
    private ImageView sensorPropImageView;
    @FXML
    private ImageView locationImageView;
    @FXML
    private Label rulePartOne;
    @FXML
    private Label ruleLocation;
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;
    @FXML
    private VBox vBox;


    private HashMap<String, String> anchorPaneInfo;

    public ConfirmNewRuleDialog(HashMap<String, String> anchorPaneInfo) {
        this.anchorPaneInfo = anchorPaneInfo;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/confirmNewRuleDialog.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {




        int sensorPropID = Integer.parseInt(anchorPaneInfo.get("Sensor_Prop_ID"));
        int locationID = Integer.parseInt(anchorPaneInfo.get("Location_ID"));
        RuleType ruleType =  RuleType.assignRuleType(Integer.parseInt(anchorPaneInfo.get("Rule_Type")));
        Image ruleTypeImage;

        rulePartOne.setText(this.buildRuleLabelText());
        switch (ruleType) {
            case PASSIVE:
                ruleTypeImage =  new Image(getClass().getResourceAsStream("/interfaceFXML/images/passive.png"));
                break;
            case ACTIVE:
                ruleTypeImage =  new Image(getClass().getResourceAsStream("/interfaceFXML/images/active.png"));
                break;
            default:
                ruleTypeImage =  new Image(getClass().getResourceAsStream("/interfaceFXML/images/passive.png"));
                break;
        }



        ruleTypeImageView.setImage(ruleTypeImage);
        sensorPropImageView.setImage(MeshModel.getInstance().getSensorPropImage(sensorPropID));
        locationImageView.setImage(MeshModel.getInstance().getLocationImage(locationID));
        imageView.setImage(MeshModel.getInstance().getLocationImage(locationID));
        yesButton.setOnMouseClicked(event -> handleYesButtonOnMouseClicked());
        noButton.setOnMouseClicked(event -> handleNoButtonOnMouseClicked());
    }

    private String buildRuleLabelText() {
        HashMap<String, String> currentRule = MeshModel.getInstance().getRule();
        String ruleText;
        String relation = "";
        String timePrefix = "";
        int sensorPropID = Integer.parseInt(anchorPaneInfo.get("Sensor_Prop_ID"));
        int locationID = Integer.parseInt(anchorPaneInfo.get("Location_ID"));
        RuleRelation ruleRelation = RuleRelation.assignRuleRelation(Integer.parseInt(currentRule.get("Rule_Relation")));
        RuleTimePrefix ruleTimePrefix = RuleTimePrefix.assignRuleTimePrefix(Integer.parseInt(currentRule.get("Time_Prefix")));
        String goalState = anchorPaneInfo.get("Goal_State");
        String sensorProp = MeshModel.getInstance().getSensorProps().get(sensorPropID).get("Sensor_Prop_Name");
        String location = MeshModel.getInstance().getLocations().get(locationID).get("Location_Name");

        switch (ruleRelation) {
            case EQUAL:
                relation = "Equal ";
                break;
            case NOT_EQUAL:
                relation = "Not equal ";
                break;
            case LESS_THAN:
                relation = "Less than ";
                break;
            case GREATER_THAN:
                relation = "Greater than ";
                break;
            case LESS_THAN_EQUAL:
                relation = "Less than equal to ";
                break;
            case GREATER_THAN_EQUAL:
                relation = "Greater than equal to ";
                break;
        }

        switch (ruleTimePrefix) {
            case BEFORE: {
                int endTime = Integer.parseInt(anchorPaneInfo.get("End_Time")) * (24/ LoadProjectProperties.getInstance().getTimeSteps());
                timePrefix = (endTime < 12)?"Before " + endTime + ":00 am":"Before " + endTime%12 + ":00 pm";
                break;
            }
            case AFTER:{
                int startTime = Integer.parseInt(anchorPaneInfo.get("Start_Time")) * 2;
                timePrefix = (startTime < 12)?"After " + startTime + ":00 am":"After " + startTime%12 + ":00 pm";
                break;
            }
            case AT:{
                int startTime = Integer.parseInt(anchorPaneInfo.get("Start_Time")) * 2;
                timePrefix = (startTime < 12)?"At " + startTime + ":00 am":"At " + startTime%12 + ":00 pm";
                break;
            }
            case WITHIN: {
                int startTime = Integer.parseInt(anchorPaneInfo.get("Start_Time")) * 2;
                int endTime = Integer.parseInt(anchorPaneInfo.get("End_Time")) *2;
                timePrefix = (startTime < 12)?"Within " + startTime + ":00 am":"Within " + startTime%12 + ":00 pm";
                timePrefix += (endTime < 12)?" and " + endTime + ":00 am":" and " + endTime%12 + ":00 pm";
                break;
            }
        }

        ruleText = sensorProp + " in " + location + "\n" + relation + goalState + "\n" + timePrefix;

        return ruleText;
    }

    public HashMap<String, String> getAnchorPaneInfo() {
        return anchorPaneInfo;
    }

    public void setAnchorPaneInfo(HashMap<String, String> anchorPaneInfo) {
        this.anchorPaneInfo = anchorPaneInfo;
    }

    private void handleYesButtonOnMouseClicked() {
        MeshModel.getInstance().insertNewRule(anchorPaneInfo);
        Stage stage = (Stage) yesButton.getScene().getWindow();
        stage.close();
    }

    private void handleNoButtonOnMouseClicked() {

    }
}
