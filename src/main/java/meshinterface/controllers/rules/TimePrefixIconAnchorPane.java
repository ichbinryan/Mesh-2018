package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import meshinterface.model.MeshModel;
import rules.RuleTimePrefix;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/22/17.
 */
public class TimePrefixIconAnchorPane extends AnchorPane implements Initializable {

    private RuleTimePrefix myTimePrefix;



    @FXML
    private Label iconPaneLabel;
    @FXML
    private ImageView iconPaneImageView;



    public TimePrefixIconAnchorPane(RuleTimePrefix timePrefix) {
        this.myTimePrefix = timePrefix;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/iconAnchorPane.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RuleTimePrefix getMyTimePrefix() {
        return myTimePrefix;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        switch (myTimePrefix) {
            case BEFORE:
                iconPaneLabel.setText("Before");
                break;
            case AFTER:
                iconPaneLabel.setText("After");
                break;
            case AT:
                iconPaneLabel.setText("At");
                break;
            case WITHIN:
                iconPaneLabel.setText("Within");
                break;
        }

        iconPaneImageView.setImage(MeshModel.getInstance().getTimePrefixImage(myTimePrefix));


    }

}
