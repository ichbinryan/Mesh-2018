package meshinterface.controllers.rules;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import meshinterface.model.MeshModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/22/17.
 */
public class GoalStateDialog extends AnchorPane implements Initializable {


    @FXML
    private Button enterButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField goalStateTextField;


    public GoalStateDialog() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/rules/addRuleGoalStatePopUp.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try{
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        cancelButton.setCancelButton(true);
        enterButton.setOnMouseClicked(event -> handleEnterButtonOnMouseClicked());
    }

    private void handleEnterButtonOnMouseClicked() {

        if(!goalStateTextField.getText().isEmpty()) {
            MeshModel.getInstance().setGoalState(Integer.parseInt(goalStateTextField.getText()));

            Stage goalStateDialog = (Stage) enterButton.getScene().getWindow();

            goalStateDialog.close();
        }
        else {
            System.out.println("Alert Popup Coming Soon - must enter goal state");
        }

    }
}
