package meshinterface.controllers.device;

import devices.Device;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jeremiah Smith on 7/31/2017.
 */
public class DeviceAP extends AnchorPane implements Initializable {

    @FXML
    private ImageView deviceIconView;

    @FXML
    private Label deviceNameLabel;

    @FXML
    private Label dviceLocationLabel;

    @FXML
    private TilePane devcieActionsTilePane;

    @FXML
    private LineChart<?, ?> deviceCostLineChart;

    private Device myDevice;

    public DeviceAP(Device myDevice) {

        this.myDevice = myDevice;
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert deviceIconView != null : "fx:id=\"deviceIconView\" was not injected: check your FXML file 'deviceView.fxml'.";
        assert deviceNameLabel != null : "fx:id=\"deviceNameLabel\" was not injected: check your FXML file 'deviceView.fxml'.";
        assert dviceLocationLabel != null : "fx:id=\"deviceLocationLabel\" was not injected: check your FXML file 'deviceView.fxml'.";
        assert devcieActionsTilePane != null : "fx:id=\"deviceActionsTilePane\" was not injected: check your FXML file 'deviceView.fxml'.";
        assert deviceCostLineChart != null : "fx:id=\"deviceCostLineChart\" was not injected: check your FXML file 'deviceView.fxml'.";

    }
}
