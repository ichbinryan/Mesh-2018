package meshinterface;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created by Jeremiah Smith on 7/31/2017.
 */
public class Main extends Application {



    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        primaryStage.setTitle("Home");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main/main.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setMaximized(false);
        primaryStage.setFullScreen(false);
        primaryStage.setResizable(false);

        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
