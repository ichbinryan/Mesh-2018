package meshinterface.model;

import agents.HouseAgent;
import devices.Actuator;
import communication.CommunicationProtocol;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import locations.Location;
import meshfx.states.RuleCreationState;
import rules.RuleRelation;
import rules.RuleTimePrefix;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by jeremiah on 10/21/16.
 */
public class MeshModel {

    //singleton instance
    private static MeshModel ourInstance = new MeshModel();

    //member variables
    private Database database;
    private HouseAgent myAgent;
    private HashMap<String, String> rule;
    private HashMap<String, String> sensorProp;
    private HashMap<String, String> location;
    private HashMap<String, String> tileInfo;
    private HashMap<Integer, Image> sensorPropImages = new HashMap<>();
    private HashMap<Integer, Image> locationImages = new HashMap<>();
    private HashMap<Integer, Image> ruleRelationImages = new HashMap<>();
    private HashMap<Integer, Image> timePrefixImages = new HashMap<>();
    private HashMap<Integer, HashMap<String, String>> locations;
    private HashMap<Integer, HashMap<String,String>> sensorProperties;
    private HashMap<Integer, HashMap<String, String>> rules;
    private HashMap<Integer, HashMap<String, String>> hourlyPCData;
    private RuleCreationState ruleCreationState;
    private int currentTileInfo = -1;
    private int goalState;
    private int hour;
    private int min;
    private boolean am;
    private HashMap<String, String> ruleForCreation = new HashMap<>();


    //singleton constructor
    private MeshModel()
    {
        database = new Database();
        //ruleCreationState = RuleCreationState.LOCATION_SELECTION;
        for(HashMap<String, String> sensorHM : database.selectAllSensorProperties().values()) {
            //sensorPropImages.put(Integer.parseInt(sensorHM.get("Sensor_Prop_ID")), new Image(getClass().getResourceAsStream(sensorHM.get("Sensor_Prop_Image_Path"))));
        }

        for(HashMap<String, String> locationHM : database.selectAllLocations().values()) {
            //locationImages.put(Integer.parseInt(locationHM.get("Location_ID")), new Image(getClass().getResourceAsStream(locationHM.get("Location_Image_Path"))));
        }

        locations = database.selectAllLocations();
        sensorProperties = database.selectAllSensorProperties();
        rules = database.selectAllRulesIndexed();
        hourlyPCData = database.selectAllHourlyPowerConsumptionData();

        ruleRelationImages.put(0, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/Equal.png")));
        ruleRelationImages.put(1, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/NotEqual.png")));
        ruleRelationImages.put(2, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/Less.png")));
        ruleRelationImages.put(3, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/Greater.png")));
        ruleRelationImages.put(4, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/LessEQ.png")));
        ruleRelationImages.put(5, new Image(getClass().getResourceAsStream("/fxml/images/RuleRelations/GreaterEQ.png")));

        timePrefixImages.put(0, new Image(getClass().getResourceAsStream("/fxml/images/TimePrefixes/Before.png")));
        timePrefixImages.put(1, new Image(getClass().getResourceAsStream("/fxml/images/TimePrefixes/After.png")));
        timePrefixImages.put(2, new Image(getClass().getResourceAsStream("/fxml/images/TimePrefixes/At.png")));
        timePrefixImages.put(3, new Image(getClass().getResourceAsStream("/fxml/images/TimePrefixes/Within.png")));



    }

    //singleton get instance
    public static MeshModel getInstance() {return ourInstance;}

    public void setMyAgent(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }



    public HouseAgent getMyAgent() {
        return myAgent;
    }

    public HashMap<Integer, HashMap<String, String>> getRules() {
        return database.selectAllRulesIndexed();
    }

    public HashMap<Integer, HashMap<String, String>> getSensorProps() {
        return sensorProperties;
    }

    public HashMap<Integer, HashMap<String, String>> getLocations() {
        return locations;
    }

    public HashMap<Integer, HashMap<String, String>> getHourlyPCData() {
        return hourlyPCData;
    }

    public Image getSensorPropImage(int sensorPropID) {
        return this.sensorPropImages.get(sensorPropID);
    }

    public Image getLocationImage(int locationID) {
        return this.locationImages.get(locationID);
    }

    public Image getRuleRelationImage(RuleRelation relation) {
        return ruleRelationImages.get(relation.getValue());
    }

    public Image getTimePrefixImage(RuleTimePrefix ruleTimePrefix) {
        return timePrefixImages.get(ruleTimePrefix.getValue());
    }

    public int getGoalState() {
        return goalState;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public boolean isAm() {
        return am;
    }

    public void setAm(boolean am) {
        this.am = am;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setGoalState(int goalState) {
        this.goalState = goalState;
    }

    public ObservableList<CommunicationProtocol> getProtocolInfo()
    {
        //return FXCollections.observableArrayList(database.selectCommunicationProtocols());
        return null;
    }

    public ObservableList<Location> getLocationList()
    {
        //return FXCollections.observableArrayList(database.selectLocationsOld());
        return null;
    }

    public ObservableList<Actuator> getActuatorList()
    {
        //return FXCollections.observableList(database.selectActuators());
        return null;
    }

    public ObservableList<String> getClassNames() {
        //return FXCollections.observableArrayList(database.selectJavaClasses());
        return null;
    }

    public ObservableList<String> getImageIcons() {
        //return FXCollections.observableArrayList(database.selectImageIcons());
        return null;
    }

    public ArrayList<Image> getImages()
    {
        ArrayList<Image> images = new ArrayList<>();

        ArrayList<String> imageLocations = new ArrayList<>();

        images.addAll(imageLocations.stream().map(Image::new).collect(Collectors.toList()));
        return images;
    }

    public boolean insertNewRule(HashMap<String, String> newRule) {
        if(database.insertData("Rule_Table", newRule) == 1) {
            return true;
        }

        return false;
    }





    public Image getDeviceImage(int imageID)
    {
        Image img ;
        //HashMap<String, String> iconInfo = database.selectDeviceIcon(imageID);
        //img = new Image(iconInfo.get("Icon_URL"));
        return null;
    }

    public HashMap<String, String> getRule() {
        return rule;
    }

    public HashMap<String, String> getSensorProp() {
        return sensorProp;
    }

    public RuleCreationState getRuleCreationState() {
        return ruleCreationState;
    }

    public HashMap<String, String> getLocation() {
        return location;
    }

    public void setLocation(HashMap<String, String> location) {
        this.location = location;
    }

    public int getCurrentTileInfo() {
        return currentTileInfo;
    }

    public HashMap<String, String> getTileInfo() {
        return tileInfo;
    }

    public void setTileInfo(HashMap<String, String> tileInfo) {
        this.tileInfo = tileInfo;
    }

    public void setCurrentTileInfo(int currentTileInfo) {
        this.currentTileInfo = currentTileInfo;
    }

    public void setRuleCreationState(RuleCreationState ruleCreationState) {
        this.ruleCreationState = ruleCreationState;
    }

    public void setSensorProp(HashMap<String, String> sensorProp) {
        this.sensorProp = sensorProp;
    }

    public void setRule(HashMap<String, String> rule) {
        this.rule = rule;
    }

    public void refreshRules() {
        this.rules = database.selectAllRulesIndexed();
    }
}

