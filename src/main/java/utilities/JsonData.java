package utilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jeremiah Smith on 4/18/2017.
 */
public class JsonData {

    private static JsonData ourInstance = new JsonData();

    private JsonParser jsonParser;
    private JsonObject jsonDataObject;
    private JsonObject jsonObject;
    private JsonArray jsonArray;
    private InputStream jsonInputStream;
    private InputStreamReader jsonInputStreamReader;

    //Sensor_Prop_ID, Sensor Property Information
    private HashMap<Integer, HashMap<String,String>> sensorPropertiesMap;
    //Location_ID , Location Information
    private HashMap<Integer, HashMap<String, String>> locationsMap;
    //Protocol_ID, Communication Protocol Information
    private HashMap<Integer, HashMap<String, String>> communicationProtocolsMap;
    //Action_ID , Action Info
    private HashMap<Integer, HashMap<String, String>> actionsMap;
    //Neighbor_ID, Neighbor Information
    private HashMap<Integer, HashMap<String, String>> neighborsMap;
    //Device_ID, Device Information
    private HashMap<Integer, HashMap<String, String>> actuatorsMap;

    private HashMap<Integer, HashMap<String, String>> sensorsMap;

    private HashMap<Integer, HashMap<String, String>> devicesMap;
    private HashMap<Integer, HashMap<String, String>> iconsMap;
    //Rule_ID, Rule Information
    private HashMap<Integer, HashMap<String, String>> activeRulesMap;
    //Rule_ID, Rule Information
    private HashMap<Integer, HashMap<String, String>> passiveRulesMap;
    //Rule_ID, Rule Information
    private HashMap<Integer, HashMap<String, String>> rulesMap;
    //ArrayList<HashMap<String, String> of device action KWH information
    private ArrayList<HashMap<String, String>> deviceActionKWHMap;
    //ArrayList<HashMap<String, String> delta information
    private ArrayList<HashMap<String, String>> deltasMaps;
    //Device_Id, Action_ID
    private ArrayList<HashMap<String, String>> deviceActionMap;

    private HashMap<Integer, ArrayList<HashMap<String, String>>> affectedSensorProperties = new HashMap<>();
    private HashMap<Integer, ArrayList<HashMap<String, String>>> deviceAction = new HashMap<>();





    private JsonData() {
        jsonParser = new JsonParser();
        jsonInputStream = getClass().getClassLoader().getResourceAsStream("data/Data.json");
        jsonInputStreamReader = new InputStreamReader(jsonInputStream);
        jsonDataObject = (JsonObject) jsonParser.parse(jsonInputStreamReader);

        rulesMap = new HashMap<>();
        devicesMap = new HashMap<>();
        sensorPropertiesMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("SensorProperties"), "Sensor_Prop_ID");
        locationsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Locations"), "Location_ID");
        communicationProtocolsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("CommunicationProtocols"), "Protocol_ID");
        actionsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Actions"), "Action_ID");
        neighborsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Neighbors"), "Neighbor_ID");
        actuatorsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Actuators"), "Device_ID");
        sensorsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Sensors"), "Device_ID");
        activeRulesMap =  this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("ActiveRules"), "Rule_ID");
        passiveRulesMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("PassiveRules"), "Rule_ID");
        deltasMaps = this.buildArrayListHashMap(jsonDataObject.get("Deltas").getAsJsonArray());
        deviceActionMap = this.buildArrayListHashMap(jsonDataObject.getAsJsonArray("DeviceActions"));
        iconsMap = this.buildIndexedHashMap(jsonDataObject.getAsJsonArray("Icons"), "Icon_ID");
        devicesMap.putAll(sensorsMap);
        devicesMap.putAll(actuatorsMap);
        rulesMap.putAll(activeRulesMap);
        rulesMap.putAll(passiveRulesMap);
    }

    public static JsonData getInstance() { return ourInstance;}


    public HashMap<Integer, HashMap<String, String>> getCommunicationProtocolsMap() {
        return communicationProtocolsMap;
    }

    public HashMap<Integer, HashMap<String, String>> getActionsMap() {
        return actionsMap;
    }

    public HashMap<Integer, HashMap<String, String>> getActuatorsMap() {
        return actuatorsMap;
    }

    public HashMap<Integer, ArrayList<HashMap<String, String>>> getDeviceAction() {
        return deviceAction;
    }

    public HashMap<Integer, HashMap<String, String>> getLocationsMap() {
        return locationsMap;
    }

    public HashMap<Integer, ArrayList<HashMap<String, String>>> getAffectedSensorProperties() {
        return affectedSensorProperties;
    }

    public HashMap<Integer, HashMap<String, String>> getSensorPropertiesMap() {
        return sensorPropertiesMap;
    }

    public HashMap<Integer, HashMap<String, String>> getRulesMap() {
        return rulesMap;
    }

    public HashMap<Integer, HashMap<String, String>> getNeighborsMap() {
        return neighborsMap;
    }

    public HashMap<Integer, HashMap<String, String>> getSensorsMap() {
        return sensorsMap;
    }



    public HashMap<Integer, HashMap<String, String>> getDevicesMap() {
        return devicesMap;
    }

    public HashMap<Integer, HashMap<String, String>> getActiveRulesMap() {
        return activeRulesMap;
    }

    public HashMap<Integer, HashMap<String, String>> getPassiveRulesMap() {
        return passiveRulesMap;
    }

    public ArrayList<HashMap<String, String>> getDeviceActionKWHMap() {
        return deviceActionKWHMap;
    }

    public ArrayList<HashMap<String, String>> getDeltasMaps() {
        return deltasMaps;
    }

    public ArrayList<HashMap<String, String>> getDeviceActionMap() {
        return deviceActionMap;
    }

    public HashMap<Integer, HashMap<String, String>> getIconsMap() {
        return iconsMap;
    }

    /**
     * Method that will return the HashMap associated with the provided location id
     * @param locationID - the id of the location needed
     * @return HashMap<String, String>
     *
     */
    public HashMap<String, String> getLocation(int locationID) {
        return locationsMap.get(locationID);
    }

    /**
     * Method that will return the HashMap associated with the provided sensor property id
     * @param sensorPropertyID - the id of the location information needed
     * @return HashMap<String, String>
     *
     */
    public HashMap<String, String> getSensorProperty(int sensorPropertyID) {
        return sensorPropertiesMap.get(sensorPropertyID);
    }

    /**
     * Method that will return the HashMap associated with the provided device ID
     * @param deviceID - the id of the device information needed
     * @return HashMap<String, String>
     *     Device_ID
     *     Device_Name
     *     Device_Type
     *     Icon_ID
     *     Class_Name
     */
    public HashMap<String, String> getDevice(int deviceID) {
        return actuatorsMap.get(deviceID);
    }

    /**
     * Method that will return a LinkedList containing all the HashMaps of actionsMap that are associated with the provided deviceID
     * @param deviceID - id of the device that actionsMap are needed
     * @return
     */
    public ArrayList<HashMap<String, String>> getActions(int deviceID) {
        return deviceAction.get(deviceID);
    }

    /**
     * Builds a HashMap<Integer, HashMap<String, String>> from JsonArray Object
     * HashMap<Integer, HashMap<String, String>>
     * @param jsonArray
     * @param indexProperty
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Indexing Property of the data
     *     HashMap<String, String> - HashMap with Json Object Keys, Values
     */
    private HashMap<Integer, HashMap<String, String>> buildIndexedHashMap(JsonArray jsonArray, String indexProperty) {
        HashMap<Integer, HashMap<String,String>> indexMap = new HashMap<>();
        for(int i = 0; i < jsonArray.size(); i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            JsonObject jo = (JsonObject) jsonArray.get(i);
            Integer index = jo.get(indexProperty).getAsInt();

            for (Map.Entry<String, JsonElement> entry : jo.entrySet()) {
                hashMap.put(entry.getKey(), entry.getValue().getAsString());
            }

            indexMap.put(index, hashMap);
        }

        return indexMap;
    }

    /**
     * Builds an ArrayList<HashMap<String, String>> from a JsonArray Object
     * @param jsonArray
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String> - HashMap with Json Object Keys, Values
     */
    private ArrayList<HashMap<String, String>> buildArrayListHashMap(JsonArray jsonArray) {
        ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
        for(int i = 0; i < jsonArray.size(); i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            JsonObject jo = (JsonObject) jsonArray.get(i);


            for (Map.Entry<String, JsonElement> entry : jo.entrySet()) {
                hashMap.put(entry.getKey(), entry.getValue().getAsString());
            }

            hashMapArrayList.add(hashMap);
        }

        return hashMapArrayList;
    }

}
