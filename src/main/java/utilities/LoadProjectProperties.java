package utilities;

import javafx.scene.image.Image;

import java.io.*;
import java.util.Properties;

/**
 * Created by jeremiah on 7/21/16.
 */
public class LoadProjectProperties {

    private Properties properties;
    private InputStream propertiesFile;
    private int timeSteps;
    private double[] priceSchema;
    private long schedulerTimeout;
    private long simulationTime;
    private int mgmCycles;
    private int alphaPrice;
    private int alphaPower;
    private String defaultSchema;
    private String connectionString;
    private String dbUserName;
    private String dbPassword;
    private String stToken;
    private String stAddress;
    private String socketIOAddress;
    private String databaseDriverClassName;
    private String[] databaseTables;
    private boolean dataFromFile;
    private boolean simulation;
    private boolean dataFromResource;
    private boolean initializationNeeded;
    private boolean connectSockets;
    private int agentDebugLevel;
    private int solverDebugLevel;
    private int factoryDebugLevel;


    private static LoadProjectProperties ourInstance = new LoadProjectProperties();

    public static LoadProjectProperties getInstance() {
        return ourInstance;
    }

    private LoadProjectProperties() {

        properties = new Properties();

        try {
            propertiesFile = getClass().getClassLoader().getResourceAsStream("config/MESH.properties");
            properties.load(propertiesFile);
            timeSteps = Integer.parseInt(properties.getProperty("TimeSteps"));
            alphaPower = Integer.parseInt(properties.getProperty("AlphaPower"));
            alphaPrice = Integer.parseInt(properties.getProperty("AlphaPrice"));
            priceSchema = new double[timeSteps];
            databaseDriverClassName = properties.getProperty("DatabaseDriverClassName");
            String[] prices = properties.getProperty("PriceSchema").split(" ");
            for (int i = 0; i < timeSteps; i++) {
                priceSchema[i] = Double.valueOf(prices[i]);
            }
            schedulerTimeout = Long.parseLong(properties.getProperty("SchedulerTimeout"));
            simulationTime =Long.parseLong(properties.getProperty("SimulationTime"));
            mgmCycles = Integer.parseInt(properties.getProperty("MGMCycles"));
            stAddress = properties.getProperty("STHubAddress");
            stToken = properties.getProperty("STToken");
            socketIOAddress = properties.getProperty("socketIOAddress");
            defaultSchema = properties.getProperty("Schema");
            connectionString = properties.getProperty("ConnectionString");
            dbUserName = properties.getProperty("UserName");
            dbPassword = properties.getProperty("Password");
            databaseTables = properties.getProperty("DatabaseTables").split(",");

            if(properties.getProperty("InitializationNeeded").equalsIgnoreCase("true")) {
                this.initializationNeeded = true;
            }
            else {
                this.initializationNeeded = false;
            }

            if(properties.getProperty("DataFromResource").equalsIgnoreCase("true")) {
                this.dataFromResource = true;
            }
            else {
                this.dataFromResource = false;
            }

            if(properties.getProperty("DataFromFile").equalsIgnoreCase("true")) {
                this.dataFromFile = true;
            }
            else {
                this.dataFromFile = false;
            }

            if(properties.getProperty("SimulationMode").equalsIgnoreCase("true")) {
                simulation = true;
            }
            else {
                simulation = false;
            }

            if(properties.getProperty("connectSockets").equalsIgnoreCase("true")){
                connectSockets = true;
            } else {
                connectSockets = false;
            }

            //debug levels for print statements
            this.agentDebugLevel  = Integer.parseInt(properties.getProperty("AgentDebugLevel"));
            this.solverDebugLevel = Integer.parseInt(properties.getProperty("SolverDebugLevel"));
            this.factoryDebugLevel = Integer.parseInt(properties.getProperty("FactoryDebugLevel"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to load the number of Time Steps the project uses
     * @return int time steps
     */
    public int getTimeSteps() {
        return timeSteps;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public String getDefaultSchema() {
        return defaultSchema;
    }

    public String getDatabaseDriverClassName() {
        return databaseDriverClassName;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public boolean isInitializationNeeded() {
        return initializationNeeded;
    }

    public String[] getDatabaseTables() {
        return databaseTables;
    }

    public String getColumnDataType(String columnName) {
        return properties.getProperty(columnName);
    }

    public String getPrimaryKeyName(String tableName) {
        return properties.getProperty(tableName);
    }

    public String getImagePath(String imageNeeded) {
        return properties.getProperty(imageNeeded);
    }

    public String getSocketIOAddress() {
        return socketIOAddress;
    }

    public int getAlphaPrice() {
        return alphaPrice;
    }

    public int getAlphaPower() {
        return alphaPower;
    }

    public String getStToken() {
        return stToken;
    }

    public String getStAddress() {
        return stAddress;
    }

    /**
     * It returns the time in ms allotted to the scheduler to search for a solution.
     */
    public long getSchedulerTimeoutMs() {
        return schedulerTimeout;
    }

    public double[] getPriceSchema() {
        return priceSchema;
    }

    public int getMgmCycles() {return mgmCycles;}
    
    public boolean isSimulation() {
        return simulation;
    }

    public int getAgentDebugLevel() {
        return agentDebugLevel;
    }

    public int getSolverDebugLevel() {
        return solverDebugLevel;
    }

    public int getFactoryDebugLevel() {
        return factoryDebugLevel;
    }

    public long getSimulationTime() {
        return simulationTime;
    }

    public boolean isDataFromFile() {
        return dataFromFile;
    }
    public boolean isDataFromResource() {
        return dataFromResource;
    }

    public boolean connectSockets(){
        return connectSockets;
    }


}
