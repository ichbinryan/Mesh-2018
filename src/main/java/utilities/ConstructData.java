package utilities;

import devices.DeviceList;
import factories.DeviceListFactory;
import factories.RuleFactory;
import jade.core.AID;
import rules.SchedulingRule;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 4/21/2017.
 */
public class ConstructData {



    private static ConstructData ourInstance = new ConstructData();

    private boolean dataFromFile;
    private Database database;


    private ConstructData() {
        dataFromFile = LoadProjectProperties.getInstance().isDataFromFile();
        database = new Database();
    }

    public static ConstructData getInstance() { return ourInstance;}


    public DeviceList constructDeviceList() {

        return DeviceListFactory.constructDeviceList();
    }


    public ArrayList<SchedulingRule> constructSchedulingRules() {
        ArrayList<SchedulingRule> rules = new ArrayList<>();
        HashMap<Integer, HashMap<String, String>> rulesHM;
        HashMap<Integer, HashMap<String,String>> sensorProps;
        HashMap<Integer, HashMap<String, String>> locations;



        rulesHM = database.selectAllRulesIndexed();
        sensorProps = database.selectAllSensorProperties();
        locations = database.selectAllLocations();



        for(HashMap<String, String> ruleHM : rulesHM.values()) {

            int sensorPropID = Integer.parseInt(ruleHM.get("Sensor_Prop_ID"));
            int locationID = Integer.parseInt(ruleHM.get("Location_ID"));

            HashMap<String, String> affectedSensorPropHM = sensorProps.get(sensorPropID);
            HashMap<String, String> ruleLocationHM = locations.get(locationID);

            rules.add(RuleFactory.constructScheduleRule(ruleHM, affectedSensorPropHM, ruleLocationHM));

        }

        return rules;
    }

    public HashMap<Integer, AID> constructNeighbors() {
        HashMap<Integer, AID> neighbors =  new HashMap<>();
        HashMap<Integer, HashMap<String, String>> neighborsHM;

        neighborsHM = database.selectNeighborsIndexed();


        for(Integer neighborID : neighborsHM.keySet()) {
            HashMap<String, String> neighbor = neighborsHM.get(neighborID);
            AID aid =  new AID(neighbor.get("Neighbor_Agent_Name") + "@" + neighbor.get("Neighbor_Platform_Name"), true);
            aid.addAddresses(neighbor.get("Neighbor_Address"));

            neighbors.put(neighborID, aid);
        }

        return neighbors;
    }


}
