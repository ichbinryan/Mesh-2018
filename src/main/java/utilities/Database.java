package utilities;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 8/5/16.
 * @author Jeremiah Smith
 * edited by ryan 10/23/16
 * edited by Jacob Valdez as of 4/6/2017
 */
public class Database {

    //member variables
    private final LoadProjectProperties LPP;
    private final String CONNECTION_STRING;
    private final String DEFAULT_SCHEMA;
    private final String DB_DRIVER_CLASS_NAME;
    private final String DB_USER_NAME;
    private final String DB_PASSWORD;
    private Connection conn;

    //constructor
    public Database() {
        LPP = LoadProjectProperties.getInstance();
        CONNECTION_STRING = LPP.getConnectionString();
        DEFAULT_SCHEMA = LPP.getDefaultSchema();
        DB_DRIVER_CLASS_NAME = LPP.getDatabaseDriverClassName();
        DB_USER_NAME = LoadProjectProperties.getInstance().getDbUserName();
        DB_PASSWORD = LoadProjectProperties.getInstance().getDbPassword();
    }

    //public methods

    /**
     * Method to build an ArrayList<HashMap<String, String>> of delta information for a given device id
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectActionSPRelations(int deviceID) {
        String sql = "SELECT * FROM Device_Action_SP_Delta WHERE Action_ID = " + deviceID;
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of action information for a given Device_ID
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectDeviceActions(int deviceID) {

        String sql = "SELECT Actions.Action_ID, Actions.Action_Name, Device_Action_Relation.KWH from Actions INNER JOIN Device_Action_Relation ON Device_Action_Relation.Action_ID = Actions.Action_ID WHERE Device_Action_Relation.Device_ID = " + deviceID;
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of all device information
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectDevices() {
        String sql = "SELECT * FROM Device_Table";
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all device information indexed by Device_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Device_ID
     *     HashMap<String, String> Device Information
     *          Column_Name, Column_Value
     */
    public HashMap<Integer, HashMap<String, String>> selectAllDevices() {
        String sql = "SELECT * FROM Device_Table";
        return this.buildIndexingHashMap(sql, "Device_ID");
    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all sensor property information indexed by Sensor_Prop_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Sensor_Prop_ID
     *     HashMap<String, String> Sensor Property Information
     *          Column_Name, Column_Value
     */
    public HashMap<Integer, HashMap<String, String>> selectAllSensorProperties() {
        String sql = "SELECT * FROM Sensor_Properties";

        return this.buildIndexingHashMap(sql, "Sensor_Prop_ID");

    }

    public HashMap<Integer, HashMap<String,String>> selectAllHourlyPowerConsumptionData() {

        String sql = "SELECT * FROM Hourly_Power_Consumption";

        return this.buildIndexingHashMap(sql, "Device_ID");

    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all action information indexed by Action_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Action_ID
     *     HashMap<String, String> Action Information
     *          Column_Name, Column_Value
     */
    public HashMap<Integer, HashMap<String, String>> selectAllActions() {
        String sql = "SELECT * FROM Actions";

        return this.buildIndexingHashMap(sql, "Action_ID");
    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all location information indexed by Location_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Location_ID
     *     HashMap<String, String> Location Information
     *          Column_Name, Column_Value
     */
    public HashMap<Integer, HashMap<String, String>> selectAllLocations() {
        String sql  = "SELECT * FROM Locations";

        return this.buildIndexingHashMap(sql, "Location_ID");
    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all rule information indexed by Rule_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Rule_ID
     *     HashMap<String, String> Rule Information
     *          Column_Name, Column_Value
     */
    public HashMap<Integer, HashMap<String, String>> selectAllRulesIndexed() {
        String sql = "SELECT * FROM Rule_Table";
        return this.buildIndexingHashMap(sql, "Rule_ID");
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of all rule information
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectRules() {
        String sql = "SELECT * FROM Rule_Table";
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of all sensor property information
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectSensorProperties() {
        String sql = "SELECT * FROM Sensor_Properties";
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of all icon information
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectIcons() {
        String sql = "SELECT * FROM Icons";
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to build an ArrayList<HashMap<String, String>> of all neighbor information
     * @return ArrayList<HashMap<String, String>>
     *     HashMap<String, String>
     *         Column_Name, Column_Value
     */
    public ArrayList<HashMap<String, String>> selectNeighbors() {
        String sql = "SELECT * FROM Neighbors";
        return this.buildArrayListHashMap(sql);
    }

    /**
     * Method to get a HashMap<Integer, HashMap<String, String>> of all neighbor information indexed by Neighbor_ID
     * @return HashMap<Integer, HashMap<String, String>>
     *     Integer - Neighbor_ID
     *     HashMap<String, String> Neighbor Information
     */
    public HashMap<Integer, HashMap<String,String>> selectNeighborsIndexed() {
        String sql = "SELECT * FROM Neighbors";
        return this.buildIndexingHashMap(sql, "Neighbor_ID");
    }

    /**
     * Method to get a HashMap<String, String> of icon information for a given icon id
     * @param iconID id of the icon that information is needed
     * @return HashMap<String, String>
     *     Column_Name, Column_Value
     */
    public HashMap<String, String> selectIcon(int iconID) {
        String sql = "SELECT * FROM Icons WHERE Icon_ID =" + iconID;
        return this.buildHashMap(sql);
    }



    /**
     * Method to return all location information from the location table for a given location id
     * @param locationID id of the location
     * @return HashMap<String, String>
     *     Location_ID, ID
     *     Location_Type_Name, Type Name of the Location eg Room/Actuator
     */
    public HashMap<String, String> selectLocation(int locationID) {
        //String sql = "SELECT Location_ID, Location_Name, Locations.Location_Type_ID, Location_Type_Name FROM Locations INNER JOIN Location_Types ON Locations.Location_Type_ID = Location_Types.Location_Type_ID WHERE Location_ID = " + locationID;
        String sql = "SELECT * from Locations WHERE Location_ID = " + locationID;
        return this.buildHashMap(sql);
    }

    /**
     * Method to return all sensor property information form sensor property table for a given sensor property id
     * @param sensorPropID id of the sensor property
     * @return HashMap<String, String>
     *     Sensor_Prop_ID, ID
     *     Sensor_Prop_Name, Sensor property name
     */
    public HashMap<String, String> selectSensorProperty(int sensorPropID) {
        String sql = "SELECT * FROM Sensor_Properties WHERE Sensor_Prop_ID =" + sensorPropID;
        return this.buildHashMap(sql);
    }

    /**
     * Method to insert data into the database from a HashMap of Column Names and Data Value.
     * Will get the primary keys and column data types from project properties.
     * @param tableName table name for the insertion
     * @param data HashMap<String, String>
     *             Column Name , Data Value
     */
    public int insertData(String tableName, HashMap<String, String> data) {

        String sql = "INSERT INTO " + tableName + " ";
        String primaryKey = LoadProjectProperties.getInstance().getPrimaryKeyName(tableName);
        String values = "(";
        String columns = "(";
        int numberOfRecordsAffected = 0;

        for(String s : data.keySet()) {
                //get data type associated with the table column from MESH.properties
                String dataType = LoadProjectProperties.getInstance().getColumnDataType(s);
                if(dataType != null) {
                    switch (dataType) {
                        case "INTEGER": {
                            columns += s + ", ";
                            values += data.get(s) + ", ";
                            break;
                        }
                        case "TEXT": {
                            columns += s + ", ";
                            values += "'" + data.get(s) + "', ";
                            break;
                        }
                        case "REAL": {
                            columns += s + ", ";
                            values += data.get(s) + ", ";
                            break;
                        }

                    }
                }
        }

        columns = columns.substring(0, columns.length() -2) + ")";
        values = values.substring(0, values.length() -2) + ")";



        sql += columns + " VALUES " + values;

        try{
            createConnection();
            Statement insertData = conn.createStatement();
            numberOfRecordsAffected = insertData.executeUpdate(sql);
            destroyConnection();
        }catch (SQLException e) {
            e.printStackTrace();
        }

        return numberOfRecordsAffected;
    }

    /**
     * Method to truncate all data in the database for the tables listed in MESH.properties
     */
    public void truncateDatabase() {
        String [] databaseTables = LoadProjectProperties.getInstance().getDatabaseTables();
        try {
            this.createConnection();

            Statement constraintSTMT = conn.createStatement();
            constraintSTMT.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");

            for (String tableName : databaseTables) {
                String sql = "TRUNCATE " + tableName;
                Statement truncateSTMT = conn.createStatement();
                truncateSTMT.executeUpdate(sql);
            }
            constraintSTMT.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            this.destroyConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to truncate data in the provided table name
     * @param tableName table name to be truncated
     */
    public void truncateTable(String tableName) {
        try {
            this.createConnection();
            Statement constraintSTMT = conn.createStatement();
            constraintSTMT.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");

            String sql = "TRUNCATE " + tableName;
            Statement truncateSTMT = conn.createStatement();
            truncateSTMT.executeUpdate(sql);

            constraintSTMT.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            this.destroyConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void updateHourly(String timestamp, float power, int deviceID, String device_name){
        //System.out.println(device_name);
        String sql = "INSERT INTO Hourly_Power_Consumption (Device_ID, Time_Stamp, WH_Usage, WH_Usage_Double, Device_Name) VALUES (" + deviceID + ", '" + timestamp +"', " + power +", "+ (double) power + ", '" + device_name + "')";
        //System.out.println(sql); //testing stuff
        try {
            createConnection();
            Statement insertData = conn.createStatement();
            insertData.executeUpdate(sql);
            destroyConnection();
        }catch(SQLException e){
            System.out.println("Database error, have we moved on to a new day?");
            e.printStackTrace();
        }
    }

    //private methods
    /**
     * Method to create a database connection
     */
    private void createConnection() {
        try {
            Class.forName(DB_DRIVER_CLASS_NAME);


            conn = DriverManager.getConnection("jdbc:mysql:" + CONNECTION_STRING + "?useSSL=false", DB_USER_NAME, DB_PASSWORD);

            conn.setSchema(DEFAULT_SCHEMA);
        }
        catch (Exception e) {
            System.err.println("Error Creating Connection:" + e.getMessage());
        }

    }

    /**
     * Method to destroy the connection to the database
     */
    private void destroyConnection() {
        try {

            if (this.conn != null){
                this.conn.close();
            }

        }
        catch (Exception e) {
            System.err.println("Error Shutting Down Connection: " + e);
        }
    }


    /**
     * Method that build an ArrayList<HashMap <String, String>> of all the records of the given sql statement
     * @param sql SQL statement to run in the database
     * @return ArrayList<HashMap <String, String>>
     *     HashMap is the Column Name paired with the data
     */
    private ArrayList<HashMap<String, String>> buildArrayListHashMap(String sql) {
        ArrayList<HashMap<String, String>> hml = new ArrayList<>();
        createConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                HashMap<String,String> hm = new HashMap();
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    hm.put(resultSet.getMetaData().getColumnName(i+1), resultSet.getString(i+1));
                }
                hml.add(hm);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();
        return hml;
    }

    /**
     * Method that build an HashMap <String, String>> of one record of the given sql statement
     * @param sql SQL statement to run in the database
     * @return HashMap<String, String>
     *     HashMap is the Column Name paired with the data
     */
    private HashMap<String, String> buildHashMap(String sql) {
        HashMap<String, String> hm = new HashMap<>();
        createConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    hm.put(resultSet.getMetaData().getColumnName(i+1), resultSet.getString(i+1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();
        return hm;
    }

    /**
     * Method to build a HashMap<Integer, HashMap<String, String>> for a given sql statement
     * @param sql sql statement to run on the database
     * @param indexingProperty column that will be in integer index for the HashMap
     * @return
     */
    private HashMap<Integer, HashMap<String, String>> buildIndexingHashMap(String sql, String indexingProperty) {
        HashMap<Integer, HashMap<String, String>> indexingHM = new HashMap<>();

        createConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                int index = resultSet.getInt(indexingProperty);
                HashMap<String, String> hashMap = new HashMap<>();
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    hashMap.put(resultSet.getMetaData().getColumnName(i+1), resultSet.getString(i+1));
                }
                indexingHM.put(index, hashMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();


        return indexingHM;
    }



    /**
     * Start of added methods for average updater
     *
     */
    /**
     * Updates the average in the database every x time step.
     * @param usage power usage
     * @param id device id
     */
    public void writeAverage(float usage, int id) {

        createConnection();
        try {
            String sql;

            sql = "SELECT Avg_Daily_Consumption FROM Power_Consumption WHERE Device_ID = " + id;
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            float curr_val = rs.getFloat("Avg_Daily_Consumption");
            curr_val = curr_val + usage / 4;
            sql = "UPDATE Power_Consumption SET Avg_Daily_Consumption =" + curr_val +
                    "WHERE Device_ID=" + id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
