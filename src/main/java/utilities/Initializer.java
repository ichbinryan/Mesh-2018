package utilities;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 4/18/2017.
 */
public class Initializer {
    private Database database;
    private int insertedRecords;


    public Initializer () {
        database = new Database();
        insertedRecords = 0;
    }



    public void initializeDatabase() {

        database.truncateDatabase();

        //insert actions
        System.out.println("InsertingActions");
        for(HashMap<String, String> action : JsonData.getInstance().getActionsMap().values()) {
            insertedRecords += database.insertData("Actions", action);
        }

        System.out.println(insertedRecords + " Actions Inserted");


        //insert communication protocols
        insertedRecords = 0;
        for(HashMap<String, String> commPro : JsonData.getInstance().getCommunicationProtocolsMap().values()) {

            insertedRecords += database.insertData("Communication_Protocols", commPro);

        }

        System.out.println(insertedRecords + " Communication Protocols Inserted");

        //insert neighbors
        insertedRecords = 0;
        for(HashMap<String, String> neighbor : JsonData.getInstance().getNeighborsMap().values()) {

            insertedRecords += database.insertData("Neighbors", neighbor);

        }

        System.out.println(insertedRecords + " Neighbors Inserted");

        //insert sensor properties
        insertedRecords = 0;
        for(HashMap<String, String> sensorProperty : JsonData.getInstance().getSensorPropertiesMap().values()) {

            insertedRecords += database.insertData("Sensor_Properties", sensorProperty);

        }

        System.out.println(insertedRecords + " Sensor Properties Inserted");

        //insert icons
        insertedRecords = 0;
        for(HashMap<String, String> icon : JsonData.getInstance().getIconsMap().values()) {

            insertedRecords += database.insertData("Icons", icon);

        }

        System.out.println(insertedRecords + " Icons Inserted");

        //insert locations
        insertedRecords = 0;
        for(HashMap<String, String> location : JsonData.getInstance().getLocationsMap().values()) {

            insertedRecords += database.insertData("Locations", location);

        }

        System.out.println(insertedRecords + " Locations Inserted");

        //insert rules
        insertedRecords = 0;
        for(HashMap<String, String> rule : JsonData.getInstance().getRulesMap().values()) {

            insertedRecords += database.insertData("Rule_Table", rule);

        }

        System.out.println(insertedRecords + " Rules Inserted");

        //insert devices
        insertedRecords = 0;
        for(HashMap<String, String> device : JsonData.getInstance().getDevicesMap().values()) {

            insertedRecords += database.insertData("Device_Table", device);

        }

        System.out.println(insertedRecords + " Devices Inserted");

        //insert deltas
        insertedRecords = 0;
        for(HashMap<String, String> deltaMap : JsonData.getInstance().getDeltasMaps()) {
            insertedRecords += database.insertData("Device_Action_SP_Delta", deltaMap);
        }

        System.out.println(insertedRecords + " Deltas Inserted");

        //insert device action relation
        insertedRecords = 0;
        for(HashMap<String, String> deviceAction : JsonData.getInstance().getDeviceActionMap()) {

            insertedRecords += database.insertData("Device_Action_Relation", deviceAction);
        }

        System.out.println(insertedRecords + " Device Action Relations Inserted");
    }


    public static void main(String args[]) {

        Initializer initializer =  new Initializer();

        initializer.initializeDatabase();

    }


}


