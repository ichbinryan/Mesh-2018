package actions;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 11/11/16.
 */
public class ActionList {

    private HashMap<Integer, Action> actions;

    public ActionList(HashMap<Integer, Action> actions) {

        this.actions = actions;
    }

    public HashMap<Integer, Action> getActions() {
        return actions;
    }

    public void setActions(HashMap<Integer, Action> actions) {

        this.actions = actions;
    }

    public void addAction(Action actionToAdd) {
        this.actions.put(actionToAdd.getActionID(), actionToAdd);
    }

    public Action getAction(int actionID) {
        return actions.get(actionID);
    }

    @Override
    public String toString() {
        return "ActionList{" +
                "actions=" + actions +
                '}';
    }
}
