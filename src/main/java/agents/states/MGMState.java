package agents.states;

/**
 * Created by jeremiah on 6/9/17.
 */
public enum MGMState {

    INITIALIZE(0),
    GENERATING_SCHEDULE(1),
    WAITING_FOR_GAIN(2),
    SENDING_GAIN_EP_MESSAGE(3),
    GAIN_MESSAGE_RECEIVED(4),
    GAIN_MESSAGE_NOT_NEEDED(5);

    private int value;

    MGMState(int value) {
        this.value = value;
    }



}
