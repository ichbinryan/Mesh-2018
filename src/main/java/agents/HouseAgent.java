package agents;

import agents.states.MGMState;
import behaviours.general.GeneralMessageChecker;
import behaviours.general.InitializeInterface;
import behaviours.general.RegisterWithDF;
import behaviours.solver.InitializeSolver;
import behaviours.schedule.MGMInfoMessageHandler;
import behaviours.schedule.PopulateDeviceList;
import behaviours.schedule.PopulateNeighborList;
import behaviours.testing.*;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import devices.*;
import factories.LocationListFactory;
import factories.SensorPropertyListFactory;
import io.socket.client.IO;
import io.socket.client.Socket;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import locations.LocationList;
import mgm.MGMInfo;
import org.json.JSONObject;
import rules.SchedulingRule;
import schedule.Schedule;
import sensorproperties.SensorPropList;
import solver.solver;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

/**
 * Created by Jeremiah Smith on 7/20/16.
 */
public class HouseAgent extends Agent {


    private Gson gson = new Gson();
    private HashMap<Integer, AID> neighbors;
    private DeviceList deviceList;
    private solver solver;
    private Schedule currentSchedule;
    private Socket socket;
    private int agentDebugLevel = LoadProjectProperties.getInstance().getAgentDebugLevel();
    private HashMap<Integer, ArrayList<HashMap<AID, MGMInfo>>> mgmInformation = new HashMap<>();
    private PowerHandler ph = PowerHandler.getInstance();
    private int simCycle;
    private MGMState mgmState;
    private HashMap<Integer, PriorityQueue<Double>> gains;
    private HashMap<Integer, SchedulingRule> schedulingRules;
    private SensorPropList allSensorProps = SensorPropertyListFactory.constructSensorPropListAll();
    private LocationList allLocations = LocationListFactory.constructLocationListAll();

    public MGMState getMgmState() {
        return mgmState;
    }


    public DeviceList getDeviceList() {
        return deviceList; }


    public int getSimCycle() {
        return simCycle;
    }

    public HashMap<Integer, AID> getNeighbors() {
        return neighbors;
    }

    public solver getSolver() {
        return solver;
    }

    public int getAgentDebugLevel() { return agentDebugLevel; }

    public Schedule getCurrentSchedule() {
        return currentSchedule;
    }

    public SensorPropList getAllSensorProps() {
        return allSensorProps;
    }

    public void setAllSensorProps(SensorPropList allSensorProps) {
        this.allSensorProps = allSensorProps;
    }

    public LocationList getAllLocations() {
        return allLocations;
    }

    public void setAllLocations(LocationList allLocations) {
        this.allLocations = allLocations;
    }

    public void setMgmState(MGMState mgmState) {
        this.mgmState = mgmState;
    }

    public void setDeviceList(DeviceList deviceList) { this.deviceList = deviceList; }
    public void setSimCycle(int simCycle) {
        this.simCycle = simCycle;
    }

    public void setNeighbors(HashMap<Integer, AID> neighbors) {
        this.neighbors = neighbors;
    }

    public void setSolver(solver solver) {
        this.solver = solver;
    }

    public HashMap<Integer, ArrayList<HashMap<AID, MGMInfo>>> getMGMInformation() {
        return mgmInformation;
    }

    public void setCurrentSchedule(Schedule currentSchedule) {
        this.currentSchedule = currentSchedule;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public ArrayList<HashMap<AID, MGMInfo>> getMGMInfoCycle(int cycleNumber) {
        return mgmInformation.get(cycleNumber);
    }


    public void setup() {



        this.setupSocketIO();

        this.mgmState = MGMState.INITIALIZE;

        //adding startup behaviors
        SequentialBehaviour sequentialBehaviour = new SequentialBehaviour(this);
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Agent Setup", "*"));
        //register agent with df
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Register with DF Behavior", "+"));
        sequentialBehaviour.addSubBehaviour(new RegisterWithDF(this, "house-agent", "HouseAgent"));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Ending Register with DF Behavior", "+"));

        //populate neighbor list
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Populate Neighbor List Behavior", "+"));
        sequentialBehaviour.addSubBehaviour(new PopulateNeighborList(this));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintNeighborList(this));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Ending Populate Neighbor List Behavior", "+"));

        //populate device list
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Populate Device List Behavior", "+"));
        sequentialBehaviour.addSubBehaviour(new PopulateDeviceList(this));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDeviceList(this));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Ending Populate Device List Behavior", "+"));

        //initialize solver
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Initialize Solver Behavior", "+"));
        sequentialBehaviour.addSubBehaviour(new InitializeSolver(this));
        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Ending Initialize Solver Behavior", "+"));

        if(agentDebugLevel > 0) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "End Agent Setup", "*"));

        sequentialBehaviour.addSubBehaviour(new InitializeInterface(this));

        this.addBehaviour(sequentialBehaviour);
        this.addBehaviour(new GeneralMessageChecker(this));
        this.addBehaviour(new MGMInfoMessageHandler(this));
        if(LoadProjectProperties.getInstance().isSimulation()) {

            this.addBehaviour(new DaySimulation(this, 10000));
        }

        //this is for the mesh interface


    }

    public void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private void setupSocketIO() {

        try {
            socket = IO.socket(LoadProjectProperties.getInstance().getSocketIOAddress());

            socket.on(Socket.EVENT_CONNECT, args -> {
                System.out.println("Connected to server");
            });

            //message to activate an action
            socket.on("device.action", args -> {
                JsonElement element = gson.fromJson(args[0].toString(), JsonElement.class);
                JsonObject actionInfo = element.getAsJsonObject();



                int deviceID = Integer.parseInt(actionInfo.get("Device_ID").getAsString());
                int actionID = Integer.parseInt(actionInfo.get("Action_ID").getAsString());
                if(agentDebugLevel > 0) {
                    System.out.println("Attempting to activate action id : " + actionID + " for deviceID: " + deviceID);
                }

                Device device = deviceList.getDevice(deviceID);
                if( device instanceof Actuator) {
                    Actuator actuator = (Actuator) device;
                    actuator.activate(actuator.getActionList().getAction(actionID));
                }

            });

            //message to get current schedule
            socket.on("device.getschedule", args -> {
                //todo after demo take out schedule generation
                double[] np = new double[LoadProjectProperties.getInstance().getTimeSteps()];
                for(int i = 0; i < np.length; i++){
                    np[i] = 0.0;
                }
                System.out.println("device.getSchedule");
                //currentSchedule = solver.getFirstSchedule();
                currentSchedule = solver.getSchedule(np);
                System.out.println(currentSchedule.getJSONScheduleObject());
                socket.emit("device.schedule", currentSchedule.getJSONScheduleObject());
            });

//            socket.on("device.getCummalativePowerConsumption", args -> {
//                JSONArray jarray = PowerHandler.getInstance().constructUsage();
//                System.out.println(jarray.toString());
//            });

            socket.on("device.getReading", args -> {
                JsonElement element = gson.fromJson(args[0].toString(), JsonElement.class);
                JsonObject actionInfo = element.getAsJsonObject();

                int deviceID = Integer.parseInt(actionInfo.get("Device_ID").getAsString());

                if(agentDebugLevel > 0) {
                    System.out.println("Attempting to get sensor reading for deviceID: " + deviceID);
                }

                Device device = deviceList.getDevice(deviceID);
                System.out.println(device.toString());
                System.out.println(device instanceof Sensor);
                if (device instanceof Sensor) {
                    System.out.println("HERE");
                    Sensor sensor = (Sensor) device;
                    JSONObject ob = new JSONObject();
                    ob.put("value", sensor.getState());
                    socket.emit("device.reading", ob);
                }
            });

            socket.on("rule.newRule", args-> {
                JsonElement element = gson.fromJson(args[0].toString(), JsonElement.class);
                JsonObject rule = element.getAsJsonObject();
                System.out.println(rule.toString());
            });

            socket.connect();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}
