package agents;


import behaviours.general.RegisterWithDF;
import behaviours.testing.MGMInfoMessage;
import jade.core.Agent;
import utilities.LoadProjectProperties;

/**
 * Created by Jeremiah Smith on 2/8/17.
 */
public class MessageTestingAgent extends Agent {

    private int debugLevel = LoadProjectProperties.getInstance().getAgentDebugLevel();

    public void setup() {

        this.addBehaviour(new RegisterWithDF(this, "message-testing-agent", "MessageTestingAgent"));
        this.addBehaviour(new MGMInfoMessage(this));
    }

    public void takeDown() {

    }

}
