package neighbor;

import mgm.GainList;
import mgm.MGMInfoList;
import jade.core.AID;

import java.util.Arrays;

/**
 * Created by Jeremiah Smith on 6/24/16.
 */
public class Neighbor {

    private int neighborID;
    private String name;
    private String address;
    private String platformName;
    private Float [] energyProfile;
    private Float scheduleGain;
    private int currentCycle;
    private boolean gainReceived;
    private boolean energyProfileReceived;
    private boolean cycleCheckPassed;
    private GainList gainList;

    private MGMInfoList mgmInfoList;

    public Neighbor (int neighborID, String name, String address, String platformName) {
        this.neighborID = neighborID;
        this.name = name;
        this.address = address;
        this.platformName = platformName;
        currentCycle = -1;
        energyProfileReceived = false;
        gainReceived = false;
        cycleCheckPassed = false;
        gainList = new GainList();
        mgmInfoList = new MGMInfoList();
    }

    //accessor methods
    public int getNeighborID() { return neighborID; }
    public String getAddress() { return address; }
    public String getPlatformName() { return platformName; }
    public Float[] getEnergyProfile() { return energyProfile; }
    public boolean isEnergyProfileReceived() {
        return energyProfileReceived;
    }
    public boolean isGainReceived() {
        return gainReceived;
    }
    public boolean isCycleCheckPassed() {
        return cycleCheckPassed;
    }
    public Float getScheduleGain() {
        return scheduleGain;
    }
    public String getName() {
        return name;
    }
    public int getCurrentCycle() {
        return currentCycle;
    }
    public GainList getGainList() { return gainList; }
    public MGMInfoList getMgmInfoList() { return mgmInfoList; }

    //modification methods
    public void setNeighborID(int neighborID) {
        this.neighborID = neighborID;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }
    public void setEnergyProfile(Float [] energyProfile) {
        this.energyProfile = energyProfile;
    }
    public void setEnergyProfileReceived(boolean energyProfileReceived) {
        this.energyProfileReceived = energyProfileReceived;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScheduleGain(Float scheduleGain) {
        this.scheduleGain = scheduleGain;
    }

    public void setGainReceived(boolean gainReceived) {
        this.gainReceived = gainReceived;
    }

    public void setCycleCheckPassed(boolean cycleCheckPassed) {
        this.cycleCheckPassed = cycleCheckPassed;
    }

    public void setCurrentCycle(int currentCycle) {
        this.currentCycle = currentCycle;
    }

    public AID generateAID() {
        AID aid =  new AID(name + "@" + platformName, true);
        aid.addAddresses(address);

        return aid;
    }

    public void printInfo () {
        System.out.println(this.toString());
        System.out.println("AID: " + this.generateAID().toString());
    }

    @Override
    public String toString() {
        return "Neighbor{" +
                "neighborID=" + neighborID +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", platformName='" + platformName + '\'' +
                ", energyProfile=" + Arrays.toString(energyProfile) +
                ", scheduleGain=" + scheduleGain +
                ", gainReceived=" + gainReceived +
                ", energyProfileReceived=" + energyProfileReceived +
                ", cycleCheckPassed=" + cycleCheckPassed +
                '}';
    }
}
