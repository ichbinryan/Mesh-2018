package solver;


import actions.Action;
//import com.sun.org.apache.xpath.internal.SourceTree;
import devices.Actuator;
import devices.Sensor;
import models.PredictiveModel;
import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.search.*;
import rules.RuleTimePredicate;
import rules.RuleType;
import rules.SchedulingRule;
import schedule.Schedule;
import schedule.ScheduleEntry;
import sensorproperties.SensorProperty;
import utilities.LoadProjectProperties;
import utilities.StdUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Jeremiah Smith on 1/16/2017.
 */
public class solver implements SolverInterface {

    //variable declarations
    private IntVar[] decisionVariables;
    private IntVar costFunction;
    private IntVar[][] predictiveModel;
    private IntVar[] aggregatedPowerConsumption;
    private IntVar[][] x; // is x?
    private int[][][] delta;
    private int[][] powerUsed;
    private int bestCost;
    private int maxAggregatedPower;
    private int minAggregatedPower;
    private int maxAggregatedPrice;
    private int debugLevel = LoadProjectProperties.getInstance().getSolverDebugLevel();
    private Store store;
    private Search<IntVar> search;
    private final int HORIZON;
    private final int KILO_WATT_TO_WATT;
    private final int  CENTS_TO_DOLLARS;
    private final int DELTA_SCALE;
    private final int ALPHA_POWER;
    private final int ALPHA_PRICE;
    private double scaleFactor;
    private double[] bgLoadsKWh;
    private double[] powerPriceKWh;
    private ArrayList<SchedulingRule> passiveRules;
    private ArrayList<SchedulingRule> activeRules;
    private Map<PredictiveModel, Integer> cpModelId;
    private Map<Integer, PredictiveModel> modelMap;
    private Map<Integer, Actuator> actuatorsMap;
    private Map<Integer, Sensor> sensorsMap;


    //constructors
    public solver(ArrayList<SchedulingRule> schedulingRules, double [] bgLoadsKWh) {
        this.HORIZON = LoadProjectProperties.getInstance().getTimeSteps();
        this.KILO_WATT_TO_WATT = 10;
        this.CENTS_TO_DOLLARS = 10;
        this.DELTA_SCALE = 10;
        this.ALPHA_POWER = LoadProjectProperties.getInstance().getAlphaPower();
        this.ALPHA_PRICE = LoadProjectProperties.getInstance().getAlphaPrice();
        this.scaleFactor = KILO_WATT_TO_WATT * CENTS_TO_DOLLARS;
        this.bestCost = 100000;
        this.passiveRules = new ArrayList<>();
        this.activeRules =  new ArrayList<>();
        this.cpModelId = new HashMap<>();
        this.modelMap = new HashMap<>();
        this.actuatorsMap = new HashMap<>();
        this.sensorsMap =  new HashMap<>();
        this.store = new Store();
        this.maxAggregatedPower = 0;
        this.minAggregatedPower = 0;
        this.maxAggregatedPrice = 0;
        this.powerPriceKWh = LoadProjectProperties.getInstance().getPriceSchema();

        if(bgLoadsKWh != null) {
            this.bgLoadsKWh = bgLoadsKWh;
        }
        else {
            this.bgLoadsKWh = new double[HORIZON];
            //set all bgloads to 0

        }

        activeRules.addAll(
                schedulingRules.stream().filter(schedulingRule -> schedulingRule.getRuleType() == RuleType.ACTIVE).collect(Collectors.toList()));


        for (SchedulingRule rule : schedulingRules) {
            if ( rule.getRuleType() == RuleType.PASSIVE) {
                boolean found = false;
                // for all the active rules, take the actuators they affect.
                for (SchedulingRule ar : activeRules) {
                    for (Actuator actuator : ar.getPredictiveModel().getActuators()) {
                        for (Action action : actuator.getActionList().getActions().values()) {
                            if (action.getAffectedSensorProperties().getSensorProperties().containsValue(rule.getAffectedProperty())) {
                                passiveRules.add(rule);
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found) break;
                }
            }
        }

        ArrayList<SchedulingRule> usedRules = new ArrayList<>();
        usedRules.addAll(activeRules);
        usedRules.addAll(passiveRules);

        if(debugLevel > 0){
            for (SchedulingRule rule : usedRules) {
                System.out.println(rule.toString());
            }
        }

        int mid = 0;
        for (SchedulingRule r : usedRules) {
            if (!cpModelId.containsKey(r.getPredictiveModel())) {

                cpModelId.put(r.getPredictiveModel(), mid);
                modelMap.put(mid, r.getPredictiveModel());
                mid++;
            }
        }

        int aid = 0, sid = 0;
        Set<Actuator> actuatorSet = new HashSet<>();

        // Populate set of sensors and actuators
        for (SchedulingRule r : usedRules) {
            Sensor sensor = r.getPredictiveModel().getSensor();
            if (!sensorsMap.containsValue(sensor)) {
                sensor.setCpID(sid);
                sensorsMap.put(sid, sensor);
                sid++;
            }
            actuatorSet.addAll(r.getPredictiveModel().getActuators().stream().collect(Collectors.toList()));
        }
        for (Actuator actuator : actuatorSet) {
            if (!actuatorsMap.containsValue(actuator)) {
                actuator.setCpID(aid);
                actuatorsMap.put(aid, actuator);
                aid++;
            }
        }

        if (debugLevel > 0) {
            System.out.println("List of all actuators and sensors in the CP model");
            System.out.println("Actuators:");
            for (Actuator s : actuatorsMap.values()) {
                System.out.println("\t" + s.getName());
            }
            System.out.println("Sensors:");
            for (Sensor s : sensorsMap.values()) {
                System.out.println("\t" + s.getName());
            }
        }


        for (Actuator a : actuatorsMap.values()) {
            int maxPower = 0;
            for (Action s : a.getActionList().getActions().values()) {
                maxPower = Math.max(maxPower, scaleAndRoundPower(s.getActionKWH()));
            }
            maxAggregatedPower += maxPower;
        }

        int mp = 0;
        for (double p : LoadProjectProperties.getInstance().getPriceSchema()) {
            mp = Math.max(mp, scaleAndRoundPrice(p));
        }
        maxAggregatedPrice = mp * maxAggregatedPower;

        if (debugLevel > 0) {
            System.out.println("Min Aggr power = " + minAggregatedPower);
            System.out.println("Max Aggr Power = " + maxAggregatedPower);
            System.out.println();
        }

        int nbActuators = actuatorsMap.size();
        // The properties associated to the rules
        int nbPredictiveModels = cpModelId.size();
        // The maximal number of actions for any actuator device in the pool of active actuators
        int nbActions    = 0;
        for (Actuator a : actuatorsMap.values()) {
            nbActions = Math.max(nbActions, a.getActionList().getActions().size());
        }
        /*
        each panel in the 3d array is for one predictive model



         */
        delta  = new int[nbPredictiveModels][nbActuators][nbActions];
        populateDeltaArray(delta);
        powerUsed  = new int[nbActuators][nbActions];
        populatePowerArray(powerUsed);

    }

    //accessor and modifier methods

    @Override
    public Schedule getSchedule(double[] neighborPower) {
        createModel(neighborPower);

        System.out.println(store.toString());
        //if (searchOptimal()) {
        if (searchMostConstrainedStatic()) {
            if (debugLevel > 0)
                printPredictiveModels();
            return constructSchedule();
        } else {  //if search fails, create a generic RulesSchedule object and set utility to max size of double.
            Schedule schedule = new Schedule();
            System.out.println("Error: Schedule not found!");

            schedule.setUtility(Double.MAX_VALUE);
            return schedule;
        }
    }


    @Override
    public Schedule getFirstSchedule() {
        double neighborPower[] = new double[HORIZON];
        for (int i = 0; i < HORIZON; i++) neighborPower[i] = 0;
        createModel(neighborPower);

        if (searchWithRestarts(1000)) {
            if (debugLevel > 0)
                printPredictiveModels();
            return constructSchedule();
        } else {  //if search fails, create a generic RulesSchedule object and set utility to max size of double.
            System.out.println("Error: First Schedule not found!");
            Schedule schedule = new Schedule();

            /*
            int nbActuators = actuatorsMap.size();
            for (int aID = 0; aID < nbActuators; aID++) {
                SimulatedDevice actuator = actuatorsMap.get(aID);
                ActuatorSchedule aSchedule = new ActuatorSchedule(actuator);
            }
            */

            schedule.setUtility(Double.MAX_VALUE);
            return schedule;
        }
    }

    @Override
    public Schedule getBaseLineSchedule(double[] neighborPower) {
        createModel(neighborPower);

        if (searchSmallestDomain()) {
            if (debugLevel > 0)
                printPredictiveModels();
            return constructSchedule();
        } else { //if search fails, create a generic RulesSchedule object and set utility to max size of double.
            System.out.println("Error: Baseline Schedule not found!");
            Schedule schedule = new Schedule();

            schedule.setUtility(Double.MAX_VALUE);
            return schedule;

        }
    }

    //private methods

    private int scaleAndRoundPower(double n) {
        return (int)(n * KILO_WATT_TO_WATT);
    }

    private int scaleAndRoundPrice(double n) {
        return (int)(n * CENTS_TO_DOLLARS);
    }

    private int scaleAndRoundDelta(double n) {
        return (int)(n * DELTA_SCALE);
    }

    private void createModel(double [] neighborPower) {
        final int NUM_ACTUATORS = actuatorsMap.size();
        final int PREMODEL_SIZE =  cpModelId.size();
        final int MIN_PROPERTY_DOMAIN = -200 * DELTA_SCALE;
        final int MAX_PROPERTY_DOMAIN = 200 * DELTA_SCALE;
        decisionVariables = new IntVar[NUM_ACTUATORS * HORIZON];
        predictiveModel = new IntVar[PREMODEL_SIZE][HORIZON];
        aggregatedPowerConsumption = new IntVar[HORIZON];
        x = new IntVar[NUM_ACTUATORS][HORIZON];
        //x is just the schedule...?
        double maxNeighborPower = 0;
        for (double pw : neighborPower) {
            maxNeighborPower = Math.max(maxNeighborPower, scaleAndRoundPower(pw));
        }
        maxAggregatedPower += maxNeighborPower;

        createIntVar2DArrayNamed(predictiveModel, store, "predictiveModel", MIN_PROPERTY_DOMAIN, MAX_PROPERTY_DOMAIN);
        createIntVarArrayNamed(aggregatedPowerConsumption, store, "aggregatedPowerConsumption", minAggregatedPower, maxAggregatedPower);


        for(int aID = 0; aID < NUM_ACTUATORS; aID++ ) {
            Actuator actuator = actuatorsMap.get(aID);
            int maxDom = actuator.getActionList().getActions().size();
            createIntVarArrayNamed(x[aID], store, "x_" + actuator.getName(), 0, maxDom - 1);
        }

        IntVar objPrice = new IntVar(store, "objPrice", 0, maxAggregatedPrice * HORIZON);
        IntVar objPower = new IntVar(store, "objPower", 0, maxAggregatedPower * HORIZON);

        for (PredictiveModel model : cpModelId.keySet()) {
            int mID = cpModelId.get(model);
            createPredictiveModelConstraints(model, predictiveModel[mID],delta[mID],x);
        }

        for (SchedulingRule rule : activeRules) {
            PredictiveModel model = PredictiveModel.valueOf(rule.getLocation(), rule.getAffectedProperty());
            int mID = cpModelId.get(model);

            createRuleConstraints(rule, predictiveModel[mID]);
        }

        for (SchedulingRule rule : passiveRules) {
            PredictiveModel model = PredictiveModel.valueOf(rule.getLocation(), rule.getAffectedProperty());
            int mID = cpModelId.get(model);

            createRuleConstraints(rule, predictiveModel[mID]);
        }

        for (int t = 0; t < HORIZON; t++) {
            createAggrPowerConstraint(aggregatedPowerConsumption[t], x, t, powerUsed, bgLoadsKWh[t]);
        }

        createObjectivePower(objPower, aggregatedPowerConsumption, neighborPower);
        createObjectivePrice(objPrice, aggregatedPowerConsumption, powerPriceKWh);

        int maxDom = (ALPHA_POWER * objPower.max() + ALPHA_PRICE * objPrice.max());

        costFunction = new IntVar(store, "costFunction", 0, maxDom);
        Constraint ctr = new LinearInt(store, new IntVar[]{objPrice, objPower, costFunction }, new int[]{ALPHA_POWER, ALPHA_PRICE, -1}, "==", 0);
        store.impose(ctr);

        for (int aID = 0; aID < NUM_ACTUATORS; aID++) {
            System.arraycopy(x[aID], 0, decisionVariables, aID * HORIZON, HORIZON);
        }

    }

    private Schedule constructSchedule() {
        Schedule schedule = new Schedule();

        int nbActuators = actuatorsMap.size();
        // Unroll decision variables into array vars:
        for (int aID = 0; aID < nbActuators; aID++) {
            Actuator actuator = actuatorsMap.get(aID);
            ScheduleEntry scheduleEntry = new ScheduleEntry(actuator);

            //t is an index for timesteps, aVal is the action of the actuator at timestep t
            for (int t = 0; t < HORIZON; t++) {
                int aVal = x[aID][t].value();
                scheduleEntry.setScheduledAction(t, actuator.getActionList().getActions().get(aVal + 1));
            }
            // store schedule for device aID
            schedule.addScheduleEntry(scheduleEntry);
        }

        // Store power result in the rulesScheduler.
        for (int t = 0; t < HORIZON; t++) {
            double power = aggregatedPowerConsumption[t].value() / KILO_WATT_TO_WATT;
            schedule.setPowerConsumptionKw(t, power);
        }
        // Set utility of this schedule
        schedule.setUtility((double)costFunction.value()/scaleFactor);

        return schedule;
    }

    private void createPredictiveModelConstraints(PredictiveModel model, IntVar[] var_PredModel_m, int[][] delta_m, IntVar[][] x) {

        if(debugLevel > 1) //Bill 6_5_2017
            System.out.println("Initial State of " + model.getSensor().getName() + ": " + model.getSensor().getState());

        int cpInitialState = scaleAndRoundDelta(model.getSensor().getState());

        Constraint ctr0 = new XeqC(var_PredModel_m[0], cpInitialState);
        store.impose(ctr0);

        for (int t = 1; t < HORIZON; t++) {
            ArrayList<IntVar> auxVars = new ArrayList<>();

            //sum constraints
            for ( Actuator actuator : model.getActuators()) {
                int aID = actuator.getCpID();
                String name = "aux_delta_"+actuator.getName()+"(t="+t+")";
                IntVar vAux = new IntVar(store, name, StdUtils.getMin(delta_m[aID]), StdUtils.getMax(delta_m[aID]));

                // c = delta[aID][ x[aID][t] ];
                // todo: check if t or t-1 -> when action propagates
                int xDom = actuator.getActionList().getActions().size();
                int[] delta = Arrays.copyOf(delta_m[aID], xDom);

                Constraint ctr = new Element(x[aID][t], delta, vAux, -1);
                store.impose(ctr);    // index, array[index],

                auxVars.add(vAux);
            }
            auxVars.add(var_PredModel_m[t-1]);// assign previous element to aux_vars[T]

            // Sum variables to create: var_predMoldel_m[t+1] = var_predMoldel_m[t] + \Sum_a delta_a[action_a]
            Constraint ctr = new SumInt(store, auxVars, "==", var_PredModel_m[t]);
            store.impose( ctr );   // sum
        }
    }

    private void createRuleConstraints(SchedulingRule rule, IntVar[] var_predModel_r) {
        ArrayList<PrimitiveConstraint> primitiveConstraints = new ArrayList<>();

        for (int t = rule.getStartTime(); t <= rule.getEndTime(); t++) {
            int cpGoalState = scaleAndRoundDelta(rule.getGoalState());
            switch (rule.getRuleRelation()) {
                case EQUAL:
                    primitiveConstraints.add(new XeqC(var_predModel_r[t], cpGoalState));
                    break;
                case NOT_EQUAL:
                    primitiveConstraints.add(new XneqC(var_predModel_r[t], cpGoalState));
                    break;
                case LESS_THAN:
                    primitiveConstraints.add(new XltC(var_predModel_r[t], cpGoalState));
                    break;
                case GREATER_THAN:
                    primitiveConstraints.add(new XgtC(var_predModel_r[t], cpGoalState));
                    break;
                case LESS_THAN_EQUAL:
                    primitiveConstraints.add(new XlteqC(var_predModel_r[t], cpGoalState));
                    break;
                case GREATER_THAN_EQUAL:
                    primitiveConstraints.add(new XgteqC(var_predModel_r[t], cpGoalState));
                    break;
                default:
                    System.out.println("ERROR: INVALID RULE CONSTRAINT");
            }
        }
        if (rule.getRuleTimePredicate() == RuleTimePredicate.CONJUNCTION) {
            Constraint ctr = new And(primitiveConstraints);
            //todo add debug prints
            store.impose(ctr);
        }
        else {
            Constraint ctr = new Or(primitiveConstraints);
            store.impose(ctr);
        }

    }

    private void populateDeltaArray(int[][][] delta) {

        if (debugLevel > 0)
            System.out.println("TEST start: delta array");

        int nbProperties = delta.length;

        for (int mID = 0; mID < nbProperties; mID++) {
            PredictiveModel model = modelMap.get(mID);
            SensorProperty modelSensorProperty = model.getSensorProperty();

            if(debugLevel > 0)
                System.out.println("sensor property of the model: " + modelSensorProperty.toString());

            for (Actuator actuator : model.getActuators()) {
                if(debugLevel > 0) System.out.println(actuator.getName());
                int aID = actuator.getCpID();
                int sID = 0;
                for (Action action : actuator.getActionList().getActions().values()) {
                    int cpActionDelta = 0;

                    // If this state has an effect on the sensing property s of the "model"
                    if (action.getAffectedSensorProperties().getSensorProperties().containsValue(modelSensorProperty)) {

                        double actionDelta = action.getActionDelta(modelSensorProperty);
                        cpActionDelta = scaleAndRoundDelta(actionDelta);
                    } else {
                        System.out.println("ERROR: action.getAffectedSensorProperties().getSensorPropertiesMap() did not contain modelSensorProperty");
                    }

                    delta[mID][aID][sID] = cpActionDelta ;

                    if(debugLevel > 0) {
                        String name = "Delta["+mID+"]["+aID+"]["+sID+"](" + action.toString() +")";
                        System.out.println(name + " = " + delta[mID][aID][sID]);
                    }

                    sID++;
                }
            }
        }
        if (debugLevel > 0)
            System.out.println();
    }

    private void populatePowerArray(int[][] power) {

        if (debugLevel > 2)
            System.out.println( "TEST start: power array");

        int nbActuators = power.length;
        for (int aID = 0; aID < nbActuators; aID++) {
            Actuator actuator = actuatorsMap.get(aID);

            int sID = 0;
            for (Action action : actuator.getActionList().getActions().values()) {
                double pwKW = action.getActionKWH();
                power[aID][sID] = scaleAndRoundPower(pwKW);   // transforms KW to Watt

                if(debugLevel > 2)
                    System.out.println("actuator " + actuator.getName() + ": state = "
                            + action.toString() +  " pw: " +  power[aID][sID]);
                sID ++;
            }
        }
        if (debugLevel > 2)
            System.out.println();

    }

    private void createAggrPowerConstraint(IntVar var_aggrPower_t, IntVar[][] x, int t,
                                           int[][] power, double bgLoadsKW) {

        // Contains the variables related to the power consumed in the current time step.
        // Used to sum among all of them
        ArrayList<IntVar> auxVars = new ArrayList<>();
        int nbActuators = power.length;

        for (int aID = 0; aID < nbActuators; aID++) {
            IntVar vAux = new IntVar(store, StdUtils.getMin(power[aID]), StdUtils.getMax(power[aID]));

            int xDom = actuatorsMap.get(aID).getActionList().getActions().size();
            int[] power_a = Arrays.copyOf(power[aID], xDom);

            Constraint ctr = new Element(x[aID][t], power_a, vAux, -1);
            store.impose(ctr);

            auxVars.add(vAux);
        }
        // It adds the background loads
        IntVar vAuxBg = new IntVar(store, scaleAndRoundPower(bgLoadsKW), scaleAndRoundPower(bgLoadsKW));
        auxVars.add(vAuxBg);

        // It sums the total power at time step t
        Constraint ctr = new SumInt(store, auxVars, "==", var_aggrPower_t);
        store.impose(ctr);


    }

    private void createObjectivePower(IntVar objPowerDiff, IntVar[] var_aggrPower, double[] neighborhsKW) {


        if(debugLevel > 2) System.out.println("TEST Start Obj: Power ");

        int maxNeighb = scaleAndRoundPower(StdUtils.getMax(neighborhsKW));

        ArrayList<IntVar> auxVars = new ArrayList<>();
        IntVar[] auxVarsPower = new IntVar[HORIZON];
        for (int t = 0; t < HORIZON; t++) {
            auxVarsPower[t] = new IntVar(store, 0, maxAggregatedPrice + maxNeighb);
            Constraint ctr = new XplusCeqZ(var_aggrPower[t], scaleAndRoundPower(neighborhsKW[t]), auxVarsPower[t]);
            store.impose(ctr);
        }

        for (int t1 = 0; t1 < HORIZON; t1++) {
            for (int t2 = t1+1; t2 < HORIZON; t2++) {
                IntVar vAux = new IntVar(store, 0, maxAggregatedPower + maxNeighb);
                Constraint ctr = new Distance(auxVarsPower[t1], auxVarsPower[t2], vAux);
                store.impose(ctr);
                auxVars.add(vAux);

                if (debugLevel > 2)
                    System.out.println("Diff constraint [" + t1 + ", " + t2 + "]:" + ctr.toString());
            }
        }
        Constraint ctrPower = new SumInt(store, auxVars, "==", objPowerDiff);
        store.impose(ctrPower);


        if (debugLevel > 2)
            System.out.println();
    }

    private  void createObjectivePrice(IntVar objPrice, IntVar[] var_aggrPower, double[] powerPriceKWh) {
        if (debugLevel > 2)
            System.out.println("TEST Start Obj: Price ");
        int[] scaledPrice = new int[HORIZON];
        for (int t=0; t<HORIZON; t++)
            scaledPrice[t] = scaleAndRoundPrice(powerPriceKWh[t]);

        // Sums the current aggregated power per time step (in Watts) * the price (in KwH)
        //todo fix deprecated SumWeight
        Constraint ctrPrice = new SumWeight(var_aggrPower, scaledPrice, objPrice);
        store.impose(ctrPrice);

        if (debugLevel > 2)
            System.out.println("weighted sum: " + ctrPrice.toString());

    }

    private void createIntVarArray(IntVar[] array, Store store, int min , int max) {
        for( int i= 0; i < array.length; i++) {
            array[i] = new IntVar(store, min, max);
        }
    }

    private void createIntVarArrayNamed(IntVar[] array, Store store, String name, int min, int max) {

        for(int i = 0; i < array.length; i++) {
            array[i] = new IntVar(store, name + "_[" + i + "]", min, max);
        }
    }

    private void createIntVar2DArray(IntVar[][] array, Store store, int min, int max) {

        for(int i= 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++){
                array[i][j] = new IntVar(store, min, max);
            }
        }

    }

    private void createIntVar2DArrayNamed(IntVar[][] array, Store store, String name, int min, int max) {
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                array[i][j] = new IntVar(store, name + "_[" + i + "," + j + "]", min, max );
            }
        }
    }

    private boolean searchOptimal() {
        long systemTimeStart, systemTimeEnd;

        systemTimeStart = System.currentTimeMillis();

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, null, new IndomainMin<>());
        search = new DepthFirstSearch<>();
        search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);

        boolean result = search.labeling(store, select, costFunction);
        systemTimeEnd = System.currentTimeMillis();

        if(debugLevel > 2) {
            System.out.println("SearchExecution Time: " + ( systemTimeEnd - systemTimeStart) + " ms");

        }

        return result;
    }

    private boolean searchSatisfaction() {

        long systemTimeStart, systemTimeEnd;

        systemTimeStart = System.currentTimeMillis();

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, new SmallestDomain<>(), new IndomainMin<>());
        search = new DepthFirstSearch<>();
        search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);

        boolean result = search.labeling(store, select);
        systemTimeEnd = System.currentTimeMillis();

        if(debugLevel > 2) {
            System.out.println("SearchExecution Time: " + ( systemTimeEnd - systemTimeStart) + " ms");

        }

        return result;

    }

    private boolean searchSmallestDomain() {

        long systemTimeStart, systemTimeEnd;

        systemTimeStart = System.currentTimeMillis();

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, new SmallestDomain<>(), new IndomainMin<>());
        search = new DepthFirstSearch<>();
        search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);

        boolean result = search.labeling(store, select, costFunction);
        systemTimeEnd = System.currentTimeMillis();

        if(debugLevel > 2) {
            System.out.println("SearchExecution Time: " + ( systemTimeEnd - systemTimeStart) + " ms");

        }

        return result;
    }

    private boolean searchMostConstrainedStatic() {
        long systemTimeStart, systemTimeEnd;

        systemTimeStart = System.currentTimeMillis();

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, new MostConstrainedStatic<>(), new IndomainMin<>());
        search = new DepthFirstSearch<>();
        search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);

        boolean result = search.labeling(store, select, costFunction);
        systemTimeEnd = System.currentTimeMillis();

        if(debugLevel > 2) {
            System.out.println("SearchExecution Time: " + ( systemTimeEnd - systemTimeStart) + " ms");

        }

        return result;
    }

    private boolean searchLDS(int numberDiscrepancy) {
        long systemTimeStart, systemTimeEnd;
        search = new DepthFirstSearch<>();
        search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);
        boolean result = false;

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, new SmallestDomain<>(), new IndomainMiddle<>());
        LDS<IntVar> lds = new LDS<>(numberDiscrepancy);

        if(search.getExitChildListener() == null){
            search.setExitChildListener(lds);
        }
        else {
            search.getExitChildListener().setChildrenListeners(lds);
        }

        systemTimeStart = System.currentTimeMillis();

        result = search.labeling(store, select, costFunction);

        systemTimeEnd = System.currentTimeMillis();

        if(debugLevel > 2) {
            System.out.println("SearchExecution Time: " + ( systemTimeEnd - systemTimeStart) + " ms");

        }

        return result;
    }

    private boolean searchWithRestarts(int nodesOut) {
        boolean result = false;
        boolean timeout = true;

        int nodes = 0;
        int decisions = 0;
        int backtracks = 0;
        int wrongDecisions = 0;

        search = new DepthFirstSearch<>();

        NoGoodsCollector<IntVar> collector = new NoGoodsCollector<>();
        search.setExitChildListener(collector);
        search.setTimeOutListener(collector);
        search.setExitListener(collector);

        SelectChoicePoint<IntVar> select = new SimpleSelect<>(decisionVariables, new SmallestDomain<>(), new IndomainSimpleRandom<>());

        while(timeout) {
            search.setNodesOut(nodesOut);

            result = search.labeling(store, select, costFunction);
            timeout &= collector.timeOut;

            nodes += search.getNodes();
            decisions += search.getDecisions();
            wrongDecisions += search.getWrongDecisions();
            backtracks += search.getBacktracks();

            search = new DepthFirstSearch<>();
            collector = new NoGoodsCollector<>();
            search.setExitChildListener(collector);
            search.setTimeOutListener(collector);
            search.setExitListener(collector);
        }

        return result;
    }

    private boolean BranchAndBound() {
        search = new DepthFirstSearch<>();
        search.setSolutionListener(new CostListener<>());
        store.setLevel(store.level + 1);

        //search.setTimeOut(LoadProjectProperties.getInstance().getSchedulerTimeoutMs() / 1000);
        SelectChoicePoint<IntVar> select =
                new SimpleSelect<>(decisionVariables, new SmallestDomain<>(), new IndomainSimpleRandom<>());

        //search.labeling(store, select, costFunction);
        boolean result = true;
        boolean optimalResult = false;
        while (result) {
            result = search.labeling(store, select);
            store.impose(new XltC(costFunction, bestCost));
            optimalResult = optimalResult || result;
        }
        store.removeLevel(store.level);
        store.setLevel(store.level-1);
        return result;
    }

    private void printPredictiveModels() {
        // Print predictive model:
        for (PredictiveModel model : cpModelId.keySet()) {
            int mID = cpModelId.get(model);
            System.out.print(model.toString() + "\t:");
            for (int t = 0; t < HORIZON; t++) {
                System.out.print(predictiveModel[mID][t].value() / (double)DELTA_SCALE + " ");
            }
            System.out.println();
        }

    }

    private class CostListener<T extends Var> extends  SimpleSolutionListener<T> {
        public boolean executeAfterSolution(Search<T> search, SelectChoicePoint<T> select) {
            boolean returnCode = super.executeAfterSolution(search, select);
            bestCost = costFunction.value();
            return returnCode;
        }
    }

}
