package devices;

/**
 * Created by Jeremiah Smith on 10/21/16.
 */
public class DeviceType {

    private int deviceTypeID;
    private String deviceTypeName;

    public DeviceType(int deviceTypeID, String deviceTypeName) {
        this.deviceTypeID = deviceTypeID;
        this.deviceTypeName = deviceTypeName;
    }

    public int getDeviceTypeID() {
        return deviceTypeID;
    }

    public void setDeviceTypeID(int deviceTypeID) {
        this.deviceTypeID = deviceTypeID;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        this.deviceTypeName = deviceTypeName;
    }

    @Override
    public String toString() {
        return "DeviceType{" +
                "deviceTypeID=" + deviceTypeID +
                ", deviceTypeName='" + deviceTypeName + '\'' +
                '}';
    }
}
