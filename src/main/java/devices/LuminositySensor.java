package devices;

import actions.ActionList;
import actions.Action;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.exceptions.UnirestException;
import communication.CommunicationProtocol;
import locations.Location;
import org.json.JSONArray;
import org.json.JSONObject;
import scala.util.parsing.json.JSON;
import sensorproperties.SensorPropList;
import sensorproperties.SensorProperty;
import com.mashape.unirest.http.*;

import java.io.*;

/**
 * Created by Jeremiah on 1/24/2017.
 * Edited Ryan 2/2017
 *
 */
public class LuminositySensor implements Sensor {

    private int deviceID;
    private int cpID;
    private int currentState;
    private String deviceName;
    private Location location;
    private SensorProperty affectedSensorProperty;
    private CommunicationProtocol communicationProtocol;
    private int stepsOn;
    private HubConnection hubConnection;

    public LuminositySensor(){
        this.hubConnection = new HubConnection();
    }

    public LuminositySensor(int deviceID, int currentState, String deviceName, Location location, SensorProperty affectedSensorProperty, CommunicationProtocol communicationProtocol) {
        this.deviceID = deviceID;
        this.currentState = currentState;
        this.deviceName = deviceName;
        this.location = location;
        this.affectedSensorProperty = affectedSensorProperty;
        this.communicationProtocol = communicationProtocol;
        this.hubConnection = new HubConnection();

    }

    @Override
    public void setState(int state) {
        this.currentState = state;
    }

    @Override
    public SensorProperty getAffectedProperty() {
        return affectedSensorProperty;
    }


    @Override
    public int getState() {
        int state = 0;

        try {
            HttpResponse<JsonNode> response = Unirest.get(this.hubConnection.getUrl() + "/takeLumReading")
                    .header("Content-Type", "application/json")
                    .header("accept", "application/json")
                    .header("Authorization", "Bearer " + this.hubConnection.getToken())
                    .asJson();

            //Print out all the devices returned, a big string of JSON
            System.out.println(response.getBody());

            JSONArray ar = response.getBody().getArray();

            JSONObject ob = (JSONObject) ar.get(0);

            state = ob.getInt("status");

            System.out.println("Status returned for lum sensor is:" + state);



            //Put the devices into a JSON Object
            //Print out the response in a nicer format
            //System.out.println(res.toString(2));
        }catch(UnirestException e){
            e.printStackTrace();
        }

        return state;

    }


    @Override
    public void setID(int id) {
        this.deviceID = id;
    }


    @Override
    public void setCommProtocol(CommunicationProtocol pro) {
        this.communicationProtocol = pro;
    }

    @Override
    public String getName() {
        return deviceName;
    }

    @Override
    public int getID() {
        return deviceID;
    }

    @Override
    public CommunicationProtocol getCommProtocol() {
        return communicationProtocol;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {

    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LuminositySensor)) return false;

        LuminositySensor that = (LuminositySensor) o;

        if (deviceID != that.deviceID) return false;
        if (cpID != that.cpID) return false;
        if (currentState != that.currentState) return false;
        if (deviceName != null ? !deviceName.equals(that.deviceName) : that.deviceName != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (affectedSensorProperty != null ? !affectedSensorProperty.equals(that.affectedSensorProperty) : that.affectedSensorProperty != null)
            return false;
        return communicationProtocol != null ? communicationProtocol.equals(that.communicationProtocol) : that.communicationProtocol == null;
    }

    @Override
    public int hashCode() {
        int result = deviceID;
        result = 31 * result + cpID;
        result = 31 * result + currentState;
        result = 31 * result + (deviceName != null ? deviceName.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (affectedSensorProperty != null ? affectedSensorProperty.hashCode() : 0);
        result = 31 * result + (communicationProtocol != null ? communicationProtocol.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LuminositySensor{" +
                "deviceID=" + deviceID +
                ", cpID=" + cpID +
                ", currentState=" + currentState +
                ", deviceName='" + deviceName + '\'' +
                ", location=" + location +
                ", affectedSensorProperty=" + affectedSensorProperty +
                ", communicationProtocol=" + communicationProtocol +
                '}';
    }

    public void setStepsOn(){
        this.stepsOn++;
    }

    public int getPowerConsumption(){
        int watts = 0;
        //TODO: Add get method for return of power consumption
        //for database when ready
        //Aeotech light uses 0.54 kwH

        return watts;
    }
}
