package devices;

import locations.Location;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 11/11/16.
 */
public class DeviceList {


    private HashMap<Integer, Device> deviceHashMap;

    public DeviceList() {
        deviceHashMap = new HashMap<>();
    }

    public DeviceList(HashMap<Integer, Device> deviceHashMap) {
        this.deviceHashMap = deviceHashMap;
    }

    public HashMap<Integer, Device> getDeviceHashMap() {
        return deviceHashMap;
    }

    public void setDeviceHashMap(HashMap<Integer, Device> deviceHashMap) {
        this.deviceHashMap = deviceHashMap;
    }

    //public methods

    public Device getDevice(int deviceID){
        return deviceHashMap.get(deviceID);
    }

    public ArrayList<Sensor> getSensorsForLocation(Location location) {

        ArrayList<Sensor> sensors = new ArrayList<>();

        for(Device device : deviceHashMap.values()) {
            if(device instanceof Sensor) {
                if(device.getLocation().equals(location)) {
                    sensors.add((Sensor) device);
                }
            }
        }

        return sensors;
    }


    /**
     * Method that will print the devices in the list to the console
     */
    public void printDevices() {

        System.out.println(deviceHashMap);

    }



}
