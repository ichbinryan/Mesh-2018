package devices;

import java.util.LinkedList;
import java.util.HashMap;

import agents.HouseAgent;
import org.joda.time.*;
import utilities.LoadProjectProperties;

import java.util.*;

/**
 * Created by ryanread on 4/3/17.
 */
public class DeviceAverage {

    //class to help calculate device average consumption.
    //implement as linked list rather than array?

    private int resolution; //amount of time steps in our tracking array.
    private HashMap<Integer, Float> tracker;
    private DateTime clock;
    //private boolean on;
    //private TimerTask timerTask;
    private Timer timer;
    //private HouseAgent myAgent

    public DeviceAverage() {
        this.resolution = LoadProjectProperties.getInstance().getTimeSteps();
        this.tracker = new HashMap(this.resolution);
        this.clock = new DateTime();
        this.timer = new Timer();

        for(Integer i : tracker.keySet()) {
            tracker.put(i, 0.0f);
        }
    }

    public DeviceAverage(int res) {
        this.resolution = res;
        this.tracker = new HashMap(res);
        this.clock = new DateTime();
        this.timer = new Timer();
    }

    public int getResolution(){
        return this.resolution;
    }

    public float getUsage(int hour) {
        //
        // return this.tracking.get(hour);
        //TODO: THIS SUCKS
        try {
            return this.tracker.get(hour);
        }catch(NullPointerException e){
            return 0;
        }
    }


    public void setUsage(int hour, float usage) {
        float used = (float) this.tracker.get(hour) + usage;
        this.tracker.put(hour%this.resolution, used);
    }

    /**
     * This is where we will be tracking power usage.
     * @param wh_usage
     */

    public void startTracking(float wh_usage) {
        this.timer = new Timer();
        this.clock = new DateTime();
        int res = this.resolution;
        if(!LoadProjectProperties.getInstance().isSimulation()) {
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    //elapsed time
                    float in1 = wh_usage / 4;
                    float in2 = 0;

                    if (tracker.get(clock.getHourOfDay()) != null) {
                        //System.out.println("tracker value: " + tracker.get(clock.getHourOfDay()));
                        int key = clock.getHourOfDay();
                        in2 = (float) tracker.get(key);
                    }

                    in1 = in1 + in2; //sum new value with value already inserted

                    tracker.put(clock.getHourOfDay(), in1);
                    //System.out.println("Hour, " + clock.getHourOfDay() + " usage, " + in1);
                    //System.out.println("Hash Map updated");
                }

            }, 0, 1000 * 15 * 60); //every fifteen minutes
        }else if(LoadProjectProperties.getInstance().isSimulation()){
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    //elapsed time
                    float in1 = wh_usage;
                    float in2 = 0;
                    int time_step = TimeStep.getOurInstance().getTimeStep();

                   //if (tracker.get(time_step) != null) {
                        //System.out.println("timmy tracker value: " + tracker.get(timmy));
                        //int key = timmy;
                        //in2 = (float) tracker.get(time_step);
                    //}

                    in1 = in1 + in2;

                    //System.out.println("Time Step:" + time_step);

                    tracker.put(time_step, in1);

                    //System.out.println("Hour, " + timmy + " usage, " + in1);
                    //System.out.println("Hash Map updated");
                }

            }, 0, 1000*10);
        }
    }

    public void endTracking(){
        timer.cancel();
        //timer = new Timer(); // need to reinstantiate this timer

        //System.out.println("we have ended tracking");

        //for (int key = 0; key<this.resolution; key++) {
        //    System.out.println(key + ", " + tracker.get(key));
        //}

    }

    public HashMap getTracker(){
        return this.tracker;
    }

    public void setTracker(int key, float kwh){
        this.tracker.put(key, kwh);
    }

}