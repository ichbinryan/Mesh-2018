package devices;

import agents.HouseAgent;
import org.json.JSONArray;
import org.json.JSONObject;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ryanread on 5/17/17.
 * class to handle math for the graphs
 */
public class PowerHandler {
    private float totalKWHUsage;
    private float totalWUsage;
    private static HouseAgent myAgent;
    private static DeviceList deviceList;

    private static PowerHandler ourInstance = new PowerHandler();

    private PowerHandler(){
        totalKWHUsage = 0;
        totalWUsage = 0;

    }

    public static PowerHandler getInstance(){
        return ourInstance;
    }

    public static void setMyAgent(HouseAgent myAgent) {
        ourInstance.myAgent = myAgent;
        ourInstance.deviceList = myAgent.getDeviceList();
    }

    public static void setDeviceList(DeviceList deviceList) {
        ourInstance.deviceList = deviceList;
    }


    public float getKWHSum(Actuator d){
        int size = d.getDeviceAverage().getResolution();
        HashMap tracker = d.getDeviceAverage().getTracker();
        float sum = 0;
        for(int i = 0; i<size; i++){
            if(tracker.get(i) != null){
                sum += (float) tracker.get(i);
            }
        }
        return sum;
    }

    public float getKWHSum(DeviceList d){
        float sum = 0;
        for (Device device : d.getDeviceHashMap().values()){
            //TODO: need to update to make generic to all actuators
            if(device instanceof BinarySwitch)
                sum += getKWHSum((Actuator) device);
        }
        return sum;
    }

    public float getKWHSum(ArrayList<BinarySwitch> d){
        float sum = 0;
        for(int i = 0; i <d.size(); i++) {
            //TODO: This still sucks, fix it.
            if (d.get(i) instanceof BinarySwitch) {
                sum+=getKWHSum(d.get(i));
            }
        }
        System.out.println("*-*-* Updated sum: " + sum);
        return sum;
    }

    public float[] getKWHGraphHourly(){
        float[] hourly = new float[LoadProjectProperties.getInstance().getTimeSteps()];

        for(int i = 0; i < hourly.length; i++){
            for (Device device : this.deviceList.getDeviceHashMap().values()){
                if(device instanceof Actuator) {
                    hourly[i] += ((Actuator) device).getDeviceAverage().getUsage(i);

                }

            }
        }
        /*
        for(int i = 0; i < hourly.length; i++){
            System.out.print("IND: " + i + " VAL: "+hourly[i]);
        }
        System.out.println();*/

        return hourly;
    }

    public float[] getKWHGraphHourly(DeviceList d){
        float[] hourly = new float[12];
        for(int i = 0; i < 12; i++){
            for (Device device : d.getDeviceHashMap().values()){
                if(device instanceof Actuator) {
                    hourly[i] += ((Actuator) device).getDeviceAverage().getUsage(i);
                }
            }
        }
        return hourly;
    }

    public float getWSum(Actuator d){
        return totalWUsage;
    }

    public float updateWSum(float usage){
        totalWUsage += usage;
        System.out.println("Updated watt usage: " + totalWUsage);
        return totalWUsage;
    }

    public JSONArray constructUsage(){
        JSONArray array = new JSONArray();
        for(Device d : ourInstance.deviceList.getDeviceHashMap().values()){
            if(d instanceof Actuator){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Name", d.getName());
                jsonObject.put("KWHUsage", this.getKWHSum((Actuator) d));
                array.put(jsonObject);
            }
        }

        System.out.println(array.toString(2));
        return array;
    }

    /**
     * Get percentage usage total
     * TODO:Finish this up.
     */

    public HashMap percentageUsed(){
        HashMap perc_used = new HashMap();
        for(Device d : ourInstance.deviceList.getDeviceHashMap().values()){
            if(d instanceof Actuator){

            }
        }

        return perc_used;
    }

    /**
     * Get percentage used by hour
     */
    public HashMap percentageUsed(int hourOfDay){
        if((LoadProjectProperties.getInstance().getTimeSteps() < hourOfDay)){
            return null;
        }
        float sum = 0;
        int numAct = 0;
        for(Device d : ourInstance.deviceList.getDeviceHashMap().values()){
            if(d instanceof Actuator){
                sum+=((Actuator) d).getDeviceAverage().getUsage(hourOfDay);
                numAct++;
            }
        }
        HashMap power_perc = new HashMap();
        for(Device d : ourInstance.deviceList.getDeviceHashMap().values()){
            if(d instanceof Actuator){
                power_perc.put(d.getID(), ((Actuator) d).getDeviceAverage().getUsage(hourOfDay)/sum);
            }
        }
        return power_perc;
    }


}
