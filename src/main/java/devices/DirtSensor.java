package devices;

import communication.CommunicationProtocol;
import locations.Location;
import locations.LocationType;
import sensorproperties.SensorProperty;

import java.io.PrintWriter;

/**
 * Created by ryanread on 8/1/17.
 */
public class DirtSensor implements Sensor{

    private int deviceID;
    private int cpID;
    private int currentState;
    private String deviceName;
    private Location location;
    private SensorProperty affectedSensorProperty;
    private CommunicationProtocol communicationProtocol;
    private int stepsOn;
    private HubConnection hubConnection;
    //private int state;


    public DirtSensor() {
        this.location = new Location(4, "Roomba", LocationType.assignLocationType(2));
    }

    public DirtSensor(int deviceID, int currentState, String deviceName, Location location) {
        this.deviceID = deviceID;
        this.currentState = currentState;
        this.deviceName = deviceName;
        this.location = new Location(4, "Roomba", LocationType.assignLocationType(2));
        this.stepsOn = 0;
    }

    public DirtSensor(int deviceID, int cpID, int currentState, String deviceName, Location location, SensorProperty affectedSensorProperty, CommunicationProtocol communicationProtocol, int stepsOn, HubConnection hubConnection) {
        this.deviceID = deviceID;
        this.cpID = cpID;
        this.currentState = currentState;
        this.deviceName = deviceName;
        this.location = new Location(4, "Roomba", LocationType.assignLocationType(2));
        this.affectedSensorProperty = affectedSensorProperty;
        this.communicationProtocol = communicationProtocol;
        this.stepsOn = stepsOn;
        this.hubConnection = hubConnection;
    }


    public DirtSensor(SensorProperty affectedSensorProperty, int initialState, Location location, String deviceName) {

    }


    @Override
    public SensorProperty getAffectedProperty() {
        return this.affectedSensorProperty;
    }

    @Override
    public int getState() {
        return this.currentState;
    }

    @Override
    public void setState(int state) {
        this.currentState = state;
    }

    @Override
    public void setID(int id) {
        this.deviceID = id;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro) {
        this.communicationProtocol = pro;
    }

    @Override
    public String getName() {
        return this.deviceName;
    }

    @Override
    public int getID() {
        return this.deviceID;
    }

    @Override
    public CommunicationProtocol getCommProtocol() {
        return this.communicationProtocol;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {
        pWriter.write("Dirt Sensor\nState: " + this.currentState );
        pWriter.flush();
    }

    @Override
    public Location getLocation() {
        return this.location;
    }

    @Override
    public int getCpID() {
        return this.cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }
}
