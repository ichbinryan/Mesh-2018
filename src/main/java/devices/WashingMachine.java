package devices;

import actions.Action;
import actions.ActionList;
import communication.CommunicationProtocol;
import locations.Location;

import java.io.PrintWriter;

/**
 * Created by ryanread on 7/13/17.
 *
 * THIS IS A SIMULATED CLASS
 *
 * This class will be programmed initially to run continuously.
 *
 * Later, we should program to pause in between cycles in the washing sequence
 */
public class WashingMachine implements Actuator{

    private String name; //name of actuator
    private int deviceID; //device id from db
    private CommunicationProtocol commProtocol; //how to talk to the device? zwave wifi
    private Location currentLocation; //device location
    private ActionList actions; //what the device does.  here that is on and off
    private int cpID;
    private DeviceAverage average; //this poorly named class tracks the wh usage.
    private HubConnection hubConnection; //oauth stuff for communication

    private String cycle_type; //large, medium, small
    private float whUsage; //power usage
    private String state; //rinse, spin, idle, etc


    public WashingMachine(){
        this.name = "Washing Machine";
        this.average = new DeviceAverage();
        //how to implement actions?
        this.state = "idle";
        this.cycle_type = "none";
        this.whUsage = 0;
    }

    public WashingMachine(String name, int deviceID, CommunicationProtocol commProtocol,
                          Location currentLocation, ActionList actions){
        this.name = name;
        this.deviceID = deviceID;
        this.commProtocol = commProtocol; //can hardcode this for simulation?
        this.currentLocation = currentLocation;
        this.actions = actions;

        this.average = new DeviceAverage();
        this.state = "idle";
        this.cycle_type = "none";
        this.whUsage = 0;

        //how to implement hub connection for sim;
    }

    /*
    Overrides for Actuator
     */

    @Override
    public ActionList getActionList(){
        return this.actions;
    }

    @Override
    public boolean activate(int actionID){
        return true;
    }

    @Override
    public boolean activate(Action action){

        return true;
    }

    @Override
    public boolean deActivate(int actionID){
        return true;
    }

    @Override
    public String toString(){
        String ret = "Washing Machine->\nCycle Type: " + this.cycle_type + "\nState: " + this.state +
                "\nWH Usage: " + this.whUsage;
        return ret;
    }

    @Override
    public float getUsage(){
       float current_usage = 0;
       return current_usage;
    }

    @Override
    public DeviceAverage getDeviceAverage(){
        return this.average;
    }

    /*
    Overrides for Device
     */
    @Override
    public void setID(int id){
        this.deviceID = id;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro){
        this.commProtocol = pro;
    }

    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public Location getLocation(){
        return this.currentLocation;
    }

    @Override
    public int getID(){
        return this.deviceID;
    }

    @Override
    public void printDevice(PrintWriter p){
        p.println(this.toString());
    }

    @Override
    public int getCpID(){
        return this.cpID;
    }

    @Override
    public void setCpID(int cpID){
        this.cpID = cpID;
    }

    @Override
    public int hashCode(){
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + deviceID;
        //result = 31 * result + (commID != null ? commID.hashCode() : 0);
        result = 31 * result + (commProtocol != null ? commProtocol.hashCode() : 0);
        result = 31 * result + (currentLocation != null ? currentLocation.hashCode() : 0);
        result = 31 * result + (actions != null ? actions.hashCode() : 0);
        result = 31 * result + cpID;
        return result;
    }

    @Override
    public CommunicationProtocol getCommProtocol(){
        return this.commProtocol;
    }



}
