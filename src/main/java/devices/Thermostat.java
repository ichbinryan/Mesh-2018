package devices;

import actions.Action;
import actions.ActionList;
import communication.CommunicationProtocol;
import locations.Location;
import sensorproperties.SensorProperty;
import com.google.gson.*;

import java.io.PrintWriter;
import java.util.HashMap;

//import java.io.PrintStream;

/**
 * Created by alex on 11/11/16.
 */
public class Thermostat implements Actuator, Sensor {

    private String name;
    private int deviceID;
    private String commID;
    private CommunicationProtocol commProtocol;
    private Location currentLocation;
    private ActionList actions;
    private int cpID;
    private DeviceAverage average;
    private float usage;


    @Override
    public void setState(int state) {

    }

    @Override
    public SensorProperty getAffectedProperty() {
        return null;
    }

    @Override
    public int getState() {
        return 0;
    }

    @Override
    public void setID(int id) {
        deviceID = id;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro) {
        commProtocol = pro;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getID() {
        return deviceID;
    }


    @Override
    public CommunicationProtocol getCommProtocol() {
        return commProtocol;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {
    }

    @Override
    public ActionList getActionList() {
        return actions;
    }

    @Override
    public boolean activate(Action action) {
        return false;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public Location getLocation() {
        return currentLocation;
    }

    @Override
    public boolean activate(int actionID) {
        this.average.startTracking(this.getUsage());
        return false;
    }

    @Override
    public boolean deActivate(int actionID) {
        return false;
    }

    @Override
    public float getUsage(){
        return this.usage;
    } //will the thermostat return the heater/AC usage?

    @Override
    public DeviceAverage getDeviceAverage(){
        return this.average;
    }

}
