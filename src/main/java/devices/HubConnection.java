package devices;

/**
 * Created by ryanread on 5/15/17.
 */
public class HubConnection {

    private String token;
    private String url;

    public HubConnection(){
        this.url = "https://graph-na02-useast1.api.smartthings.com/api/smartapps/installations/c891a564-ea53-4aeb-9e07-ba5d352df014";
        this.token = "ef88fbdf-ed48-405c-a407-952aeed66157";
    }

    public HubConnection(String token, String url){
        this.url = url;
        this.token = token;
    }

    public HubConnection(boolean simDevice){
        this.url = "";
        this.token = "";

        //todo, hub connection sim hub here
    }

    public String getToken(){
        return this.token;
    }

    public String getUrl(){
        return this.url;
    }

}
