package devices;

import agents.HouseAgent;

/**
 * Created by ryanread on 5/25/17.
 */
public class TimeStep {

    private int timeStep;
    private HouseAgent myAgent;

    private static TimeStep ourInstance = new TimeStep();

    private TimeStep(){
        timeStep = 0;
    }

    public void setAgent(HouseAgent h){
        myAgent = h;
    }

    public int getTimeStep(){
        return this.timeStep;
    }

    public static TimeStep getOurInstance(){
        return ourInstance;
    }

    public void setTimeStep(int timeStep){
        this.timeStep = timeStep;
    }
}
