package devices;

import actions.ActionList;
import actions.Action;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import communication.CommunicationProtocol;
import locations.Location;
//import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
//Yay merge

/**
 * Created by Jeremiah Smith on 11/11/16.
 * Updated by ryanread
 */
public class BinarySwitch implements Actuator {

    private String name; //name of actuator
    private int deviceID; //device id from db
    private CommunicationProtocol commProtocol; //how to talk to the device? zwave wifi
    private Location currentLocation; //device location
    private ActionList actions; //what the device does.  here that is on and off
    private int cpID;
    private float usage; //the usage in watts, converted to watt hours while device is on
    private boolean state; //on or off?
    private DeviceAverage average; //this poorly named class tracks the wh usage.
    private HubConnection hubConnection; //oauth stuff for communication

    public BinarySwitch(){
        this.average = new DeviceAverage();
        this.state = false;
        this.activate(this.getActionList().getAction(2));
    }

    /**
     * argd constructor
     * @param name
     * @param deviceID
     * @param commProtocol
     * @param currentLocation
     * @param actions
     */
    public BinarySwitch(String name, int deviceID, CommunicationProtocol commProtocol, Location currentLocation, ActionList actions) {
        this.name = name;
        this.deviceID = deviceID;
        this.commProtocol = commProtocol;
        this.currentLocation = currentLocation;
        this.actions = actions;
        //this.usage = usage;
        this.usage = 10.2f;  //why is this being hardcoded?
        this.hubConnection = new HubConnection();
        this.average = new DeviceAverage();
        this.state = false;

        this.activate(this.getActionList().getAction(2));
    }



    @Override
    public Location getLocation() {
        return currentLocation;
    }


    @Override
    public CommunicationProtocol getCommProtocol(){ return commProtocol;}

    @Override
    public void printDevice(PrintWriter pWriter) {
        pWriter.write("ID" + this.deviceID);
    }

    @Override
    public ActionList getActionList() {
        return actions;
    }

    /**
     * This activates the device (turns it on or off).
     * At activation, we search for the power usage and begin tracking power consumption using device average.
     *
     * @param action what action to activate
     * @return
     */
    @Override
    public boolean activate(Action action) {
        System.out.println("Activating switch " + this.deviceID);

        for(Action da: actions.getActions().values()) {
            if(da.equals(action)) {
                System.out.println("Activating Action " + da.toString());

                String act = da.getActionName().toLowerCase();

                if(act.equals(this.state)){
                    System.out.println("already in this state");
                    return true;
                }
                //System.out.println("***ACTION***: " + act);

                try {
                    HttpResponse<JsonNode> response = Unirest.get(this.hubConnection.getUrl() + "/switches/" + act + this.getID())
                            .header("Content-Type", "application/json")
                            .header("accept", "application/json")
                            .header("Authorization", "Bearer " + this.hubConnection.getToken())
                            .asJson();

                    //Print out all the devices returned, a big string of JSON
                    //System.out.println(response.getBody());

                    //Put the devices into a JSON Object
                    JSONObject res = new JSONObject(response.getBody());

                    //Print out the response in a nicer format
                    //System.out.println(res.toString(2));

                    if (act.equals("on")) {
                        //System.out.println("***BEGINNING TRACKING***");
                        this.state = true;
                        while(this.usage == 0) {
                            this.usage = this.getUsage();
                            //System.out.println("Usage: " + this.usage);
                        }
                        //System.out.println("Device " +this.deviceID + " usage is " + this.usage);
                        this.average.startTracking(this.usage);
                    } else if (act.equals("off")) {
                        //System.out.println("***END TRACKING***");
                        this.state = false;
                        this.average.endTracking();
                        this.usage = 0;
                    }
                } catch(UnirestException e) {
                    e.printStackTrace();
                }
                //comment
                //System.out.println("Hash Map for device" + this.deviceID);
                //System.out.println(this.getDeviceAverage().getTracker());
                return true;
            }
        }

        return false;
    }


    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getID() {
        return deviceID ;
    }


    @Override
    public boolean activate(int actionID) {
        return true;
    }

    @Override
    public boolean deActivate(int actionID) {
        return true;
    }


    @Override
    public void setID(int id){ deviceID = id;}

    @Override
    public void setCommProtocol(CommunicationProtocol pro){ commProtocol = pro; }

    @Override
    public String toString() {
        return "BinarySwitch{" +
                "name='" + name + '\'' +
                ", deviceID=" + deviceID +
                //", commID='" + commID + '\'' +
                ", commProtocol=" + commProtocol +
                ", currentLocation=" + currentLocation +
                ", actions=" + actions +
                ", usage=" + usage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinarySwitch)) return false;

        BinarySwitch that = (BinarySwitch) o;

        if (deviceID != that.deviceID) return false;
        if (cpID != that.cpID) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        //if (commID != null ? !commID.equals(that.commID) : that.commID != null) return false;
        if (commProtocol != null ? !commProtocol.equals(that.commProtocol) : that.commProtocol != null) return false;
        if (currentLocation != null ? !currentLocation.equals(that.currentLocation) : that.currentLocation != null)
            return false;
        return actions != null ? actions.equals(that.actions) : that.actions == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + deviceID;
        //result = 31 * result + (commID != null ? commID.hashCode() : 0);
        result = 31 * result + (commProtocol != null ? commProtocol.hashCode() : 0);
        result = 31 * result + (currentLocation != null ? currentLocation.hashCode() : 0);
        result = 31 * result + (actions != null ? actions.hashCode() : 0);
        result = 31 * result + cpID;
        return result;
    }

    /*@Override
    public float getUsage(){
        return this.usage;
    }*/

    public boolean getState(int id){ //TODO remove ID
        boolean state = false;

        float use = getUsage();
        if(use!=0) return true;
        return false;
    }

    /**
     * queries smart things for the wattage being used.  This is used for WH tracking later.
     * @return watt usage
     */
    @Override
    public float getUsage(){
        //int watts = 0;
        //TODO: Why is this not in unirest? Split into separate method
        //for database when ready
        //Aeotech light uses 0.54 kwH
        try {

            HttpResponse<JsonNode> response = Unirest.get(this.hubConnection.getUrl() + "/switches/power" + this.deviceID)
                    .header("Content-Type", "application/json")
                    .header("accept", "application/json")
                    .header("Authorization", "Bearer " + this.hubConnection.getToken())
                    .asJson();

            JSONArray ar = response.getBody().getArray();
            JSONObject obj = (JSONObject) ar.get(0);
            this.usage = (float) obj.getDouble("power");

        }
        catch(UnirestException e){
            e.printStackTrace();
        }
        catch(JSONException e){
            System.out.println("Check smart things logs for errors");
        }

        return this.usage;
    }

    @Override
    public DeviceAverage getDeviceAverage(){
        return this.average;
    }

    /**
     * consider changing this so that it gets the state directly from the device
     * @return
     */

    public boolean getState(){
        return this.state;
    }


}
