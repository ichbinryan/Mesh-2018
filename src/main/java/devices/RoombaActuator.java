package devices;
import actions.ActionList;
import actions.Action;
import communication.CommunicationProtocol;
import locations.Location;
import utilities.LoadProjectProperties;

import java.io.*;
import java.net.*;

/**
 * Created by rread on 10/14/16.
 */

public class RoombaActuator implements Actuator {

    //Todo:running wh usage is 7.62 wh calculated  use this until updates can be made.
    //Todo: incorporate dirt sensor in both python and java

    private Socket socket;
    //private Socket data;
    private PrintWriter out;
    private int cpID;
    private BufferedReader in;
    private int roomba_id;
    private String comm_id;
    private CommunicationProtocol comm_proto;
    private String name;
    private float usage;
    private DeviceAverage average;
    private ActionList actions;
    private Location location;
    private DirtSensor dirtSensor;
    private String state;

    public RoombaActuator(){
        this.comm_id="192.168.1.123"; //ip for the pi brain
        this.comm_proto = new CommunicationProtocol(3, "wifi","192.168.1.123");
        this.name = "Roomba";
        this.average = new DeviceAverage();
        this.usage = 0.0f;
        this.actions = actions;
        this.location = location;
        this.dirtSensor = new DirtSensor();
        this.state = "NONE";
    }

    public RoombaActuator(int deviceid, Location location, ActionList actions){
        this.roomba_id=deviceid;//Necessary?
        this.comm_id="192.168.1.123"; //ip for the pi brain
        this.comm_proto = new CommunicationProtocol(3, "wifi","192.168.1.123");
        this.name = "Roomba";
        this.average = new DeviceAverage();
        this.usage = 0.0f;
        this.actions = actions;
        this.location = location;
        this.dirtSensor = new DirtSensor();
        this.state = "NONE";


        if(LoadProjectProperties.getInstance().connectSockets()) {
            int x = this.connect(3141);
            if (x == 1) {
                System.out.println("Roomba initialized");
            } else {
                System.out.println("Issues connecting roomba.");
            }
        }
    }

    /**
     * Connect to the pi brain
     * @param port port to connect on Roomba
     * @return
     */
    public int connect(int port){
        try {
            //ServerSocket sock = new ServerSocket(port);

            this.socket = new Socket("192.168.1.123", 3141);


            this.out = new PrintWriter(this.socket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            System.out.println("Socket Connected");

        } catch (IOException e) {
            return 0;
        }
        return 1;
    }

    @Override
    public boolean activate(Action a){
        int id = a.getActionID();
        //this.average.startTracking(jjj);
        return this.activate(id);
    }

    @Override
    public ActionList getActionList() {
        return actions;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public CommunicationProtocol getCommProtocol() {
        return this.comm_proto;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {
        pWriter.println("Roomba" + this.name + "\nState: " + this.getState());
    }


    @Override
    public void setCommProtocol(CommunicationProtocol pro) {
        this.comm_proto = pro;
    }


    @Override
    public void setID(int id){
        this.roomba_id = id;
    }

    @Override
    public int getID(){
        return this.roomba_id;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public boolean activate(int actionID){
        //need to incorporate different modes
        //clean
        this.usage = 0.0f;
        try {
            if (actionID == 3) {
                //clean mode
                this.out.write("clean");
                this.out.flush();
                this.in.readLine();
                this.state = "CLEAN";

            }
            //max
            else if (actionID == 4) {
                //max mode
                this.out.write("max");
                this.out.flush();
                this.in.readLine();
                this.average.startTracking(0);
                //this.average.startTracking(7.6f);  //todo: report usage with battery or when charging?
                this.state = "CLEAN";
            }
            //seek_dock
            else if (actionID == 5) {
                this.out.write("seek_dock");
                this.out.flush();
                this.in.readLine(); //block until roomba reaches dock
                this.average.startTracking(0);
                this.state = "NONE";
            }
            else if (actionID == 111) { //this is a sensor prop
                this.out.write("get_charge");
                this.out.flush();
                String x = this.in.readLine();
                int charge = Integer.parseInt(x);
                System.out.println("charge is " + charge);
                this.state = "CHARGE";


                //charge in charge
            }
            else if (actionID == 6){ //charge
                //charge or idle error?
                this.out.write("charge");
                this.out.flush();
                this.in.readLine(); //block until roomba signals
                this.average.startTracking(25);
                this.state = "CHARGE";

                //it has reached the dock
            }
            else{
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deActivate(int actionID) {
        this.activate(5);
        return true; //need to update to ensure finished
    }


    @Override
    public String getName(){
        return "roomba";
    }

    //prompts roomba for state and returns current sta
    public String getState() {
        String state;
        this.out.write("state");
        this.out.flush();

        //read in a STRING
        try {
            //reads in the state
            state = this.in.readLine();
            return state;
        } catch (IOException e) {
            System.out.println("Error reading from Roomba.");
            return "ERROR";
        }
    }

    @Override
    public float getUsage(){
        return this.usage;
    }

    @Override
    public DeviceAverage getDeviceAverage(){
        return this.average;
    }

    /**
     * request usage currently from Roomba
     * @return
     */

    public float reqUsage() { //currently returns amp hours, need to convert
        //TODO
        float usage = 0.0f;
        return usage;
    }


 //For testing
    public static void main(String[] args){

        String s;

        RoombaActuator robo = new RoombaActuator();

        robo.activate(1);//activate clean
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //s = robo.getState();
        //System.out.println(s); //returned state should be clean


        robo.activate(2);// activate max
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //s = robo.getState();
        //System.out.println(s);//printed state should be max

        robo.activate(3);//delay charge
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //s = robo.getState();
        //System.out.println(s);//printed state should be delay charge

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
