package rules;

import models.PredictiveModel;
import sensorproperties.SensorProperty;
import locations.Location;

import java.util.HashMap;

/**
 * Created by Jeremiah Smith on 12/2/16.
 */
public class SchedulingRule {

    private int ruleID; //number that ref's that rule (primary key in database)
    private SensorProperty affectedProperty; //what state property is affected?
    private Location location; //where is it?
    private RuleRelation ruleRelation; //greater than less than...
    private RuleTimePrefix ruleTimePrefix; //when?
    private RuleType ruleType; //passive or active
    private int goalState;
    private int startTime;
    private int endTime;
    private RuleTimePredicate ruleTimePredicate;
    private PredictiveModel predictiveModel;

    public SchedulingRule() {

    }

    public SchedulingRule(int ruleID, SensorProperty affectedProperty, Location location, RuleRelation ruleRelation, RuleTimePrefix ruleTimePrefix, RuleType ruleType, int goalState, int startTime, int endTime, RuleTimePredicate ruleTimePredicate, PredictiveModel predictiveModel) {
        this.ruleID = ruleID;
        this.affectedProperty = affectedProperty;
        this.location = location;
        this.ruleRelation = ruleRelation;
        this.ruleTimePrefix = ruleTimePrefix;
        this.ruleType = ruleType;
        this.goalState = goalState;
        this.startTime = startTime;
        this.endTime = endTime;
        this.ruleTimePredicate = ruleTimePredicate;
        this.predictiveModel = predictiveModel;
    }

    public int getRuleID() {
        return ruleID;
    }

    public void setRuleID(int ruleID) {
        this.ruleID = ruleID;
    }

    public SensorProperty getAffectedProperty() {
        return affectedProperty;
    }

    public void setAffectedProperty(SensorProperty affectedProperty) {
        this.affectedProperty = affectedProperty;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public RuleRelation getRuleRelation() {
        return ruleRelation;
    }

    public void setRuleRelation(RuleRelation ruleRelation) {
        this.ruleRelation = ruleRelation;
    }

    public RuleTimePrefix getRuleTimePrefix() {
        return ruleTimePrefix;
    }

    public void setRuleTimePrefix(RuleTimePrefix ruleTimePrefix) {
        this.ruleTimePrefix = ruleTimePrefix;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public int getGoalState() {
        return goalState;
    }

    public void setGoalState(int goalState) {
        this.goalState = goalState;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public RuleTimePredicate getRuleTimePredicate() {
        return ruleTimePredicate;
    }

    public void setRuleTimePredicate(RuleTimePredicate ruleTimePredicate) {
        this.ruleTimePredicate = ruleTimePredicate;
    }

    public PredictiveModel getPredictiveModel() {
        return predictiveModel;
    }

    public void setPredictiveModel(PredictiveModel predictiveModel) {
        this.predictiveModel = predictiveModel;
    }

    @Override
    public String toString() {
        return  "ruleID=" + ruleID + " " +
                ruleType + " " + location.getLocationName() + " " + affectedProperty.getSensorPropertyName() + " " +
                ruleRelation + " " + goalState + " " + ruleTimePrefix + " " + startTime + " " + endTime;
    }

    public HashMap<String, String> createRuleHM() {
        HashMap<String, String> ruleHM = new HashMap<>();

        ruleHM.put("Rule_Type", Integer.toString(this.ruleType.getValue()));
        ruleHM.put("Location_ID", Integer.toString(this.location.getLocationID()));
        ruleHM.put("Sensor_Prop_ID", Integer.toString(this.affectedProperty.getSensorPropertyID()));
        ruleHM.put("Rule_Relation", Integer.toString(this.ruleRelation.getValue()));
        ruleHM.put("Time_Prefix", Integer.toString(this.ruleTimePrefix.getValue()));
        ruleHM.put("Goal_State", Integer.toString(this.goalState));
        ruleHM.put("Start_Time", Integer.toString(this.startTime));
        ruleHM.put("End_Time", Integer.toString(this.endTime));

        return ruleHM;
    }
}
