package meshfx;

import agents.HouseAgent;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

//import java.event.ActionEvent;


public class Main extends Application
{
    public static int counter;


    @Override
    public void start(Stage primaryStage) throws Exception
    {
        counter = 0;
        primaryStage.setTitle("Home");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/home2.fxml"));

        //Parent[] root = {FXMLLoader.load(getClass().getResource("homepage.fxml")),
        //FXMLLoader.load(getClass().getResource("screensaver.fxml"))};
        Scene scene = new Scene(root);

        //Fade into home screen
        FadeTransition ft = new FadeTransition(Duration.millis(3000), root);
        ft.setFromValue(.1);
        ft.setToValue(1.0);
        ft.play();

        primaryStage.setMaximized(false);
        primaryStage.setFullScreen(false);
        primaryStage.setResizable(false);

        primaryStage.setScene(scene);
        primaryStage.show();


/*
        scene.setOnMousePressed(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                if (event.getButton() == MouseButton.PRIMARY)
                {
                    counter++;
                }
            }
        });

        final Timeline loop = new Timeline(new KeyFrame(Duration.millis(5), new EventHandler<javafx.event.ActionEvent>()
        {
            @Override
            public void handle(javafx.event.ActionEvent event)
            {

            }
        }));

*/
/*

        //checks to see if there was any mouse click, this helps the timer
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                System.out.println("mouse click detected! " + mouseEvent.getSource());
                counter++;
                System.out.println("\nCounter = " + counter);

                //timer for screensaver
        Timer timer = new Timer();
        timer.schedule(new TimerTask()
        {
            public void run()
            {
                Platform.runLater(new Runnable()
                {
                    public void run()
                    {
                        //if(counter == 0)
                        //{
                        Scene scene2 = new Scene(root[1]);

                        FadeTransition ft = new FadeTransition(Duration.millis(3000), root[1]);
                        ft.setFromValue(.1);
                        ft.setToValue(1.0);
                        ft.play();

                        primaryStage.setScene(scene2);
                        primaryStage.show();
                        //}

                    }
                });
            }
        }, 5000);
    }
});*/
    }//end start

    public static void main(String[] args)
    {
        launch(args);
    }
}//end Main
