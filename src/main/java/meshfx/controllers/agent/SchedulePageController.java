package meshfx.controllers.agent;
import behaviours.schedule.MGM;
import behaviours.testing.DaySimulation;
import behaviours.testing.GenerateSchedule;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import meshinterface.model.MeshModel;
import utilities.LoadProjectProperties;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Friday on 6/23/2017.
 */
public class SchedulePageController implements Initializable {
    @FXML
    private JFXButton rule_b,schedule_b;

    @FXML
    private Button mgmButton;

    @FXML
    private Button ruleManager;

    @FXML
    private Button generateSchedule;

    @FXML
    private Button startSimulationButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mgmButton.setOnMouseClicked(event -> startMGM());
        ruleManager.setOnMouseClicked(event -> loadRuleManager());
        generateSchedule.setOnMouseClicked(event -> generateSchedule());
        startSimulationButton.setOnMouseClicked(event -> startSimulation());

    }


    private void startMGM() {

        MeshModel.getInstance().getMyAgent().addBehaviour(new MGM(MeshModel.getInstance().getMyAgent(), LoadProjectProperties.getInstance().getMgmCycles()));

    }

    private void loadRuleManager() {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/rules/ruleManager.fxml"));
            Stage stage = (Stage) ruleManager.getScene().getWindow();
            stage.setTitle("Rules");
            stage.setScene(new Scene(root));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startSimulation() {
        MeshModel.getInstance().getMyAgent().addBehaviour(new DaySimulation(MeshModel.getInstance().getMyAgent(), LoadProjectProperties.getInstance().getSimulationTime()));
    }

    private void generateSchedule() {
        MeshModel.getInstance().getMyAgent().addBehaviour(new GenerateSchedule(MeshModel.getInstance().getMyAgent()));
    }

}
