package meshfx.controllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable
{
    @FXML private Button deviceButton;
    @FXML private ImageView title;
    @FXML private Pane homeAnchor;
    //@FXML private AnchorPane homeAnchor;
    //@FXML private AnchorPane screenSaver;
    //@FXML private ImageView deviceImage, energyImage, agentImage, roomsImage;


    @FXML
    private void handleDeviceButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) deviceButton.getScene().getWindow();
        stage.setTitle("Devices");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/NewDeviceView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
/*
    @FXML
    private void handleRoomsButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) roomsButton.getScene().getWindow();
        stage.setTitle("Rooms");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/roomspage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleEnergyButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) energyButton.getScene().getWindow();
        stage.setTitle("Device Information");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/addDevice.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleSettingsButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) settingsButton.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/settings.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
*/
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
/*
        //Motioning the buttons for effect on the home page
        TranslateTransition devTT = new TranslateTransition(Duration.seconds(2), deviceButton);
        TranslateTransition ageTT = new TranslateTransition(Duration.seconds(2), agentButton);
        TranslateTransition rooTT = new TranslateTransition(Duration.seconds(2), roomsButton);
        TranslateTransition eneTT = new TranslateTransition(Duration.seconds(2), energyButton);
        devTT.setByX(-36);
        ageTT.setByX(-13);
        rooTT.setByX(13);
        eneTT.setByX(36);
        devTT.play();
        ageTT.play();
        rooTT.play();
        eneTT.play();
        */
    }//end initialize
}//end HomeController