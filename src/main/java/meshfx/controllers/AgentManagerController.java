package meshfx.controllers;

import behaviours.schedule.MGM;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import meshinterface.model.MeshModel;
import utilities.LoadProjectProperties;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 6/12/17.
 */
public class AgentManagerController implements Initializable {



    @FXML
    private TextField agentName;
    @FXML
    private TextField deviceCount;

    @FXML
    private Button startMGM;

    @FXML
    private TableView schedule;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        agentName.setText(MeshModel.getInstance().getMyAgent().getName());
        deviceCount.setText(Integer.toString(MeshModel.getInstance().getMyAgent().getDeviceList().getDeviceHashMap().size()));
        for(int i = 0; i < LoadProjectProperties.getInstance().getTimeSteps(); i++) {
            TableColumn tableColumn = new TableColumn();
            tableColumn.setText("TS: " + (i + 1));
            schedule.getColumns().add(tableColumn);
        }
        startMGM.setOnAction(event -> MeshModel.getInstance().getMyAgent().addBehaviour(new MGM(MeshModel.getInstance().getMyAgent(), LoadProjectProperties.getInstance().getMgmCycles())));

    }

}
