package meshfx.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jordan Michael on 8/30/2016.
 */
public class ScreensaverController implements Initializable
{
    @FXML private AnchorPane screenSaver;

    @FXML
    private void handleMouseEvent(MouseEvent mouseEvent2) throws Exception
    {
        System.out.println("mouse click detected! " + mouseEvent2.getSource());



            //Scene scene2 = new Scene(root[0]);
            //primaryStage.setScene(scene2);
            //primaryStage.show();

            Stage stage = (Stage) screenSaver.getScene().getWindow();
            //stage.setTitle("Rooms");
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/homepage.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
           // Main.counter = 0;*/


    }
    //});

    //Try and have live data on line graph

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

    }
}
