package meshfx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jeremiah Smith on 12/9/16.
 */
public class AddClassController implements Initializable {



    @FXML
    private Button addClass;
    @FXML
    private Button back;
    @FXML
    private ListView<String> currentClasses;
    @FXML
    private TextField newClassName;



    @FXML
    private void handleBackButton(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage) back.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/settings.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleAddClassButton(ActionEvent actionEvent) throws Exception {


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }
}
