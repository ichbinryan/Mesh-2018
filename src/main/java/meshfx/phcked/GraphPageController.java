package meshfx.phcked;

import database.model.DeviceTable;
import database.model.HourlyPowerConsumption;
import database.service.ServiceHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Created by bob on 26.05.17
 *
 */
public class GraphPageController implements Initializable {

    @FXML private ScrollPane scroller;
    @FXML private AnchorPane sceneScreen;
    @FXML private LineChart totalUsageLineChart;
    @FXML private NumberAxis totalUsageXAxis;
    @FXML private NumberAxis totalUsageYAxis;
    @FXML private LineChart usagePerDeviceLineChart;
    @FXML private NumberAxis usagePerDeviceXAxis;
    @FXML private NumberAxis usagePerDeviceYAxis;
    @FXML private ComboBox<Integer> deviceComboBox;

    private ObservableList<Integer> deviceList = FXCollections.observableArrayList();
    private XYChart.Series<Integer, Double> totalUsageList = new XYChart.Series<>();
    private List<XYChart.Series<Integer, Double>> usagePerDeviceListList = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        totalUsageLineChart.setLegendVisible(false);
        usagePerDeviceLineChart.setLegendVisible(false);
        totalUsageLineChart.getData().clear();
        usagePerDeviceLineChart.getData().clear();

        totalUsageLineChart.getData().add(totalUsageList);
        deviceComboBox.setItems(deviceList);

        getDeviceIds();
        loadTotalUsageData();
        loadUsagePerDeviceData();
    }

    public void comboAction() {
        usagePerDeviceLineChart.getData().clear();
        usagePerDeviceLineChart.getData().addAll(usagePerDeviceListList.get(deviceComboBox.getValue()));
    }

    private void getDeviceIds() {
        List<Integer> deviceIds = new ArrayList<>();
        ServiceHelper.getInstance().getAllDevices().enqueue(new Callback<List<DeviceTable>>() {

            @Override
            public void onResponse(Call<List<DeviceTable>> call, Response<List<DeviceTable>> response) {
                deviceIds.addAll(response
                        .body()
                        .stream()
                        .map(DeviceTable::getDeviceId)
                        .collect(Collectors.toList()));
                deviceList.setAll(deviceIds);
                deviceComboBox.getSelectionModel().select(0);
                comboAction();
            }

            @Override
            public void onFailure(Call<List<DeviceTable>> call, Throwable t) {
                //Could not call the API
                System.out.println("Error: " + t); //todo actual logger
            }
        });
    }

    private void loadTotalUsageData() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String today = sdf.format(calendar.getTime());

        double[] total = new double[12]; //TODO remove hardcoding? Will it always be 12?
        Arrays.fill(total, 0);

        XYChart.Series<Integer, Double> usageTotals = new XYChart.Series<>();

        ServiceHelper.getInstance().getAllHourlyPowerConsumption(today).enqueue(new Callback<List<HourlyPowerConsumption>>() {
            @Override
            public void onResponse(Call<List<HourlyPowerConsumption>> call, Response<List<HourlyPowerConsumption>> response) {
                for (HourlyPowerConsumption hpc : response.body()) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(hpc.getTimeStamp()));
                    int hour = calendar.get(Calendar.HOUR_OF_DAY)/2;
                    total[hour] += hpc.getWhUsageDouble();
                }

                for (int i = 0; i < 12; i++) {
                    usageTotals.getData().add(new XYChart.Data<>(i, total[i]));
                }

                totalUsageList.getData().setAll(usageTotals.getData());
            }
            @Override
            public void onFailure(Call<List<HourlyPowerConsumption>> call, Throwable t) {
                //Could not call the API
                System.out.println("Error: " + t); //todo actual logger
            }
        });
    }

    private void loadUsagePerDeviceData() {
        //Initialize the list
        for (int i = 0; i <= deviceComboBox.getItems().size()+1; i++) {
            usagePerDeviceListList.add(new XYChart.Series<>());
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String today = sdf.format(calendar.getTime());
        List<XYChart.Series<Integer, Double>> usageTotals = new ArrayList<>();

        ServiceHelper.getInstance().getAllHourlyPowerConsumption(today).enqueue(new Callback<List<HourlyPowerConsumption>>() {
            @Override
            public void onResponse(Call<List<HourlyPowerConsumption>> call, Response<List<HourlyPowerConsumption>> response) {
                for (int i = 0; i < response.body().size(); i++) {
                    usageTotals.add(new XYChart.Series<>());
                }

                for (HourlyPowerConsumption hpc : response.body()) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(hpc.getTimeStamp()));
                    int hour = calendar.get(Calendar.HOUR_OF_DAY)/2;
                    usageTotals.get(hpc.getDeviceId()).getData().add(new XYChart.Data<>(hour, hpc.getWhUsageDouble()));
                }

                for (int i = 0; i < usageTotals.size(); i++) {
                    if (usageTotals.get(i).getData().size() > 0) {
                        usagePerDeviceListList.add(i, usageTotals.get(i));
                    }
                }
            }
            @Override
            public void onFailure(Call<List<HourlyPowerConsumption>> call, Throwable t) {
                //Could not call the API
                System.out.println("Error: " + t); //todo actual logger
            }
        });
    }
}
