package meshfx.phcked;


import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class MenuFrameController implements Initializable {

    @FXML
    private Button menuButton;

    @FXML
    private StackPane mainScreen;

    @FXML
    private AnchorPane menu;

    @FXML
    private GridPane fullscreen;

    @FXML
    private Rectangle fade_screen;
    private TranslateTransition menu_action;
    private FadeTransition fader;
    @FXML
    private ListView<String> menuList;
    private HashMap<String,Node> frames = new HashMap<>();

// TODO: Make a scroll pane that dynamically scales to all nodes on this page.

    public final String GRAPH_RESOURCE     = "/interfaceFXML/newstuff/GraphPage.fxml";
    public final String GRAPH_NAME         = "Graphs";
    public final String DEVICE_RESOURCE    = "/interfaceFXML/newstuff/DevicePage.fxml";
    public final String DEVICE_NAME        = "Devices";
    public final String HOME_NAME          = "Home";
    public final String HOME_RESOURCE      = "/interfaceFXML/newstuff/HomePage.fxml";
    public final String SCHEDULER_NAME     = "Schedules";
    public final String SCHEDULER_RESOURCE = "/fxml/agent/agentManager.fxml";
   // method in which the menu is translated by its preferred width off the screen
   // Upon clicking the menu button, it moves itself onto the screen and upon choosing
   // an option on the menu, or by clicking away, it moves back off the screen.
   @FXML
   private void moveMenu(){
    if(menu.getTranslateX() == 0) {
        menu_action.setToX(-(menu.getPrefWidth()));
        fader.setToValue(0);
    }
    else {
        menu_action.setToX(0);
        fader.setToValue(0.3);
    }
        fader.play();
        menu_action.play();
   }

   public void mapToFrame(String name, String resource){

       FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
       try
       {
           Node node = loader.load();
           frames.put(name, node);
           menuList.getItems().add(name);
       }
       catch(IOException error){
           error.printStackTrace();
       }


   }

   // TODO: Make a cool transition btwn pages
    private void transition(Node p)
    {



    }
    private void setPage(String name)
    {
        Node node = frames.get(name);
        double none = 0.0;
        AnchorPane.setTopAnchor(node, none);
        AnchorPane.setBottomAnchor(node, none);
        AnchorPane.setLeftAnchor(node, none);
        AnchorPane.setRightAnchor(node, none);
        mainScreen.getChildren().setAll(frames.get(name));

    }
    @FXML
    private void setPageFromList()
    {
        if(!menuList.getSelectionModel().isEmpty()) {
            setPage(menuList.getSelectionModel().getSelectedItem());
            moveMenu();
        }
        }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

       // Defines the transitions used for menu and fading screen
       menu_action = new TranslateTransition(Duration.millis(350), menu);
       fader = new FadeTransition(Duration.millis(350),fade_screen);


        // Binds the height and width of the fading screen to the fullscree
        // TODO: Find a way to do this within fxml to separate view
        fade_screen.widthProperty().bind(fullscreen.widthProperty());
        fade_screen.heightProperty().bind(fullscreen.heightProperty());
        // Puts all pages into the menuFramer so that it's stack pane can load whichever page is selected at start.
        mapToFrame(GRAPH_NAME, GRAPH_RESOURCE);
        mapToFrame(DEVICE_NAME,DEVICE_RESOURCE);
        mapToFrame(SCHEDULER_NAME,SCHEDULER_RESOURCE);
        // Sets the initial page to HOME

        setPage(GRAPH_NAME);
        // In case user accidentally clicks the menu button, they can make it disappear anywhere else on the screen
        menu.translateXProperty().addListener((observable, oldValue, newValue) -> {
           if(menu.getTranslateX() == 0){

               fullscreen.setOnMouseClicked(event->moveMenu());
           }
           else
               fullscreen.setOnMouseClicked(null);
       });
    }
}
