package meshfx.phcked;

import actions.Action;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import devices.BinarySwitch;
import devices.Device;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Monday on 6/12/2017.
 */
public class DeviceNode2 extends AnchorPane {
    @FXML
    private JFXTextField dev_name;
    @FXML
    private JFXToggleButton bees;
    @FXML
    private JFXComboBox<String> quickAct;
    private HashMap<String, EventHandler<ActionEvent>> g;
    private BinarySwitch bswitch;

    DeviceNode2(Device device) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceFXML/newstuff/DeviceNode2.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        dev_name.setText(device.getName());
        bswitch = (BinarySwitch) device;
        Action turnOn = bswitch.getActionList().getAction(1);
        Action turnOff = bswitch.getActionList().getAction(2);
        if(bswitch.getState())
            bees.setSelected(true);
        bees.setOnAction( (event) ->
        {
            if(bswitch.getState())
        {
            bswitch.activate(turnOff);
        }
        else
        {
            bswitch.activate(turnOn);
        }});
    }
}
