package meshfx.phcked;

import com.jfoenix.controls.JFXButton;
import devices.Device;
import javafx.animation.ScaleTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Thursday on 6/1/2017
 */
public class DeviceNode extends AnchorPane implements Initializable
{
    @FXML
    private AnchorPane mainNode, cancer;

    @FXML
    private ImageView dev_icon;

    @FXML
    private Label device_name;
    private boolean b = true;
    @FXML
    private GridPane enteredNode;
    @FXML
    private JFXButton quick_b, opt_b;

    private String name;
    private StackPane stackPane;
    private GridPane gridPane;
    private Group rectangle;
    private Device d;
    ScaleTransition transition = new ScaleTransition(Duration.millis(2000), this);
    DeviceNode(Device device, StackPane stackPane, GridPane gp, Group rectangle)
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceFXML/newstuff/DeviceNode.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try
        {
        loader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        device_name.setText(device.getName());
        this.stackPane = stackPane;
        this.gridPane = gp;
        this.d = device;
     //   this.rectangle = rectangle;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mainNode.setOnMouseEntered(event ->
                {
                    enteredNode.setVisible(true);
                }
         );

        mainNode.setOnMouseExited(event -> enteredNode.setVisible(false));
        //device_name.setText(this.name);
        opt_b.setOnAction(event->
        {
            gridPane.setVisible(true);
            stackPane.getChildren().setAll(new DeviceNode2(d));
        }
        );

        device_name.setText(name);

    }
}
