package meshfx.phcked;

import com.jfoenix.controls.JFXButton;
import devices.Actuator;
import devices.Device;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import meshinterface.model.MeshModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Thursday on 6/1/2017
 */
public class DevicePageController implements Initializable
{

    @FXML
    private FlowPane dev_display;
    @FXML
    private FlowPane g_display;
    @FXML
    private JFXButton create_group;
    @FXML
    private JFXButton add_device;
    @FXML
    private StackPane John;
    @FXML
    private ScrollPane scrolla;
    @FXML
    private GridPane griddy,John2;
    @FXML
    private JFXButton q_b;
    private ObservableList<DeviceNode> devices = FXCollections.observableArrayList();
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        Group g = new Group();
        for(Device d : MeshModel.getInstance().getMyAgent().getDeviceList().getDeviceHashMap().values())
        {
            if(d instanceof Actuator)
                devices.add(new DeviceNode(d,John,griddy,g));
        }
        dev_display.getChildren().addAll(devices);
       scrolla.viewportBoundsProperty().addListener(
                (observableValue, oldBounds, newBounds) ->
                {
                    John.setPrefSize(
                            Math.max(John.getPrefWidth(), newBounds.getWidth()),
                            Math.max(John.getPrefHeight(), newBounds.getHeight()));
                });
        q_b.setOnAction(event->
                {
                    griddy.setVisible(false);
                    John.getChildren().clear();
                }
        );


    }
}
