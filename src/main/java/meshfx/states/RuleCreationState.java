package meshfx.states;

/**
 * Created by jeremiah on 6/16/17.
 */
public enum RuleCreationState {

    SENSOR_PROPERTY_SELECTION,
    LOCATION_SELECTION,
    RULE_RELATION_SELECTION,
    RULE_TYPE_SELECTION,
    TIME_PREFIX_SELECTION,
    TIME_SELECTION,
    GOAL_STATE_SELECTION

}
