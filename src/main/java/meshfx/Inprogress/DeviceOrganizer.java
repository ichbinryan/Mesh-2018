package meshfx.Inprogress;
import actions.Action;
import devices.Actuator;
import devices.BinarySwitch;
import devices.Device;
import devices.PowerHandler;
import javafx.application.Platform;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import meshinterface.model.MeshModel;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jorda on 1/4/2017.
 */
public class DeviceOrganizer implements Initializable
{
    @FXML
    private Label deviceName;
    @FXML
    private Button on;
    @FXML
    private Accordion accordion;
    @FXML
    private TitledPane deviceInfo;
    @FXML
    private ImageView deviceImage;
    @FXML
    private Button nextDevice;
    private int current = 0;
    @FXML
    private HBox hBox;
    @FXML
    private Label PowerNum;

    private ArrayList<Device> arrayList = new ArrayList<>();
    @FXML
    private ChoiceBox<String> wombocombo;


    private void populateHBox(Device device){

        deviceName.setText(device.getName());
        deviceImage.setImage(MeshModel.getInstance().getDeviceImage(9));
        if(device instanceof BinarySwitch) {
            BinarySwitch bswitch = (BinarySwitch) device;
            Action turnOn = bswitch.getActionList().getAction(1);
            Action turnOff = bswitch.getActionList().getAction(2);
            Glyph glyph = new Glyph("FontAwesome", FontAwesome.Glyph.POWER_OFF);
            glyph.setColor(Color.WHITE);
            Timer t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        public void run() {
                            PowerNum.setText(Float.toString(PowerHandler.getInstance().getKWHSum(bswitch))+" Watt Hours");
                        }
                });}
            },0,500);



            FloatProperty p = new SimpleFloatProperty(PowerHandler.getInstance().getKWHSum(bswitch));
            Glyph glyph1 = new Glyph("FontAwesome", FontAwesome.Glyph.POWER_OFF);
            glyph1.setColor(Color.GRAY);
            on.setText("");
            if(bswitch.getState())
            {
                on.setGraphic(glyph);
            }
            else {
                on.setGraphic(glyph1);
            }

            on.setOnAction(event -> {
                if(!bswitch.getState()) {
                    bswitch.activate(turnOn);
                    on.setGraphic(glyph);
                }
                else {
                    bswitch.activate(turnOff);
                    on.setGraphic(glyph1);
                }}
            );
        }



        }
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        for(Device d : MeshModel.getInstance().getMyAgent().getDeviceList().getDeviceHashMap().values()){
            if(d instanceof Actuator) {
                arrayList.add(d);
                wombocombo.getItems().add(d.getName());
            }
        }
        wombocombo.setOnAction(
                event-> populateHBox(arrayList.get(wombocombo.getSelectionModel().getSelectedIndex())));
        populateHBox(arrayList.get(current));
        nextDevice.setOnAction(e->populateHBox(arrayList.get(++current % arrayList.size())));
        on.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.POWER_OFF));
    }//End initialize()
}//End DeviceOrganizer class
