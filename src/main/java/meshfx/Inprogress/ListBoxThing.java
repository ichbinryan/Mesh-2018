package meshfx.Inprogress;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by bob on 20.05.17.
 */
public class ListBoxThing extends AnchorPane implements Initializable{
    @FXML
    private Button approveButton,denyButton;
    @FXML
    private Label rule;
    @FXML
    private CheckBox prev;

    ListBoxThing(String rule) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceFXML/ListBoxThing.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
            this.rule.setText(rule);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        approveButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.CHECK));
        denyButton.setGraphic(new Glyph("FontAwesome",FontAwesome.Glyph.UNDO));
        approveButton.setOnAction(e->getChildren().clear());
        denyButton.setOnAction(e->getChildren().clear());
    }
}
