package meshfx.Inprogress;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by Richard on 2/13/2017.
 */
public class MainController implements Initializable {
    @FXML
    private StackPane mobileScreen;
    @FXML
    private Button menuButton;
    @FXML
    private AnchorPane menuBar;
    @FXML
    private ListView menuChoose;
    private HashMap<String, Node> screens = new HashMap<>();
    private TranslateTransition openMenu, closeMenu;


    public void setSelectedScreen() {
        closeMenu();
        String s = (String)menuChoose.getSelectionModel().getSelectedItem();
        setMobileScreen(s);
    }

    public void injectController(String name, String resource){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(resource));
        try {
            screens.put(name,fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    public void setMobileScreen(String name){
        FadeTransition ft = new FadeTransition(Duration.millis(500), mobileScreen);
        ft.setFromValue(0);
        ft.setToValue(1.0);
        ft.play();
        mobileScreen.getChildren().setAll(screens.get(name));
    }



    public void extendMenu() {

        if (menuBar.getTranslateX() != 0) {
            openMenu.play();
        }
    }

    public void closeMenu(){
        if(menuBar.getTranslateX() == 0)
            closeMenu.setToX(-(menuBar.getWidth()));
        closeMenu.play();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> observableList = FXCollections.observableArrayList("Home","Devices","Graphs");
        menuChoose.setItems(observableList);
        closeMenu = new TranslateTransition(new Duration(350), menuBar);
        openMenu  = new TranslateTransition(new Duration(350), menuBar);
        openMenu.setToX(0);
        injectController("Graphs","/interfaceFXML/graph.fxml");
        injectController("Devices","/interfaceFXML/NewDeviceView.fxml");
        setMobileScreen("Graphs");
        menuButton.setText("");
        menuButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.BARS));

    }
}