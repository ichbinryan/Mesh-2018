package meshfx.Inprogress;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created by Richard on 2/13/2017.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("MESH");
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceFXML/newstuff/MenuFrame.fxml"));
        primaryStage.setMaximized(true);
        Scene scene = new Scene(root,800,600);
        //Fade into home screen
        FadeTransition ft = new FadeTransition(Duration.millis(3000), root);
        ft.setFromValue(.1);
        ft.setToValue(1.0);
        ft.play();
        primaryStage.setMaximized(false);
        primaryStage.setFullScreen(false);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String args[]){launch(args);}




}
