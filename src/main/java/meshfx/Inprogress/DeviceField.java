package meshfx.Inprogress;
import actions.Action;
import devices.Actuator;
import devices.BinarySwitch;
import devices.Device;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DeviceField extends VBox implements Initializable {
    @FXML
    private Label deviceName;
    @FXML
    private Button on;
    @FXML
    private Accordion accordion;
    @FXML
    private TitledPane deviceInfo;
    @FXML
    private ImageView deviceImage;
    private Device device;
    private boolean status;
    private Actuator actuator;
    private Action deviceAction;
    private BinarySwitch binarySwitch = new BinarySwitch();


    public DeviceField(String name, Image image, Device device) {
        status = true;
        this.device = device;

        if(device instanceof BinarySwitch)
            binarySwitch = (BinarySwitch) device;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/interfaceFXML/DeviceField.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            deviceName.setText(name);
            deviceImage.setImage(image);
        } catch (IOException e) {


        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       accordion.setExpandedPane(deviceInfo);
       Action off1 = binarySwitch.getActionList().getAction(2);
       Action on1 = binarySwitch.getActionList().getAction(1);

       on.setOnAction(event->{
           if(status)
           {
               on.setText("OFF");
               //actuator.activate();
               status = false;
                binarySwitch.activate(on1);
           }

           else
           {
               on.setText("ON");
               status = true;
               binarySwitch.activate(off1);
           }
       }
       );

    }
}