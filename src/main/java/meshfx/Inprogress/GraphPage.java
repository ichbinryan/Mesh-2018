package meshfx.Inprogress;
import behaviours.testing.DaySimulation;
import behaviours.testing.GenerateSchedule;
import devices.PowerHandler;
import jade.core.behaviours.SequentialBehaviour;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.effect.Glow;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import meshinterface.model.MeshModel;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by Richard on 3/10/2017.
 **/


public class GraphPage implements Initializable{



    @FXML
    private RadioButton dayToggle, monthToggle, yearToggle;
    private ToggleGroup tGroup = new ToggleGroup();

    @FXML
    private SplitPane splitPane;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private StackPane graphScreen;
    @FXML
    private Button scheduleButton;
    //@FXML
    //private VBox box;

    private CategoryAxis xAxis = new CategoryAxis();
    private NumberAxis   yAxis = new NumberAxis();
    private BarChart<String,Number> barChart = new BarChart<>(xAxis,yAxis);
    private PieChart pieChart = new PieChart();
    private Date date = new Date();
    private SimpleDateFormat dateFormat = new SimpleDateFormat();
    private XYChart.Series<String,Number> bar1 = new XYChart.Series<>();

    public void setBarDay(){
//        splitPane.setPrefSize(splitPane.getBoundsInParent().getMaxX(), splitPane.getBoundsInParent().getMaxY());

        float[] hourGraphs = PowerHandler.getInstance().getKWHGraphHourly();

        dateFormat.applyPattern("MMMM d, YYYY");
        xAxis.setLabel("Time (Hours of day)");
        yAxis.setLabel("Watt Hours");
        barChart.setTitle(dateFormat.format(date));

        barChart.setAnimated(false);
        XYChart.Data<String,Number> g;
        bar1.getData().clear();
        barChart.getData().clear();
        barChart.getData().add(bar1);
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        for(int i = 1; i < 13; i++)
        {
            bar1.getData().add(i-1,new XYChart.Data<>(""+(i)+" am", hourGraphs[i-1]));
        }

//kl;

      timeline.getKeyFrames().add(new KeyFrame(Duration.millis(20000),event ->{
          float[] hourGraphs2 = PowerHandler.getInstance().getKWHGraphHourly();
          //System.out.println("Hourly array: " + Arrays.toString(hourGraphs2));
          for(int i = 1; i < 13; i++)
          {
              bar1.getData().set(i-1, new XYChart.Data<>(""+(i)+" am", hourGraphs2[i-1]));
          }
          for(XYChart.Data<String,Number> x : bar1.getData())
          {
              x.getNode().setOnMouseClicked(event1-> setToPieGraph(x.getYValue(),barChart));
              x.getNode().setOnMouseEntered(event1 -> x.getNode().setEffect(new Glow(0.4)));
              x.getNode().setOnMouseExited(event1 -> x.getNode().setEffect(null));
          }
      }));
        timeline.play();

    }

    public void setBarWeek(){
        dateFormat.applyLocalizedPattern("M, Y");
        barChart.setTitle(dateFormat.format(date));
    }

    public void setBarMonth(){
        dateFormat.applyLocalizedPattern( "Y");
    }

    public void setBarYear(){
        dateFormat.applyLocalizedPattern("TOTAL");
    }

    public void setToPieGraph(Number value, Node n){
        graphScreen.getChildren().setAll(pieChart);
        float val = (float) value;
        double x,y,z;
        Random r = new Random();
        x = 0.5 * r.nextDouble();
        y = 0.5 * r.nextDouble();
        z = 1 - x - y;

        pieChart.getData().setAll(new PieChart.Data("Washer",x*val),new PieChart.Data("Dryer", y*val), new PieChart.Data("Light1",z*val));
        pieChart.setOnMouseClicked(event -> {
            if(event.isSecondaryButtonDown()) {
                pieChart.getData().clear();
                graphScreen.getChildren().setAll(n);
            }
        });

}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        graphScreen.getChildren().setAll(barChart);
        // Create a group of toggles so that only one toggle may be accessed at anypoint
        dayToggle.setOnAction(event ->{
                    setBarDay();
        }
        );

        monthToggle.setOnAction(event -> setBarWeek());
        //yearToggle.setOnAction(event -> box.getChildren().remove(1));
        tGroup.getToggles().addAll(dayToggle,monthToggle,yearToggle);
        //box.getChildren().addAll(new ListBoxThing("Turn off lights at 1 pm"),new ListBoxThing("Do Laundry at blah blah blah"), new ListBoxThing("blah blah blah blah"));
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scheduleButton.setOnAction(event -> {

            SequentialBehaviour sequentialBehaviour = new SequentialBehaviour();

            sequentialBehaviour.addSubBehaviour(new GenerateSchedule(MeshModel.getInstance().getMyAgent()));
            sequentialBehaviour.addSubBehaviour(new DaySimulation(MeshModel.getInstance().getMyAgent(), 10000));

            MeshModel.getInstance().getMyAgent().addBehaviour(sequentialBehaviour);

        });
    }
}

