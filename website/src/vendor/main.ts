/**
 * This is an entry point for additional assets,
 * require your assets under this file.
 * Example:
 * require('./bootstrap/css/bootstrap.min.css');
 */

require('./custom.css');
require('./bootstrap/css/bootstrap.min.css');
require('./sb-admin/sb-admin.css');
require('./font-awesome/css/font-awesome.min.css');
require('react-big-calendar/lib/css/react-big-calendar.css');
