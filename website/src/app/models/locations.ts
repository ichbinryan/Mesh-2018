export interface ILocations {
  isFetching?: boolean;
  locations?: any;
  error?: boolean;
  message?: any;
}

export interface ILocationAction {
  type: string;
  payload?: {
    locations?: number;
    message?: any;
    location?: any;
  };
}
