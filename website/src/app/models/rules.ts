export interface IRules {
  isFetching?: boolean;
  rules?: any;
  error?: boolean;
  message?: any;
}

export interface IRuleAction {
  type: string;
  payload?: {
    rules?: number;
    rule?: any;
    message?: any;
    location?: any;
  };
}
