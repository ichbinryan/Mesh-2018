export interface IAct {
  isFetching?: boolean;
  acts?: any;
  error?: boolean;
  message?: any;
}

export interface IActAction {
  type: string;
  payload?: {
    acts?: number;
    message?: any;
  };
}

export interface ISendAction {
  type: string;
  payload: {
    Action_ID: number;
    Device_ID: number;
  };
}
