export interface IDevices {
  isFetching?: boolean;
  devices?: any;
  error?: boolean;
  message?: any;
}

export interface IDevicesAction {
  type: string;
  payload?: {
    devices?: number;
    message?: any;
    deviceId?: number;
    iconId?: number;
  };
}
