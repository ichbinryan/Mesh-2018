export interface ISensorProperties {
  isFetching?: boolean;
  sensorProperties?: any;
  error?: boolean;
  message?: any;
}

export interface ISensorPropertiesAction {
  type: string;
  payload?: {
    sensorProperties?: number;
    message?: any;
  };
}
