export interface ISchedule {
  isFetching?: boolean;
  schedule?: any;
  error?: boolean;
  message?: any;
}

export interface IScheduleAction {
  type: string;
  payload?: {
    schedule?: any;
    message?: any;
  };
}
