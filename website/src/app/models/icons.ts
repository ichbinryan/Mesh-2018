export interface IIcons {
  isFetching?: boolean;
  icons?: any;
  error?: boolean;
  message?: any;
}

export interface IIconsAction {
  type: string;
  payload?: {
    icons?: any;
    message?: any;
  };
}
