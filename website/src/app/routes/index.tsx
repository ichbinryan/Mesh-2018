import * as React from 'react';
import { IndexRoute, Route } from 'react-router';
import { App, Home, Devices, Locations, Rules, Schedule } from 'containers';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="devices" component={Devices} />
    <Route path="locations" component={Locations} />
    <Route path="rules" component={Rules} />
    <Route path="schedule" component={Schedule} />
  </Route>
);
