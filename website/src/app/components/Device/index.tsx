import * as React from 'react';
import { Col, Panel, Accordion, Button, ListGroup, ListGroupItem, Modal, Row} from 'react-bootstrap';
const style = require('./style.css');

export class Device extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };

    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.onClickIcon = this.onClickIcon.bind(this);
  }

  public close() {
    this.setState({ showModal: false });
  }
  public open() {
    this.setState({ showModal: true });
  }

  public onClickAction(deviceId, actionId) {
    this.props.sendDeviceAction(deviceId, actionId);
  }

  public renderDeviceActions(deviceId: number) {
    let {acts} = this.props;
    if (acts === null) {
      return;
    }
    acts = acts.filter( (action) => {
      if (action.Device_ID === deviceId) {
        return action;
      }
    });
    return acts.map( (action) => {
      return (
        <ListGroupItem key={action.Action_ID} onClick={this.onClickAction.bind(this, deviceId, action.Action_ID)}>
          {action.Action_Name}
        </ListGroupItem>
      );
    });
  }

  public onClickIcon(deviceId: number, iconId: number) {
    this.props.changeDeviceIcon(deviceId, iconId);
    this.close();
  }

  public renderIcons(deviceId: number) {
    const {icons} = this.props;

    if (!icons) {
      return;
    }

    return icons.map((icon) => {
      return (
        <Col key={icon.Icon_ID} className={style.Icon} xs={2}>
          <img onClick={this.onClickIcon.bind(null, deviceId, icon.Icon_ID)}
            src={icon.Icon_URL}
          />
        </Col>
      );
    });
  }

  public render() {
    return (
      <Col xs={12} sm={6} md={4} key={this.props.device.Device_ID}>
        <Panel header={this.props.device.Device_Name} bsStyle="primary">
          <Accordion>
            <Panel bsStyle="info" header="Information" eventKey="1">
              <img src={this.props.device.Icon_URL} />
              <br />
              <br />
              <label>Location: {this.props.device.User_Location_Name}</label>
            </Panel>
            <Panel bsStyle="info" header="Actions" eventKey="2">
              <ListGroup>
                {this.renderDeviceActions(this.props.device.Device_ID)}
              </ListGroup>
            </Panel>
            <Panel bsStyle="info" header="Preferences" eventKey="3">
              <Button bsStyle="primary"
                onClick={this.open}>
                Change Icon
              </Button>
            </Panel>
            <Panel bsStyle="info" header="Energy Usage" eventKey="4">
              Insert charts here..
            </Panel>
          </Accordion>
        </Panel>
        {/* Modal */}
        <Modal bsSize="large" show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton={true}>
            <Modal.Title>Change Icon</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              {this.renderIcons(this.props.device.Device_ID)}
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

}
