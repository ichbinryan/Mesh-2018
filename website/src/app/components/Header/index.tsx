import * as React from 'react';
// import { Link } from 'react-router';
import { Navbar, NavDropdown, MenuItem, Nav} from 'react-bootstrap';
import { Link } from 'react-router';
// const style = require('./style.css');

export const Header = (props) => (
  <div id="wrapper">
      <Navbar inverse={true} fixedTop={true} collapseOnSelect={true} fluid={true} style={{margin: 0}}>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">MESH</Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Navbar.Collapse>
          { props.location !== '/' && (
            <Nav pullLeft={true}>
              <li><Link to="/devices">Devices</Link></li>
              <li><Link to="/locations">Locations</Link></li>
              <li><Link to="/rules">Rules</Link></li>
              <li><Link to="/schedule">Schedule</Link></li>
            </Nav>
        )}

          <Nav pullRight={true}>
            <NavDropdown title={<i className="fa fa-bell" />} id = "navDropdown3">
              <MenuItem eventKey="1" style={{width: 300}}>
                <div> <i className="fa fa-comment fa-fw" />New Device Added
                  <span className="pull-right text-muted small">4 minutes ago</span>
                </div>
              </MenuItem>
              <MenuItem divider={true} />
              <MenuItem eventKey="4">
                <div> <i className="fa fa-tasks fa-fw"/> New Task
                  <span className="pull-right text-muted small">4 minutes ago</span>
                </div>
              </MenuItem>
              <MenuItem divider={true} />
              <MenuItem eventKey="5">
                <div> <i className="fa fa-upload fa-fw"/> Server Rebooted
                  <span className="pull-right text-muted small">4 minutes ago</span>
                </div>
              </MenuItem>
              <MenuItem divider={true}/>
              <MenuItem eventKey="6">
                <strong>See All Alerts</strong> <i className="fa fa-angle-right"/>
              </MenuItem>
            </NavDropdown>

           <NavDropdown title={<i className="fa fa-user"/>} id="navDropdown4">
              <MenuItem eventKey="1">
                <span> <i className="fa fa-user fa-fw"/> User Profile </span>
              </MenuItem>
              <MenuItem eventKey="2">
                <span><i className="fa fa-gear fa-fw" /> Settings </span>
              </MenuItem>
              <MenuItem divider={true} />
              <MenuItem eventKey = "4">
                <span> <i className = "fa fa-sign-out fa-fw" /> Logout </span>
              </MenuItem>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>

);
