import * as React from 'react';
// import { Link } from 'react-router';

// const style = require('./style.css');

const Sidebar = () => (
  <div className="navbar-default sidebar" style={{ marginLeft: '-20px' }} role="navigation">
        <div className="sidebar-nav navbar-collapse collapse">
          <ul className="nav in" id="side-menu">

            <li>
              <a href="">
                <i className="fa fa-dashboard fa-fw" /> &nbsp;Home
              </a>
            </li>

            <li>
              <a href="">
                <i className="fa fa-location-arrow fa-fw" /> &nbsp;Locations
              </a>
            </li>

            <li>
              <a href="">
                <i className="fa fa-tablet fa-fw" /> &nbsp;Devices
              </a>
            </li>

            <li>
              <a href="">
                <i className="fa fa-list-ol fa-fw" /> &nbsp;Rules
              </a>
            </li>

            <li>
              <a href="http://www.cs.nmsu.edu/mesh/">About MESH</a>
            </li>
          </ul>
        </div>
      </div>
);

export default Sidebar;
