import * as React from 'react';
import { Col, Panel, Accordion } from 'react-bootstrap';

export class Sensor extends React.Component<any, any> {

  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <Col xs={12} sm={6} md={4} key={this.props.sensor.Device_ID}>
        <Panel header={this.props.sensor.Device_Name} bsStyle="primary">
          <Accordion>
            <Panel bsStyle="danger" header="Information" eventKey="1">
              <img src={this.props.sensor.Icon_URL} />
              <br />
              <br />
              <label>Location: {this.props.sensor.User_Location_Name}</label>
            </Panel>
            <Panel bsStyle="danger" header="Preferences" eventKey="3">
              asdf
            </Panel>
            <Panel bsStyle="danger" header="Stats" eventKey="4">
              Insert chart here...
            </Panel>
          </Accordion>
        </Panel>
      </Col>
    );
  }
}
