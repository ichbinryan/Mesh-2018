import { IIcons, IIconsAction } from 'models/icons';

/** Action Types */
export const ICONS_GET_REQUEST: string = 'icons/ICONS_GET_REQUEST';
export const ICONS_GET_SUCCESS: string = 'icons/ICONS_GET_SUCCESS';
export const ICONS_GET_FAILURE: string = 'icons/ICONS_GET_FAILURE';

/** Initial State */
const initialState: IIcons = {
  isFetching: false,
};

/** Reducer */
export function iconsReducer(state = initialState, action: IIconsAction) {
  switch (action.type) {
    case ICONS_GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case ICONS_GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        icons: action.payload.icons,
      });

    case ICONS_GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    default:
      return state;
  }
}

/** Async Action Creator */
export function getIcons() {
  return (dispatch) => {
    dispatch(iconsRequest());

    return fetch('/api/icons')
      .then((res) => {
        if (res.ok) {
          return res.json()
            .then((res) => dispatch(iconsSuccess(res)));
        } else {
          return res.json()
            .then((res) => dispatch(iconsFailure(res)));
        }
      })
      .catch((err) => dispatch(iconsFailure(err)));
  };
}

/** Action Creator */
export function iconsRequest(): IIconsAction {
  return {
    type: ICONS_GET_REQUEST,
  };
}

/** Action Creator */
export function iconsSuccess(icons: any): IIconsAction {
  console.log(icons);
  return {
    type: ICONS_GET_SUCCESS,
    payload: {
      icons,
    },
  };
}

/** Action Creator */
export function iconsFailure(message: any): IIconsAction {
  return {
    type: ICONS_GET_FAILURE,
    payload: {
      message,
    },
  };
}
