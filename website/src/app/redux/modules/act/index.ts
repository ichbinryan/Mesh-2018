import axios from 'axios';
import { IAct, IActAction, ISendAction } from 'models/act';

/** Action Types */
export const GET_DEVICE_ACTIONS: string = 'actions/GET_DEVICE_ACTIONS';
export const GET_DEVICE_ACTIONS_SUCCESS: string = 'actions/GET_DEVICE_ACTIONS_SUCCESS';
export const GET_DEVICE_ACTIONS_FAILURE: string = 'actions/GET_DEVICE_ACTIONS_FAILURE';
export const SEND_DEVICE_ACTION: string = 'server/SEND_DEVICE_ACTION';

/** Counter: Initial State */
const initialState: IAct = {
  acts: null,
};

/** Reducer: CounterReducer */
export function actionsReducer(state = initialState, action: IActAction) {
  switch (action.type) {
    case GET_DEVICE_ACTIONS:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_DEVICE_ACTIONS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        acts: action.payload.acts,
      });

    case GET_DEVICE_ACTIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    default:
      return state;
  }
}

/** Action Creator: Gets all Device Actions */
export function getDeviceActions() {
  return (dispatch) => {
    dispatch(getDeviceActionsRequest());
    // Make a request for a user with a given ID
    axios.get('/api/device_actions')
      .then((response) => {
        dispatch(deviceActionsSuccess(response.data));
      })
      .catch((error) => {
        dispatch(deviceActionsFailure(error));
      });
  };
}

/** Action Creator: Sends an Action to the agent */
export function sendDeviceAction(deviceId: number, actionId: number): ISendAction {
  return {
    type: SEND_DEVICE_ACTION,
    payload: {
      Action_ID: actionId,
      Device_ID: deviceId,
    },
  };
}

/** Action Creator */
export function getDeviceActionsRequest(): IActAction {
  return {
    type: GET_DEVICE_ACTIONS,
  };
}

/** Action Creator */
export function deviceActionsSuccess(acts: any): IActAction {
  return {
    type: GET_DEVICE_ACTIONS_SUCCESS,
    payload: {
      acts,
    },
  };
}

/** Action Creator */
export function deviceActionsFailure(message: any): IActAction {
  return {
    type: GET_DEVICE_ACTIONS_FAILURE,
    payload: {
      message,
    },
  };
}
