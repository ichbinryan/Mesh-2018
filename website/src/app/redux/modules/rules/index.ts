import axios from 'axios';
import { IRules, IRuleAction } from 'models/rules';

/** Action Types */
export const GET_RULES: string = 'rules/GET_RULES';
export const GET_RULES_SUCCESS: string = 'rules/GET_RULES_SUCCESS';
export const GET_RULES_FAILURE: string = 'rules/GET_RULES_FAILURE';
export const SAVE_RULE: string = 'rules/SAVE_RULE';
export const SAVE_RULE_SUCCESS: string = 'rules/SAVE_RULE_SUCCESS';
export const SAVE_RULE_FAILURE: string = 'rules/SAVE_RULE_FAILURE';
export const DELETE_RULE: string = 'rules/DELETE_RULE';
export const DELETE_RULE_SUCCESS: string = 'rules/DELETE_RULE_SUCCESS';
export const DELETE_RULE_FAILURE: string = 'rules/DELETE_RULE_FAILURE';

/** Rules: Initial State */
const initialState: IRules = {
  rules: null,
};

/** Reducer: rulesReducer */
export function rulesReducer(state = initialState, action: IRuleAction) {
  let newState;
  switch (action.type) {
    // A request to get all rules has been made
    case GET_RULES:
      return Object.assign({}, state, {
        isFetching: true,
      });
    // A request to get all rules has succeeded
    case GET_RULES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        rules: action.payload.rules,
      });
    // Getting all rules has failed
    case GET_RULES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });
    // A request to save a new rule has been made
    case SAVE_RULE:
      return Object.assign({}, state, {
        isFetching: true,
      });
    // The saving of a rule has succeeded
    case SAVE_RULE_SUCCESS:
      newState = Object.assign({}, state);
      newState.rules.push(action.payload.rule);
      return newState;
    // Could not save the new rule
    case SAVE_RULE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });
    // A request has been made to delete a particular rule
    case DELETE_RULE:
      return Object.assign({}, state, {
        isFetching: true,
      });
    // A request to delete a rule has completed
    case DELETE_RULE_SUCCESS:
      newState = Object.assign({}, state);
      newState.rules = newState.rules.filter((rule) => {
        if (rule.Rule_ID !== action.payload.rule) {
          return rule;
        }
      });
      return newState;
    // A request to delete a rule has failed
    case DELETE_RULE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    default:
      return state;
  }
}

/** Action Creator: Get a list of all rules */
export function getRules() {
  return (dispatch) => {
    dispatch(rulesRequest());
    // Make a request to get all the rules
    axios.get('/api/rules')
      .then((response) => {
        dispatch(getRulesSuccess(response.data));
      })
      .catch((error) => {
        dispatch(getRulesFailure(error));
      });
  };
}

/** Action Creator: Delete a rule by Id */
export function deleteRule(ruleId: number) {
  return (dispatch) => {
    dispatch(deleteRuleRequest());
    // Make a request to get all the rules
    axios.delete('/api/rules/' + ruleId)
      .then(() => {
        dispatch(deleteRuleSuccess(ruleId));
      })
      .catch((error) => {
        dispatch(deleteRuleFailure(error));
      });
  };
}

// Makes an API call to save a location to the database
export function saveRule(rule: any) {
  return (dispatch) => {
    dispatch(saveRuleRequest(rule));

    axios({
      url: '/api/rules',
      method: 'post',
      data: rule,
    }).then((response) => {
      dispatch(saveRulesuccess(response.data));
    }).catch((error) => {
      dispatch(saveRuleFailure(error));
    });
  };
}

export function deleteRuleRequest(): IRuleAction {
  return {
    type: DELETE_RULE,
  };
}

export function deleteRuleSuccess(ruleId: number): IRuleAction {
  return {
    type: DELETE_RULE_SUCCESS,
    payload: {
      rule: ruleId,
    },
  };
}

export function deleteRuleFailure(message): IRuleAction {
  return {
    type: DELETE_RULE_FAILURE,
    payload: {
      message,
    },
  };
}

// Save API request initiated
export function saveRuleRequest(rule): IRuleAction {
  return {
    type: SAVE_RULE,
    payload: {
        rule,
    },
  };
}

// Save API request failed
export function saveRuleFailure(message): IRuleAction {
  return {
    type: SAVE_RULE_FAILURE,
    payload: {
      message,
    },
  };
}

// Save API request suceeded
export function saveRulesuccess(rule): IRuleAction {
  return {
    type: SAVE_RULE_SUCCESS,
    payload: {
      rule,
    },
  };
}

/** Action Creator */
export function rulesRequest(): IRuleAction {
  return {
    type: GET_RULES,
  };
}

/** Action Creator */
export function getRulesSuccess(rules: any): IRuleAction {
  return {
    type: GET_RULES_SUCCESS,
    payload: {
      rules,
    },
  };
}

/** Action Creator */
export function getRulesFailure(message: any): IRuleAction {
  return {
    type: GET_RULES_FAILURE,
    payload: {
      message,
    },
  };
}
