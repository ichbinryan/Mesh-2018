import {ISchedule, IScheduleAction} from 'models/schedule';

/** Action Types */
export const GET_SCHEDULE: string = 'schedule/GET_SCHEDULE';
export const GET_SCHEDULE_SUCCESS: string = 'schedule/GET_SCHEDULE_SUCCESS';

/** Schedule: Initial State */
const initialState: ISchedule = {
  schedule: null,
};

/** Reducer: scheduleReducer */
export function scheduleReducer(state = initialState, action: IScheduleAction) {
  switch (action.type) {

    case GET_SCHEDULE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        schedule: action.payload.schedule,
      });

    default:
      return state;
  }
}

/** Action Creator */
export function getSchedule(): IScheduleAction {
  return {
    type: GET_SCHEDULE,
  };
}
