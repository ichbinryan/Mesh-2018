import axios from 'axios';
import { ILocations, ILocationAction } from 'models/locations';

/** Action Types */
export const GET_LOCATIONS: string = 'locations/GET_LOCATIONS';
export const GET_LOCATIONS_SUCCESS: string = 'locations/GET_LOCATIONS_SUCCESS';
export const GET_LOCATIONS_FAILURE: string = 'locations/GET_LOCATIONS_FAILURE';
export const SAVE_LOCATION: string = 'locations/SAVE_LOCATION';
export const SAVE_LOCATION_SUCCESS: string = 'locations/SAVE_LOCATION_SUCCESS';
export const SAVE_LOCATION_FAILURE: string = 'locations/SAVE_LOCATION_FAILURE';
export const DELETE_LOCATION: string = 'locations/DELETE_LOCATION';
export const DELETE_LOCATION_FAILURE: string  = 'locations/DELETE_LOCATION_FAILURE';
export const DELETE_LOCATION_SUCCESS: string  = 'locations/DELETE_LOCATION_SUCCESS';

/** Counter: Initial State */
const initialState: ILocations = {
  locations: null,
};

/** Reducer: CounterReducer */
export function locationsReducer(state = initialState, action: ILocationAction) {
  let newState = null;
  switch (action.type) {
    // A request to get all locations has been made
    case GET_LOCATIONS:
      return Object.assign({}, state, {
        isFetching: true,
      });
    // The request to get all locations has succeeded
    case GET_LOCATIONS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        locations: action.payload.locations,
      });
    // The request to get all locations has failed
    case GET_LOCATIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    // A request to save a new location has been made
    case SAVE_LOCATION:
      return Object.assign({}, state, {
        isFetching: true,
      });

    // The request to save a location has succeeded
    case SAVE_LOCATION_SUCCESS:
      newState = Object.assign({}, state);
      newState.locations.push(action.payload.location.Location_Preferences);
      return newState;

    // A request to add a new location has failed
    case SAVE_LOCATION_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    // A request to delete a location has been made
    case DELETE_LOCATION:
      return Object.assign({}, state, {
        isFetching: true,
      });

    // The request to delete a location has succeeded
    case DELETE_LOCATION_SUCCESS:
      newState = Object.assign({}, state);
      newState.locations = newState.locations.filter((location) => {
        if (location.Location_ID !== action.payload.location) {
          return location;
        }
      });
      return newState;

    // The request to delete a location has failed
    case DELETE_LOCATION_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    default:
      return state;
  }
}

/** Actom Creator: Delete a location */
export function deleteLocation(locationId: number) {
  return (dispatch) => {
    dispatch(locationDeleteRequest());

    // Make a request to delete a location
    axios.delete('/api/locations/' + locationId)
      .then(() => {
        dispatch(locationDeleteSuccess(locationId));
      })
      .catch((error) => {
        dispatch(locationDeleteFailure(error.response.data));
      });
  };
}

/** Action Creator: Gets all the locations */
export function getLocations() {
  return (dispatch) => {
    dispatch(locationsRequest());
    // Make a request to get all the locations
    axios.get('/api/locations')
      .then((response) => {
        dispatch(locationSuccess(response.data));
      })
      .catch((error) => {
        dispatch(locationFailure(error));
      });
  };
}

// Makes an API call to save a location to the database
export function saveLocation(locationName: string) {
  return (dispatch) => {
    dispatch(saveLocationRequest());

    axios({
      url: '/api/locations',
      method: 'post',
      data: {
        Location: {
          Location_Type_Name: 'room',
        },
        Location_Preferences: {
          User_Location_Name: locationName,
          User_ID: 1,
        },
      },
    }).then((response) => {
      dispatch(saveLocationSuccess(response.data));
    }).catch((error) => {
      dispatch(saveLocationFailure(error));
    });
  };
}

export function locationDeleteRequest(): ILocationAction {
  return {
    type: DELETE_LOCATION,
  };
}

export function locationDeleteSuccess(locationId: number): ILocationAction {
  return {
    type: DELETE_LOCATION_SUCCESS,
    payload: {
      location: locationId,
    },
  };
}

export function locationDeleteFailure(message): ILocationAction {
  return {
    type: DELETE_LOCATION_FAILURE,
    payload: {
      message,
    },
  };
}

// Save API request initiated
export function saveLocationRequest(): ILocationAction {
  return {
    type: SAVE_LOCATION,
  };
}

// Save API request failed
export function saveLocationFailure(message): ILocationAction {
  return {
    type: SAVE_LOCATION_FAILURE,
    payload: {
      message,
    },
  };
}

// Save API request suceeded
export function saveLocationSuccess(location): ILocationAction {
  return {
    type: SAVE_LOCATION_SUCCESS,
    payload: {
      location,
    },
  };
}

/** Action Creator */
export function locationsRequest(): ILocationAction {
  return {
    type: GET_LOCATIONS,
  };
}

/** Action Creator */
export function locationSuccess(locations: any): ILocationAction {
  return {
    type: GET_LOCATIONS_SUCCESS,
    payload: {
      locations,
    },
  };
}

/** Action Creator */
export function locationFailure(message: any): ILocationAction {
  return {
    type: GET_LOCATIONS_FAILURE,
    payload: {
      message,
    },
  };
}
