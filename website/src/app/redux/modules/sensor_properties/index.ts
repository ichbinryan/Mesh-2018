import axios from 'axios';
import { ISensorProperties, ISensorPropertiesAction } from 'models/sensor_properties';

/** Action Types */
export const GET_SENSOR_PROPS: string = 'sensor_props/GET_SENSOR_PROPS';
export const GET_SENSOR_PROPS_SUCCESS: string = 'sensor_props/GET_SENSOR_PROPS_SUCCESS';
export const GET_SENSOR_PROPS_FAILURE: string = 'sensor_props/GET_SENSOR_PROPS_FAILURE';

/** Sensor Properties: Initial State */
const initialState: ISensorProperties = {
  sensorProperties: null,
};

/** Reducer: CounterReducer */
export function sensorPropertiesReducer(state = initialState, action: ISensorPropertiesAction) {
  switch (action.type) {
    case GET_SENSOR_PROPS:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_SENSOR_PROPS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        sensorProperties: action.payload.sensorProperties,
      });

    case GET_SENSOR_PROPS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    default:
      return state;
  }
}

/** Action Creator: gets all sensor properties by location */
export function getSensorPropertiesByLocation(locationId: number) {
  return (dispatch) => {
    dispatch(sensorPropertiesRequest());
    // Make a request to get all the locations
    axios.get('/api/sensor_properties_for_location/' + locationId)
      .then((response) => {
        dispatch(sensorPropertiesSuccess(response.data));
      })
      .catch((error) => {
        dispatch(sensorPropertiesFailure(error));
      });
  };
}

/** Action Creator */
export function sensorPropertiesRequest(): ISensorPropertiesAction {
  return {
    type: GET_SENSOR_PROPS,
  };
}

/** Action Creator */
export function sensorPropertiesSuccess(sensorProperties: any): ISensorPropertiesAction {
  return {
    type: GET_SENSOR_PROPS_SUCCESS,
    payload: {
      sensorProperties,
    },
  };
}

/** Action Creator */
export function sensorPropertiesFailure(message: any): ISensorPropertiesAction {
  return {
    type: GET_SENSOR_PROPS_FAILURE,
    payload: {
      message,
    },
  };
}
