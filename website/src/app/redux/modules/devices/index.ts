import axios from 'axios';
import { IDevices, IDevicesAction } from 'models/devices';

/** Action Types */
export const GET_DEVICES: string = 'devices/GET_DEVICES';
export const GET_DEVICES_SUCCESS: string = 'devices/GET_DEVICES_SUCCESS';
export const GET_DEVICES_FAILURE: string = 'devices/GET_DEVICES_FAILURE';

export const UPDATE_ICON: string = 'devices/UPDATE_ICON';
export const UPDATE_ICON_SUCCESS: string = 'devices/UPDATE_ICON_SUCCESS';
export const UPDATE_ICON_FAILURE: string = 'devices/UPDATE_ICON_FAILURE';

/** Counter: Initial State */
const initialState: IDevices = {
  devices: null,
};

/** Reducer: handles all device reducers and updates */
export function deviceReducer(state = initialState, action: IDevicesAction) {
  switch (action.type) {
    case GET_DEVICES:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        devices: action.payload.devices,
      });

    case GET_DEVICES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    case UPDATE_ICON_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true,
      });

    case UPDATE_ICON:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case UPDATE_ICON_SUCCESS:
      const newState = Object.assign({}, state);
      newState.isFetching = false;
      newState.devices.map((device) => {
        if (device.Device_ID === action.payload.deviceId) {
            device.Icon_ID = action.payload.iconId;
        }
      });
      return newState;

    default:
      return state;
  }
}

/** Action Creator: Gets all devices */
export function getDevices() {
  return (dispatch) => {
    dispatch(devicesRequest());
    // Make a request for a user with a given ID
    axios.get('/api/devices')
      .then((response) => {
        dispatch(devicesSuccess(response.data));
      })
      .catch((error) => {
        dispatch(devicesFailure(error));
      });
  };
}

/** Action creator: sets the icon for a particular device */
export function changeDeviceIcon(deviceId: number, iconId: number) {
  return (dispatch) => {
    dispatch(updateIconRequest());
    // Make a request for a user with a given ID
    axios({
      url: '/api/devices/' + deviceId,
      method: 'patch',
      data: {
        Icon_ID: iconId,
      },
    }).then(() => {
      dispatch(updateIconSuccess(deviceId, iconId));
    }).catch((error) => {
      dispatch(updateIconFailure(error));
    });
  };
}

export function updateIconRequest() {
  return {
    type: UPDATE_ICON,
  };
}

export function updateIconSuccess(deviceId, iconId): IDevicesAction {
  return {
    type: UPDATE_ICON_SUCCESS,
    payload: {
      deviceId,
      iconId,
    },
  };
}

export function updateIconFailure(message): IDevicesAction {
  return {
    type: UPDATE_ICON_FAILURE,
    payload: {
      message,
    },
  };
}

/** Action Creator */
export function devicesRequest(): IDevicesAction {
  return {
    type: GET_DEVICES,
  };
}

/** Action Creator */
export function devicesSuccess(devices: any): IDevicesAction {
  return {
    type: GET_DEVICES_SUCCESS,
    payload: {
      devices,
    },
  };
}

/** Action Creator */
export function devicesFailure(message: any): IDevicesAction {
  return {
    type: GET_DEVICES_FAILURE,
    payload: {
      message,
    },
  };
}
