const io = require('socket.io-client');
import {SEND_DEVICE_ACTION} from '../modules/act';
import {GET_SCHEDULE, GET_SCHEDULE_SUCCESS} from '../modules/schedule';
import {SAVE_RULE} from '../modules/rules';

let socket = null;

export function socketIoMiddleware(store) {
  return (next) => (action) => {
    const result = next(action);
    if (!socket) {
      return;
    }

    switch (action.type) {
      case 'server/connected-user':
        const profile = store.getState();
        socket.emit('connected-user', profile);
        break;
      case SEND_DEVICE_ACTION:
        socket.emit('device.action', action.payload);
        break;
      case GET_SCHEDULE:
        socket.emit('device.getschedule');
        break;
      case SAVE_RULE:
        socket.emit('rule.newRule', action.payload);
        break;
      default:
        break;
    }
    return result;
  };
}

export default function(store) {
  socket = io.connect('http://localhost:8889');
  socket.on('device.schedule', (schedule) => {
    for (const s of schedule) {
      s.start = new Date(s.start);
      s.end = new Date(s.end);
    }
    store.dispatch({
      type: GET_SCHEDULE_SUCCESS,
      payload: {
        schedule,
      },
    });
  });
};
