import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { locationsReducer } from './modules/locations';
import { rulesReducer } from './modules/rules';
import { deviceReducer } from './modules/devices';
import { sensorPropertiesReducer } from './modules/sensor_properties';
import { actionsReducer } from './modules/act';
import { iconsReducer } from './modules/icons';
import { scheduleReducer } from './modules/schedule';
import { IStore } from './IStore';

const { reducer } = require('redux-connect');

const rootReducer: Redux.Reducer<IStore> = combineReducers<IStore>({
  routing: routerReducer,
  locations: locationsReducer,
  rules: rulesReducer,
  devices: deviceReducer,
  deviceActions: actionsReducer,
  sensorProperties: sensorPropertiesReducer,
  schedule: scheduleReducer,
  icons: iconsReducer,
  reduxAsyncConnect: reducer,
});

export default rootReducer;
