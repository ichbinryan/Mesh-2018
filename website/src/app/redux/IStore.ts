import { ILocations } from 'models/locations';
import { IIcons } from 'models/icons';
import { IDevices } from 'models/devices';
import { IRules } from 'models/rules';

export interface IStore {
  locations: ILocations;
  icons: IIcons;
  devices: IDevices;
  rules: IRules;
};
