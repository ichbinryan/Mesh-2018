import * as React from 'react';
import { Device } from '../../components/Device';
import { Sensor } from '../../components/Sensor';
import { getDevices, changeDeviceIcon } from 'modules/devices/';
import { getDeviceActions, sendDeviceAction } from 'modules/act';
import { getIcons } from 'modules/icons';
import { IDevices, IDevicesAction } from 'models/devices';
import { IActAction, IAct, ISendAction } from 'models/act';
import { IIconsAction, IIcons } from 'models/icons';
const { connect } = require('react-redux');
const style = require('./style.css');
import { Grid, Row } from 'react-bootstrap';

interface IProps {
  devices: IDevices;
  deviceActions: IAct;
  icons: IIcons;
  getDevices: Redux.ActionCreator<IDevicesAction>;
  getDeviceActions: Redux.ActionCreator<IActAction>;
  sendDeviceAction: Redux.ActionCreator<ISendAction>;
  changeDeviceIcon: Redux.ActionCreator<IDevicesAction>;
  getIcons: Redux.ActionCreator<IIconsAction>;
}

@connect(
  (state) => ({
    devices: state.devices,
    deviceActions: state.deviceActions,
    icons: state.icons,
  }),
  (dispatch) => ({
    getDevices: () => dispatch(getDevices()),
    getDeviceActions: () => dispatch(getDeviceActions()),
    getIcons: () => dispatch(getIcons()),
    sendDeviceAction: (deviceId: number, actionId: number) => dispatch(sendDeviceAction(deviceId, actionId)),
    changeDeviceIcon: (deviceId: number, iconId: number) => dispatch(changeDeviceIcon(deviceId, iconId)),
  }),
)

class Devices extends React.Component<IProps, void> {

  constructor(props) {
    super(props);
    this.sendDeviceAction = this.sendDeviceAction.bind(this);
    this.changeDeviceIcon = this.changeDeviceIcon.bind(this);
  }

  public componentWillMount() {
    this.props.getDevices();
    this.props.getDeviceActions();
    this.props.getIcons();
  }

  public sendDeviceAction(deviceId: number, actionId: number) {
    this.props.sendDeviceAction(deviceId, actionId);
  }

  public changeDeviceIcon(deviceId: number, iconId: number) {
    this.props.changeDeviceIcon(deviceId, iconId);
  }

  public renderDeviceList() {
    const devices = this.props.devices.devices;
    const {icons} = this.props.icons;

    if ( devices == null || icons == null) {
      return;
    }
    return devices.map((dev) => {
        if ( dev.Device_Type_ID === 1 ) {
          return (
            <Device key={dev.Device_ID}
              device={dev}
              acts={this.props.deviceActions.acts}
              sendDeviceAction={this.sendDeviceAction}
              changeDeviceIcon={this.changeDeviceIcon}
              icons={icons}
            />
          );
      }
    });
  }

  public renderSensorList() {
    const devices = this.props.devices.devices;
    const {icons} = this.props;
    if ( devices == null || icons == null) {
      return;
    }
    return devices.map((dev) => {
      if ( dev.Device_Type_ID === 2 ) {
        return (
          <Sensor
            sensor={dev}
            key={dev.Device_ID}
            icons={icons}
          />
        );
      }

    });
  }

  public render() {
    return (
      <Grid className={style.Devices}>
        <h2>Devices</h2>
        <Row>
          {this.renderDeviceList()}
        </Row>
        <h2>Sensors</h2>
        <Row>
          {this.renderSensorList()}
        </Row>
      </Grid>
    );
  }
}

export { Devices }
