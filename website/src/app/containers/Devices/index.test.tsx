import { expect } from 'chai';
import { renderComponent } from 'helpers/TestHelper';
import { Devices } from './index';

describe('<About />', () => {

  const component = renderComponent(Devices);

  it('Renders with correct style', () => {
    const style = require('./style.css');
    expect(component.find(style.Devices)).to.exist;
  });

});
