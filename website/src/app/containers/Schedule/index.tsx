import * as React from 'react';
const BigCalendar = require('react-big-calendar');
const moment = require('moment');
import {ISchedule, IScheduleAction} from 'models/schedule';
import {getSchedule} from 'modules/schedule';
import { Row, Grid, Col } from 'react-bootstrap';
const { connect } = require('react-redux');

BigCalendar.momentLocalizer(moment);
// const { connect } = require('react-redux');
// const { asyncConnect } = require('redux-connect');
const style = require('./style.css');

interface IProps {
  schedule: ISchedule;
  getSchedule: Redux.ActionCreator<IScheduleAction>;
}

interface IState {
  schedule: any;
}

@connect(
  (state) => ({
    schedule: state.schedule,
  }),
  (dispatch) => ({
    getSchedule: () => dispatch(getSchedule()),
  }),
)

class Schedule extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
  }

  public componentWillMount() {
    this.props.getSchedule();
  }

  public render() {
    const {schedule} = this.props.schedule;
    if (schedule == null) {
      return <div>Loading...</div>;
    }
    return (
      <Grid>
        <Row>
          <Col xs={12} className={style.Schedule}>
            <BigCalendar
              events={schedule}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export { Schedule }
