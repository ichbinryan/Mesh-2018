import * as React from 'react';
import { getLocations, saveLocation, deleteLocation } from 'modules/locations/';
import { ILocations } from 'models/locations';
import { ILocationAction } from 'models/locations';
const { connect } = require('react-redux');
import {ListGroupItem, ListGroup, Grid, Row, Col,
  Button, Modal, FormControl, FormGroup} from 'react-bootstrap';
const style = require('./style.css');

interface IProps {
  locations: ILocations;
  getLocations: Redux.ActionCreator<ILocationAction>;
  saveLocation: Redux.ActionCreator<ILocationAction>;
  deleteLocation: Redux.ActionCreator<ILocationAction>;
};

interface IState {
  showModal: boolean;
  locationName: string;
};

@connect(
  (state) => ({ locations: state.locations }),
  (dispatch) => ({
    getLocations: () => dispatch(getLocations()),
    saveLocation: (locationName: string) => dispatch(saveLocation(locationName)),
    deleteLocation: (locationId: number) => dispatch(deleteLocation(locationId)),
  }),
)

class Locations extends React.Component<IProps, IState> {

  public constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      locationName: '',
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onSaveLocation = this.onSaveLocation.bind(this);
  }

  public componentWillMount() {
    this.props.getLocations();
  }

  public close() {
    this.setState({ showModal: false });
  }

  public open() {
    this.setState({ showModal: true });
  }

  public onChangeInput(e) {
    this.setState({locationName: e.target.value});
  }

  // Checks to see if string is alphaNum and spaces
  public isAlphaNumeric(str) {
    return str.match(/^[a-z\d\-_\s]+$/i);
  };

  public getValidationState() {
    const length = this.state.locationName.length;
    if ( this.isAlphaNumeric(this.state.locationName) === false ) {
      return 'error';
    } else if ( (length > 3)) {
      return 'success';
    } else if ( length > 0 ) {
      return 'error';
    }
  }

  public onSaveLocation() {
    if ( this.getValidationState() === 'success') {
      this.close();
      this.props.saveLocation(this.state.locationName);
    }
  }

  public onClickTrash(location) {
    this.props.deleteLocation(location.Location_ID);
  }

  public renderLocations() {
    const { locations } = this.props.locations;
    if ( locations == null ) {
      return;
    }

    return locations.map( (loc, i) => {
      return (
        <ListGroupItem key={i}>
          {loc.User_Location_Name}
          <div className="pull-right">
            <i onClick={this.onClickTrash.bind(this, loc)} className="fa fa-trash fa-lg"/>
          </div>
        </ListGroupItem>
      );
    });
  }

  public render() {

    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <ListGroup className={style.Location}>
              {this.renderLocations()}
            </ListGroup>
            <Button
              bsStyle="primary"
              onClick={this.open}>
              Add Location
            </Button>
            <Modal show={this.state.showModal} onHide={this.close}>
              <Modal.Header closeButton={true}>
                <Modal.Title>Add Location</Modal.Title>
              </Modal.Header>

              <Modal.Body >
                <FormGroup
                  controlId="formBasicText"
                  validationState={this.getValidationState()}
                >
                  <FormControl
                    type="text"
                    value={this.state.locationName}
                    placeholder="Enter Location Name"
                    onChange={this.onChangeInput}
                  />
                  <FormControl.Feedback />
                </FormGroup>
              </Modal.Body>

              <Modal.Footer>
                <Button onClick={this.close}>Close</Button>
                <Button
                  onClick={this.onSaveLocation}
                  bsStyle="primary">
                  Save changes
                </Button>
              </Modal.Footer>

            </Modal>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export { Locations }
