import * as React from 'react';
const style = require('./style.css');
import {Widget} from '../../components/Widget/';
// import {Donut} from '../../components/Donut/';
import {Grid, Col, Row, Panel} from 'react-bootstrap';

import {AreaChart, ResponsiveContainer, XAxis, YAxis, CartesianGrid, Tooltip, Area} from 'recharts';

const data = [
      { name: 'Monday', predicted: 2400, actual: 2400, potential: 1000 },
      { name: 'Tuesday', predicted: 1398, actual: 2210, potential: 1200 },
      { name: 'Wednesday', predicted: 2200, actual: 2290, potential: 1300 },
      { name: 'Thursday', predicted: 3908, actual: 2000, potential: 1200 },
      { name: 'Friday', predicted: 4800, actual: 2181, potential: 1500 },
      { name: 'Saturday', predicted: 3800, actual: 2500, potential: 1900 },
      { name: 'Sunday', predicted: 4300, actual: 2100, potential: 2000 },
];

// const data2 = [
//       { name: 'Toaster', uv: 4000, pv: 2400, amt: 2400, value: 600 },
//       { name: 'Microwave', uv: 3000, pv: 1398, amt: 2210, value: 300 },
//       { name: 'Tesla', uv: 2000, pv: 9800, amt: 2290, value: 500 },
//       { name: 'Television', uv: 2780, pv: 3908, amt: 2000, value: 400 },
//       { name: 'Washer', uv: 1890, pv: 4800, amt: 2181, value: 200 },
//       { name: 'Dryer', uv: 2390, pv: 3800, amt: 2500, value: 700 },
//       { name: 'lights', uv: 3490, pv: 4300, amt: 2100, value: 100 },
// ];

class Home extends React.Component<any, any> {
  public render() {
    return (
      <Grid className={style.Home}>
        <Row>
          <Col xs={12} sm={6} md={6} lg={3}>
            <Widget
              style="panel-green"
              icon="fa fa-lightbulb-o fa-5x"
              count=""
              headerText="Devices"
              footerText="View Devices"
              linkTo="/devices"
            />
          </Col>
          <Col xs={12} sm={6} md={6} lg={3}>
            <Widget
              style="panel-primary"
              icon="fa fa-location-arrow fa-5x"
              count=""
              headerText="Locations"
              footerText="View Locations"
              linkTo="/locations"
            />
          </Col>
          <Col xs={12} sm={6} md={6} lg={3}>
            <Widget
              style="panel-yellow"
              icon="fa fa-tasks fa-5x"
              count=""
              headerText="Rules"
              footerText="View Rules"
              linkTo="/rules"
            />
          </Col>
          <Col xs={12} sm={6} md={6} lg={3}>
            <Widget
              style="panel-red"
              icon="fa fa-calendar-o fa-5x"
              count=""
              headerText="Schedule"
              footerText="View Schedule"
              linkTo="/schedule"
            />
          </Col>
        </Row>

        <Row>
          <Col xs={12} sm={8}>
            <Panel
              header={(
                <span>
                  <i className="fa fa-bar-chart-o fa-fw" /> Weekly power consumption
                </span>
            )}>
              <div>
                <ResponsiveContainer width="100%" aspect={2}>
                  <AreaChart data={data} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} >
                    <XAxis dataKey="name" />
                    <YAxis />
                    <CartesianGrid stroke="#ccc" />
                    <Tooltip />
                    <Area type="monotone" dataKey="potential" stackId="1" stroke="#8884d8" fill="#8884d8" />
                    <Area type="monotone" dataKey="predicted" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
                    <Area type="monotone" dataKey="actual" stackId="1" stroke="#ffc658" fill="#ffc658" />
                  </AreaChart>
                </ResponsiveContainer>
              </div>

            </Panel>
          </Col>
          <Col xs={12} sm={4}>
            <Panel
              header={(
                <span>
                  <i className="fa fa-clock-o fa-fw" /> Today's Summary
                </span>
              )}
            >
              <div>
                <ul className="timeline">
                  <li>
                    <div className="timeline-badge"><i className="fa fa-check" />
                    </div>
                    <div className="timeline-panel">
                      <div className="timeline-heading">
                        <h4 className="timeline-title">Roomba Clean</h4>
                        <p>
                          <small className="text-muted">
                            <i className="fa fa-clock-o" /> 11 AM
                          </small>
                        </p>
                      </div>
                      <div className="timeline-body">
                        <p>
                            The roomba will clean the living room to 10% dirt
                        </p>
                      </div>
                    </div>
                  </li>
                  <li className="timeline-inverted">
                    <div className="timeline-badge warning"><i className="fa fa-battery" />
                    </div>
                    <div className="timeline-panel">
                      <div className="timeline-heading">
                        <h4 className="timeline-title">Charge Tesla Battery</h4>
                        <p>
                          <small className="text-muted">
                            <i className="fa fa-clock-o" /> 12 AM
                          </small>
                        </p>
                      </div>
                      <div className="timeline-body">
                        <p>
                            The battery on your Tesla car will be charged
                        </p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="timeline-badge danger"><i className="fa fa-lock" />
                    </div>
                    <div className="timeline-panel">
                      <div className="timeline-heading">
                        <h4 className="timeline-title">Lock Doors</h4>
                        <p>
                          <small className="text-muted">
                            <i className="fa fa-clock-o" /> 12 AM
                          </small>
                        </p>
                      </div>
                      <div className="timeline-body">
                          <p>
                              Lock the front door, the garage, and the back door.
                          </p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </Panel>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export { Home }
