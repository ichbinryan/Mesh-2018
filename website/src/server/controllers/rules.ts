const {db} = require('./../db/sqlite3');
const _ = require('lodash');

// Insert a new rule
module.exports.insert = (req, res) => {
    const ruleObj = req.body;
    ruleObj.Goal_State = parseInt(ruleObj.Goal_State, 10);

    // Change the keys to contain the '$' prefix
    const keys = Object.keys(ruleObj);
    for ( let j=0; j < keys.length; j++ ) {
        ruleObj['$' + keys[j]] = ruleObj[keys[j]];
        delete ruleObj[keys[j]];
    }

    console.log(ruleObj);

    // The sql query to be run
    let sql = 'INSERT INTO Rule_Table ' +
            '(Rule_Type, Location_ID, Sensor_Prop_ID, Rule_Relation, ' +
            'Time_Prefix, Goal_State, Time_Value, Time_Modifier) ' +
            'VALUES ($Rule_Type, $Location_ID, $Sensor_Prop_ID, ' +
            '$Rule_Relation, $Time_Prefix, $Goal_State, $Time_Value, ' +
            '$Time_Modifier)';

    db.run(sql, ruleObj, function (err) {
        if (err) {
            console.log(err);
            return res.status(400).send();
        }
        const Rule_ID = this.lastID;

        sql = 'SELECT * FROM Rule_Table JOIN Sensor_Properties ' +
        'on Sensor_Properties.Sensor_Prop_ID = Rule_Table.Sensor_Prop_ID ' +
        'JOIN Location_Preferences on Location_Preferences.Location_ID = Rule_Table.Location_ID ' +
        'WHERE Rule_Table.Rule_ID = ' + Rule_ID;

        db.all(sql, function(err, row) {
          if(err) {
            console.log(err);
            res.status(400).send();
          }

          res.status(200).send(row[0]);
          res.end();
        });


    });
};

// get all rules from the data
module.exports.getAll = (req, res) => {
    if (!req) {
      return;
    }
    var sql ='SELECT * FROM Rule_Table JOIN Location_Preferences ' +
    'ON Location_Preferences.Location_ID = Rule_Table.Location_ID ' +
    'JOIN Sensor_Properties ON Sensor_Properties.Sensor_Prop_ID = Rule_Table.Sensor_Prop_ID';

    db.all(sql, (err , row) => {
        if (err) {
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row);
        res.end();
    });
};

// get a rule by Rule_ID
// module.exports.getById = (req, res) => {
//     var id = req.params.Rule_ID;
//     db.get('SELECT * FROM Rule_Table WHERE Rule_ID = ?', id, function(err, row){
//         if(err) {
//             console.log(error);
//             return res.status(404);
//         }
//         res.status(200).send(row);
//         res.end();
//     });
// };
//
// //update a rule by Rule_ID
// module.exports.updateById = (req, res) => {
//     var id = req.params.Rule_ID;
//     var body = _.pick(req.body, [
//         'Rule_ID',
//         'Rule_Type',
//         'Location_ID',
//         'Sensor_Prop_ID',
//         'Sensor_Property_Name',
//         'Rule_Relation',
//         'Time_Prefix',
//         'Goal_State',
//         'Time_Value',
//         'Time_Modifier'
//     ]);
//
//     var sql = 'UPDATE Rule_Table SET ';
//
//     var keys = Object.keys(body);
//     for (var j=0; j < keys.length; j++) {
//         sql += keys[j] + ' = $' + keys[j] + ', ';
//         body['$' + keys[j]] = body[keys[j]];
//         delete body[keys[j]];
//     }
//
//     //Remove final ',' and add a space
//     sql = sql.substring(0, sql.length - 2) + ' ';
//
//     sql += 'WHERE Rule_ID = ' + id;
//
//     // As an object with named parameters.
//     db.run(sql, body, (err, rows) => {
//         if(err) {
//             console.log(err);
//             return res.status(400).send({});
//         }
//
//         res.status(200).send(rows);
//     });
// };
//
// delete a rule by Rule_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Rule_ID;
    db.run('DELETE FROM Rule_Table WHERE Rule_ID = ?', id, function(err, row){
        if(err) {
            console.log(err);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
