const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//get all locations from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Icon", function(err, row){
        if (err){
            console.log(err);
            return res.status(400).send();
        }
        res.status(200).send(row);
        res.end();
    });
};
