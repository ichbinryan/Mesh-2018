const {db} = require('../db/sqlite3');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const bcrypt = require('bcryptjs');

const _ = require('lodash');

exports.signup = (req, res, next) => {

    const body = _.pick(req.body, [
        'username',
        'password'
    ]);
    
    body.Access_Level = 0;

    if(!body.username) {
        return res.status(400).send("username is required");
    }

    if(!body.password) {
        return res.status(400).send("password is required");
    }
    
    
    //generate a salt
    bcrypt.genSalt(11, (err, salt) => {
        
        let user = req.body;
        let _res = res;
        
        //hash using salt
        bcrypt.hash(body.password, salt, (err, hash) => {
            body.password = hash;
        
            //Change the keys to contain the '$' prefix
            var keys = Object.keys(body);
            for (var j=0; j < keys.length; j++) {
                body['$' + keys[j]] = body[keys[j]];
                delete body[keys[j]];
            }
            
            //The sql query to be run
            var sql = "INSERT INTO Users " +
                    "(User_Name, Password, Access_Level)" +
                    "VALUES ( $username , $password, $Access_Level)";
        
            db.run(sql, body, function(err) {
                if (err){
                    console.log(err);
                    return res.status(400);
                }
                
                delete user.password;
                user.User_ID = this.lastID;
                
                var access = 'auth';
                const timestamp = new Date().getTime();
                
                var token = jwt.sign({ sub: user.User_ID, iat: timestamp, access}, config.secret).toString();
                
                _res.header('x-auth', token).send(user);
            });
        
        });
    });
}

exports.login = (req, res, next) => {
    let user = req.user;
    
    let access = 'auth';
    const timestamp = new Date().getTime();
    
    var token = jwt.sign({ sub: user.User_ID, iat: timestamp, access}, config.secret).toString();
    
    res.header('x-auth', token).send(user);
}