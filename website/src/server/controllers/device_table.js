const {db} = require('../db/sqlite3');
const _ = require('lodash');

//Insert a new device
module.exports.insert = (req, res) => {
    var deviceObj = req.body;

    let deviceObjClone = JSON.parse(JSON.stringify(deviceObj));

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(deviceObj);
    for (var j=0; j < keys.length; j++) {
        deviceObj['$' + keys[j]] = deviceObj[keys[j]];
        delete deviceObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Table " +
            "(Device_Type_ID, Device_Name, " +
            "Icon_ID, " +
            "Location_ID, Class_Name) VALUES ($Device_Type_ID, " +
            "$Device_Name, " +
            "$Icon_ID, $Location_Name, $Class_Name)";

    db.run(sql, deviceObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400).send();
        }

        res.status(200).send(deviceObjClone);
        res.end();
    });
};

//get all devices from the data
module.exports.getAll = (req, res) => {
  db.all("SELECT * FROM Device_Table", function(err, rows){
      if (err){
          console.log(err);
          return res.status(400).send();
      }
      res.status(200).send(rows)
      res.end();
  });
};

module.exports.getActuators = (req, res) => {
  db.all("SELECT * FROM Device_Table WHERE Device_Type_ID = 1", function(err, rows){
      if (err){
          console.log(err);
          return res.status(400).send();
      }
      res.status(200).send(rows)
      res.end();
  });
};

module.exports.getSensors = (req, res) => {
  db.all("SELECT * FROM Device_Table WHERE Device_Type_ID = 2", function(err, rows){
      if (err){
          console.log(err);
          return res.status(400).send();
      }
      res.status(200).send(rows)
      res.end();
  });
};

//get all devices with details from the database
module.exports.getDevicePage = (req, res) => {
  var sql ="SELECT * FROM Device_Table JOIN Location_Preferences " +
  "ON Device_Table.Location_ID = Location_Preferences.Location_ID " +
  "JOIN Device_Icon ON Device_Table.Icon_ID = Device_Icon.Icon_ID";
  db.all(sql, function(err, row){
      if (err){
          console.log(err);
          return res.status(400).send();
      }
      res.status(200).send(row)
      res.end();
  });
};

module.exports.getDeviceActionsByID = (req, res) => {
  const Device_ID = req.params.Device_ID;
  var sql = 'SELECT * FROM Device_Table ' +
  'JOIN Device_Action_Relation ON Device_Table.Device_ID = Device_Action_Relation.Device_ID ' +
  'JOIN Device_Actions ON Device_Actions.Action_ID = Device_Action_Relation.Action_ID ' +
  'WHERE Device_Table.Device_ID = ?';

  db.get(sql, Device_ID, function(err, rows) {
    if (err) {
      console.log(err);
      return res.status(400).send();
    }
    res.status(200).send(rows);
  });
};

module.exports.getAllDeviceActions = (req, res) => {
  var sql = 'SELECT * FROM Device_Table ' +
  'JOIN Device_Action_Relation ON Device_Table.Device_ID = Device_Action_Relation.Device_ID ' +
  'JOIN Device_Actions ON Device_Actions.Action_ID = Device_Action_Relation.Action_ID';

  db.all(sql, function(err, rows) {
    if (err) {
      console.log(err);
      return res.status(400).send();
    }
    res.status(200).send(rows);
  });
};

//get a device by Device_ID
module.exports.getById = (req, res) => {
    var id = req.params.Device_ID;
    db.get("SELECT * FROM Device_Table WHERE Device_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404).send();
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a device by Device_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Device_ID;
    var body = _.pick(req.body, [
        'Device_Type_ID',
        'Device_Name',
        'Icon_ID',
        'Location_ID',
        'Class_Name_ID'
    ]);

    var sql = "UPDATE Device_Table SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Device_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, function(err, rows) {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(this.lastId);
    });
};

//delete a device by Device_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Device_ID;
    db.run("DELETE FROM Device_Table WHERE Device_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404).send();
        }
        res.status(200).send(row);
        res.end();
    });
};
