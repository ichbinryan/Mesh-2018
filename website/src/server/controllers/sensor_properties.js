const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Retrieve a list of sensor properties from the database
module.exports.getAll = (req, res) => {
  db.all("SELECT * FROM Sensor_Properties", function(err, row){
      if(err) {
          console.log(err);
          return res.status(404).send(err);
      }
      res.status(200).send(row);
      res.end();
  });
}

// Retrieves a list of sensor properties that can be affected by a location
module.exports.getByLocationId = (req, res) => {
  var id = req.params.Location_ID;
  var sql = 'SELECT * FROM Locations as A, Device_Table as B ' +
  'JOIN Device_Action_Relation as C ON C.Device_ID = B.Device_ID ' +
  'JOIN Device_Actions as D ON D.Action_ID = C.Action_ID ' +
  'JOIN Device_Action_SP_Relation as E ON E.Action_ID = D.Action_ID ' +
  'JOIN Sensor_Properties as F ON F.Sensor_Prop_ID = E.Sensor_Property_ID ' +
  'WHERE A.Location_ID = ? AND A.Location_ID = B.Location_ID GROUP BY F.Sensor_Prop_ID';

  db.all(sql, id, function(err, rows) {
    if(err) {
        console.log(err);
        return res.status(404).send(err);
    }
    res.status(200).send(rows);
    res.end();
  });
}
