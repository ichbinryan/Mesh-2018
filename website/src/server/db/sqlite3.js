const path = require('path')
var sqlite3 = require('sqlite3').verbose();
const yargs = require('yargs');

const argv = yargs.argv;    //set the program arguments

//the path to the db.
let p  = path.join(__dirname, '../../src/main/resources/data/');
console.log(p + "mesh.db");

//Connect to Sqlite data
var db = new sqlite3.Database(p + "mesh.db");

module.exports = {db};
