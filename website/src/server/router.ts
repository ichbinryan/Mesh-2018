const Devices = require('./controllers/device_table');
const Locations = require('./controllers/locations');
const Rules = require('./controllers/rules');
const Icons = require('./controllers/icons');
const SensorProperties = require('./controllers/sensor_properties');

module.exports = (app) => {
      // Device related routes
      app.post('/api/devices', Devices.insert);
      app.get('/api/devices', Devices.getDevicePage);
      app.get('/api/devices/:Device_ID', Devices.getById);
      app.patch('/api/devices/:Device_ID', Devices.updateById);
      app.delete('/api/devices/:Device_ID', Devices.deleteById);

      app.get('/api/actuators', Devices.getActuators);
      app.get('/api/sensors', Devices.getSensors);

      // Location related routes
      app.get('/api/locations', Locations.getAll);
      app.post('/api/locations', Locations.insert);
      app.delete('/api/locations/:Location_ID', Locations.deleteById);

      // Rules routes
      app.get('/api/rules', Rules.getAll);
      app.post('/api/rules', Rules.insert);
      app.delete('/api/rules/:Rule_ID', Rules.deleteById);

      // Device Actions
      app.get('/api/device_actions', Devices.getAllDeviceActions);

      // Sensor Properties routes
      app.get('/api/sensor_properties_for_location/:Location_ID', SensorProperties.getByLocationId);

      // Icon routes
      app.get('/api/icons', Icons.getAll);
};
