const appConfig = require('../config/main');

import * as e6p from 'es6-promise';
(e6p as any).polyfill();
import 'isomorphic-fetch';

import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';

import { Provider } from 'react-redux';
import { createMemoryHistory, match } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
const { ReduxAsyncConnect, loadOnServer } = require('redux-connect');
import { configureStore } from './app/redux/store';
import routes from './app/routes';

import { Html } from './app/containers';
const manifest = require('../build/manifest.json');

const express = require('express');
const path = require('path');
const compression = require('compression');
const Chalk = require('chalk');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const NUM_TIMESTEPS = 12;
const NUM_HOURS = 24;
const TIME_STEP_SIZE = NUM_HOURS / NUM_TIMESTEPS;

// Socket.io is used to interact with the Agent
io.on('connection', (socket) => {
  console.log('A connection has been established!');

  setInterval(() => {
      console.log('calling power stuff');
      socket.emit('device.getCummalativePowerConsumption');
  }, 5000);

  // socket.emit('device.getReading', {Device_ID: 5});
  socket.on('disconnect', () => {
    console.log('Client disconnected');
  });

  // The user has turned on a device
  socket.on('device.action', (data) => {
    console.log(data);
    socket.broadcast.emit('device.action', data);
  });

  socket.on('rule.newRule', (data) => {
    console.log('New Rule', data);
    socket.broadcast.emit('rule.newRule', data);
  });

  socket.on('device.getschedule', (data) => {
    console.log('requesting schedule');
    socket.broadcast.emit('device.getschedule', data);
  });

  socket.on('device.schedule', (data) => {
    console.log('received schedule');
    data = JSON.parse(data);
    // Get first element in the data (The schedule Object)
    const schedule = data[Object.keys(data)[0]];
    let newActionList = [];
    // iterate through each device in the schedule
    for (let i = 0; i < schedule.length; i++) {
      // get the device and the actionList
      const device = schedule[i];
      const deviceActions = device.Actions;
      let k = 0;
      // Get a list of new Actions
      do {

        const currentAction = deviceActions[0].Action_ID;
        const startTime = new Date();
        startTime.setHours(deviceActions[0].Timestep * TIME_STEP_SIZE, 0, 0, 0);

        let newAction = {
          // Action_ID: currentAction,
          // Action_Name: deviceActions[0].Action_Name,
          start: startTime,
          end: null,
          title: device.Device_Name + ' ' + deviceActions[0].Action_Name,
        };

        let lastTimeStep = deviceActions[0].Timestep;
        k++;
        for (let j = 1; j < deviceActions.length; j++) {
          if (currentAction === deviceActions[j].Action_ID &&
              lastTimeStep === deviceActions[j].Timestep - 1) {
              k++;
              lastTimeStep = deviceActions[j].Timestep;
          }
        }
        deviceActions.splice(0, 1);
        const endTime = new Date();
        endTime.setHours(lastTimeStep * TIME_STEP_SIZE, 0, 0, 0);
        newAction.end = endTime;
        newActionList.push(newAction);

      } while (k < deviceActions.length);
    }

    console.log(newActionList);
    socket.broadcast.emit('device.schedule', newActionList);
  });

  socket.on('device.reading', (data) => {
     console.log(data);
  });
});

app.use(compression());
app.use(bodyParser.json());

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackConfig = require('../config/webpack/dev');
  const webpackCompiler = webpack(webpackConfig);

  app.use(require('webpack-dev-middleware')(webpackCompiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: { colors: true },
    noInfo: true,
    hot: true,
    inline: true,
    lazy: false,
    historyApiFallback: true,
    quiet: true,
  }));

  app.use(require('webpack-hot-middleware')(webpackCompiler));
}

app.use(favicon(path.join(__dirname, 'public/favicon.ico')));

// API routes
const router = require('./server/router');
router(app);

app.use('/public', express.static(path.join(__dirname, 'public')));

app.get('*', (req, res) => {
  const location = req.url;
  const memoryHistory = createMemoryHistory(req.originalUrl);
  const store = configureStore(memoryHistory);
  const history = syncHistoryWithStore(memoryHistory, store);

  match({ history, routes, location },
    (error, redirectLocation, renderProps) => {
      if (error) {
        res.status(500).send(error.message);
      } else if (redirectLocation) {
        res.redirect(302, redirectLocation.pathname + redirectLocation.search);
      } else if (renderProps) {
        const asyncRenderData = Object.assign({}, renderProps, { store });

        loadOnServer(asyncRenderData).then(() => {
          const markup = ReactDOMServer.renderToString(
            <Provider store={store} key="provider">
              <ReduxAsyncConnect {...renderProps} />
            </Provider>,
          );
          res.status(200).send(renderHTML(markup, store));
        });
      } else {
        res.status(404).send('Not Found?');
      }
    });
});

server.listen(appConfig.port, appConfig.host, (err) => {
  if (err) {
    console.error(Chalk.bgRed(err));
  } else {
    console.info(Chalk.black.bgGreen(
      `\n\n💂  Listening at http://${appConfig.host}:${appConfig.port}\n`,
    ));
  }
});

function renderHTML(markup: string, store: any) {
  const html = ReactDOMServer.renderToString(
    <Html markup={markup} manifest={manifest} store={store} />,
  );

  return `<!doctype html> ${html}`;
}
