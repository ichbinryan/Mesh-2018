This is the website used for displaying a house and the devices connected to it.

This project is based off a starter kit which uses React, Redux, Server Side
rendering and many other tools. Please visit this URL to learn more about
the technologies used: https://barbar.github.io/vortigern/

Before development can proceed on the website most of the tools used by the
boilerplate project should be learned. Primarily what a developer must know are

-Typescript
-react
-Redux
-JavaScript (with ES6 syntax)
-Webpack (as our build tool)
-React-router
-Node.JS (using typescript)
-Express
-TSLint (for source code correctness)

(A Google search will easily find resources on the topics)

In order to be able to start developing there are a few tools needed.
Install node v6 or greater at: https://nodejs.org/en/

The installation package should come with NPM (otherwise known as the
Node Package Manager). After changing into this directory the command to
install the project dependencies is:
npm install

After the dependencies are installed (into a node_modules folder) then the
project should be able to start.

npm start

should compile the contents inside the src directory into a folder
called "build" which will contain a server file to execute and the client Side
javaScript to render the page. After building the script finishes it
will start the project on localhost:8889.

A developer will notice that anytime a client side typescript file is changed,
the build will automatically deploy. If changes aren't  noticed it is probable
that TSLint caught a Typescript code readability error. Inorder to avoid
restarting the project to catch the error a useful command can be run

npm run lint

will run TSlint on the project to make sure the source code validates.

Another thing to notice is that changing the server files does not automatically
recompile the project. The "npm start" process must be killed and started again
inorder to recompile the server files. I found this quite tedious and came up
with an alternative. Inorder to recompile everything on every save the user
can run the following commands (in their own terminal):

webpack --config config/webpack/server.js --watch
webpack --config config/webpack/dev.js --watch
nodemon build/server.js

This tells webpack to constantly recompile the project and uses nodemon to
restart the server on changes to the build/server.js file. I found this to be a
much nicer alternative to the standard scripts. Test files can be run by
running

npm run test

which will initiate the testing suite (unit tests, etc)
